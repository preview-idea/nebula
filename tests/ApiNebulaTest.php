<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 02/04/2019
 * Time: 14:41
 */

namespace Tests;

use Core\Services\ApiNebula;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class ApiNebulaTest extends TestCase
{
    /** @var Client $client */
    private $client;

    /** @var ApiNebula $apiNebula */
    private $apiNebula;

    private $token;

    public function setUp()
    {

        $this->testShouldCreateInstanceOfClientGuzzle();
        $this->testShouldGetAccessToken();

    }

    public function tearDown()
    {
        $this->client = null;
    }

    public function testShouldCreateInstanceOfClientGuzzle()
    {
        $headers = [
            'Accept'    => 'application/json',
            'User-Agent' => 'test-api-nebula-php',
        ];

        $this->client = new Client([
            'base_uri'      => 'http://api-nebula.local',
            'http_errors'   => true,
            'headers' => $headers
        ]);

        $this->assertInstanceOf(
            Client::class,
            $this->client
        );
    }

    public function testShouldGetAccessToken()
    {
        $creds = [
            'user_login'    => 'dev',
            'secret_api'    => 'qFQvVR8P5GyLLR98',
        ];

        $response = $this->client->post('/auth/verify',
            [ 'json' => $creds ]
        );

        $this->assertEquals(
            200,
            $response->getStatusCode()
        );

        $result = json_decode($response->getBody()->getContents());
        $this->token = sprintf('Bearer %s', $result->access_token);

    }
}
