<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 03/07/2018
 * Time: 09:28
 */

namespace Core\Services;

use Core\Services\Interfaces\EmailManagerInterface;
use Exception;

class EmailManager implements EmailManagerInterface
{

    private $mailer, $apiNebula;
    CONST DS = DIRECTORY_SEPARATOR;

    /**
     * EmailController constructor.
     * @param \Swift_Mailer $mailer
     * @param ApiNebula $apiNebula
     */
    public function __construct(\Swift_Mailer $mailer, ApiNebula $apiNebula)
    {
        $this->mailer = $mailer;
        $this->apiNebula = $apiNebula;
    }

    /**
     * @param string $template
     * @param string $message
     * @param string|array $recipient
     * @param string|array|null $toCopy
     * @param string|null $file
     * @return string
     * @throws Exception
     */
    public function sendEmail($template, $message, $recipient, $toCopy = null, $file = null): string
    {
        $message = (new \Swift_Message( sprintf('Mosaïc - %s', $message) ))
            ->setFrom('nebula@seris.fr')
            ->setTo($recipient)
            ->setBody($template, 'text/html')
        ;

        // OTHER RECIPIENTS PUT ON COPY
        if ($toCopy) :
            $message->setBcc($toCopy);
        endif;

        // ATTACHMENT FILE
        if (file_exists($file)) :

            $ext = strtolower(substr($file, strrpos($file, '.'), 5));
            $filename = str_replace(self::DS, '', substr($file, strrpos($file, self::DS), 50000));

            $message->attach(\Swift_Attachment::fromPath($file)
                ->setFilename($filename)
                ->setContentType($this->getMimeType($ext))
            );

        endif;

        $this->mailer->send($message);

        return ('L\'e-mail a été bien envoyé au(x) destinatiare(s)!');

    }

    /**
     * @param array $idSignataire
     * @return int|mixed|string
     * @throws Exception
     */
    public function getSignataire(array $idSignataire)
    {

        $signataire = $this->apiNebula->request(
            'POST',
            '/utilisateurs/sign',
            ["filter" => $idSignataire]

        );

        if (!$signataire) {
            throw new Exception(
                'Signataire not found',
                500
            );
        }

        return $signataire;
    }

    /**
     * @param string $ext
     * @return mixed
     * @throws Exception
     */
    public function getMimeType(string $ext): string
    {

        $mimeType = [
            '.doc' => 'application/msword',
            '.docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            '.pdf' => 'application/pdf',
            '.txt' => 'text/plain',
            '.rtf' => 'application/rtf',
            '.csv' => 'text/csv',
            '.xml' => 'application/xml',
            '.jpeg' => 'image/jpeg',
            '.jpg' => 'image/jpeg',
            '.png' => 'image/png',
            '.gif' => 'image/gif'
        ];

        if (!key_exists($ext, $mimeType)) {
            throw new Exception('Mime type not found', 404);

        }

        return $mimeType[$ext];

    }

}