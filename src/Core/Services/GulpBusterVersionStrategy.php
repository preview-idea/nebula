<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 22/03/2019
 * Time: 09:57
 */

namespace Core\Services;

use Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface;

class GulpBusterVersionStrategy implements VersionStrategyInterface
{
    /**
     * @var string
     */
    private $manifestPath;

    /**
     * @var string
     */
    private $format;

    /**
     * @var string[]
     */
    private $hashes;

    /**
     * GulpBusterVersionStrategy constructor.
     * @param string $manifestPath
     * @param string|null $format
     */
    public function __construct($manifestPath, $format = null)
    {
        $this->manifestPath = $manifestPath;
        $this->format = $format ?? '%s?%s';

    }

    /**
     * Returns the asset version for an asset.
     *
     * @param string $path A path
     *
     * @return string The version string
     */
    public function getVersion($path)
    {

        if (!is_array($this->hashes)) :
            $this->hashes = $this->loadManifest();
        endif;

        return $this->hashes[$path] ?? '';
    }

    /**
     * Applies version to the supplied path.
     *
     * @param string $path A path
     *
     * @return string The versionized path
     */
    public function applyVersion($path)
    {
        if (!$version = $this->getVersion($path)) :
            return $path;
        endif;

        $versionized = sprintf($this->format, ltrim($path, DIRECTORY_SEPARATOR), $version);

        if ($path && DIRECTORY_SEPARATOR === $path[0]) :
            return DIRECTORY_SEPARATOR.$versionized;
        endif;


        return $versionized;
    }

    /**
     * @return mixed
     */
    private function loadManifest()
    {
        $busterJson = json_decode(file_get_contents($this->manifestPath), true);

        $json_manifest = [];
        foreach ($busterJson as $key => $value) :
            $json_manifest[str_replace('web/', '', $key)] = $value;
        endforeach;

        return $json_manifest;
    }

}