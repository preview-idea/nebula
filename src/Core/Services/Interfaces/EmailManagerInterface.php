<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 23/08/2018
 * Time: 18:28
 */

namespace Core\Services\Interfaces;

/**
 * Interface InterfaceEmailManager
 * @package Core\Services\Interfaces
 */
interface EmailManagerInterface
{
    public function sendEmail($template, $message, $recipient, $toCopy = null,  $file = null);
}