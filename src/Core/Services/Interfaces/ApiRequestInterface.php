<?php

namespace Core\Services\Interfaces;

interface ApiRequestInterface
{

    public function request($method, $url, array $data = null, bool $ajax = false);

    public function getClient();

    public function getAccessToken();

}