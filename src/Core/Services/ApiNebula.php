<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 29/01/2018
 * Time: 09:06
 */

namespace Core\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiNebula
{

    private $base_uri_api,
        $secret_api,
        $container,
        $user_login,
        $headers,
        $token;

    /**
     * Score constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->base_uri_api = $container->getParameter('base_uri_api');
        $this->secret_api = $container->getParameter('secret_api');
        $this->user_login = $container->getParameter('remote_user');

    }

    public function request($method, $url, array $data = null, bool $ajax = false)
    {
        if ($this->token) :
            $this->setHeaders(['Authorization' => $this->token]);
        endif;

        try {
            $response = $this->getClient()->request($method, $url, [
                strtoupper($method) === 'GET' ? 'query'  : 'json' => $data
            ]);

        } catch (GuzzleException $e) {
            return $e->getCode();
        }

        return $ajax
            ? $response->getBody()->getContents()
            : json_decode($response->getBody()->getContents());

    }

    public function setLogin($login)
    {
        $this->user_login = $login;
    }

    public function setHeaders(array $header)
    {
        foreach ($header as $key => $value):
            $this->headers[$key] = $value;
        endforeach;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getClient()
    {
        $this->setHeaders([
            'Accept'    => 'application/json',
            'User-Agent' => 'Project-Meteor/Mosaic'
        ]);

        $client = new Client(['base_uri' => $this->base_uri_api,
            'headers' => $this->getHeaders()
        ]);

        return $client;
    }

    public function getAccessToken()
    {
        $data = [
            'user_login' => $this->user_login,
            'secret_api' => $this->secret_api
        ];

        $response = $this->getClient()->post('/auth/verify',
            [ 'json' => $data ]
        );

        $result = json_decode($response->getBody()->getContents());
        $this->token = sprintf('Bearer %s', $result->access_token);

        return $this->token;
    }

}