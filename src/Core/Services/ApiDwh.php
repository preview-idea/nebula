<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 29/01/2018
 * Time: 09:06
 */

namespace Core\Services;

use Core\Services\Interfaces\ApiRequestInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;


class ApiDwh implements ApiRequestInterface
{

    private $api_uri;
    private $client;
    private $container;
    private $headers;

    public function __construct(ContainerInterface $container)
    {

        $this->container = $container;
        $this->api_uri = $container->getParameter('api_dwh_uri');

        $this->client = HttpClient::createForBaseUri($this->api_uri);

    }

    public function request($method, $url, array $data = null, bool $ajax = false)
    {

        $this->setHeaders([
            'Accept'    => 'application/json',
            'User-agent' => 'Project-Meteor/Mosaic',
        ]);

        try {
            $response = $this->client->request($method, $url, [
                'headers' => $this->getHeaders(),
                'json' => $data,
                'buffer' => false
            ]);

            foreach ($this->client->stream($response) as $chunk) :
                echo $chunk->getContent();
            endforeach;

            die();

        } catch (TransportExceptionInterface $e) {
            return $e->getMessage();
        }

    }

    public function setHeaders(array $header)
    {
        foreach ($header as $key => $value) :
            $this->headers[$key] = $value;
        endforeach;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getClient()
    {
        // TODO: Implement getClient() method.

    }

    public function getAccessToken()
    {
        // TODO: Implement getAccessToken() method.
    }
}