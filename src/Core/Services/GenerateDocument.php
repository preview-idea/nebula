<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 02/07/2018
 * Time: 10:24
 */
namespace Core\Services;

use Dompdf\Dompdf;
use PhpOffice\PhpWord\Element\Section;
use PhpOffice\PhpWord\Element\TextRun;
use PhpOffice\PhpWord\Element\Title;
use PhpOffice\PhpWord\Exception\Exception;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\Shared\Html;
use PhpOffice\PhpWord\Style\Language;
use PhpOffice\PhpWord\Writer\Word2007\Element\Text;

class GenerateDocument
{

    private $section, $name, $phpw, $settings, $pathname, $apiNebula;
    CONST DS = DIRECTORY_SEPARATOR;

    public function __construct(ApiNebula $apiNebula)
    {
        $this->phpw = new PhpWord();
        $this->settings = $this->phpw->getSettings();
        $this->apiNebula = $apiNebula;
        $this->pathname = __DIR__ . self::DS . 'GenerateDocument' . self::DS;

        if (!is_dir($this->pathname)) :
            mkdir($this->pathname, 0700);

        endif;

        $this->defaultSettings();

    }

    public function defaultSettings()
    {
        // Default language
        $this->settings->setMirrorMargins(true);
        $this->settings->setThemeFontLang(new Language(Language::FR_FR));

        // Default Options
        Settings::setZipClass(Settings::PCLZIP);

        // Make sure you have `dompdf/dompdf` in your composer dependencies.
        $path_dompdf = sprintf('%s/../../../vendor/dompdf/dompdf', __DIR__);

        Settings::setPdfRendererPath($path_dompdf);
        Settings::setPdfRendererName(Settings::PDF_RENDERER_DOMPDF);

    }

    /**
     * @param int $idDocument
     * @param int $idTypeDocument
     * @param bool $pdf
     * @return string
     * @throws Exception
     */
    public function getDocument(int $idDocument, int $idTypeDocument, bool $pdf = false)
    {
        // GET CONTENT OF DOCUMENT
        $content = $this->apiNebula->request(
            'GET',
            sprintf('/admin/document/%d/content/%d', $idTypeDocument, $idDocument)
        );

        if (!is_array($content)) :
            return $content;
        endif;

        $pdf ?
            $doc = $this->createPdf($idDocument, $content, $idTypeDocument) :
            $doc = $this->createWord($idDocument, $content, $idTypeDocument);

        return $doc;
    }

    /**
     * @param string $name
     * @param array $content
     * @param $type
     * @return string
     */
    public function createWord($name, $content, $type): string
    {

        $this->name = $name;
        $nameDoc = $type === 1 ? '%sContrat-%s.docx' : '%sAvenant-%s.docx';
        $this->createContent($content);

        // Try to make the document to directory src/Core/Services/GenerateDocument :
        try {
            $filePath = sprintf($nameDoc, $this->pathname, $this->name);
            $objWriter = IOFactory::createWriter($this->phpw, 'Word2007');
            $this->escapeElementTextHtml($objWriter->getPhpWord()->getSections());
            $objWriter->save($filePath);

        } catch (Exception $e) {
            return $e->getMessage();
        }

        return $filePath;

    }

    /**
     * @param string $name
     * @param array $content
     * @param $type
     * @return string
     */
    public function createPdf($name, $content, $type): string
    {
        $this->name = $name;
        $nameDoc = $type === 1 ? '%sContrat-%s.pdf' : '%sAvenant-%s.pdf';

        // Open template in format Word2007
        $source = $this->createWord($name, $content, $type);
        $filePath = sprintf($nameDoc, $this->pathname, $this->name);

        try {
            $document = IOFactory::load($source);
            $objWriter = IOFactory::createWriter($document, 'PDF');
            $objWriter->save($filePath);

            @unlink($source);

        } catch (Exception $e) {
            return $e->getMessage();
        }

        return $filePath;

    }

    private function createContent(array $content)
    {
        /******************* STYLES ******************/
        // If you often need the same style again you can create a user defined style to the word document
        // and give the addText function the name of the style:
        $this->phpw->addFontStyle('fontStyle', ['name' => 'Arial', 'size' => 10, 'color' => '1B2232']);

        // Style to first title
        $this->phpw->addTitleStyle(1, ['size' => 12, 'bold' => true], ['keepNext' => true, 'align' => 'center', 'borderBottomSize' => 15]);

        // Adding a style to title common
        $this->phpw->addTitleStyle(2, ['size' => 10, 'bold' => true], ['keepNext' => true, 'spaceBefore' => 0, 'borderBottomSize' => 5]);

        /************** SECTION ****************/
        // Every element you want to append to the word document is placed in a section. So we need a section:

        /************ PAGE MARGIN *************/
        // The valus of margin in twips, take a look in the DOC.
        $this->section = $this->phpw->addSection([
            'marginTop' => 1700.787401575, // 3cm
            'marginRight' => 1133.858267717, // 2cm
            'marginBottom' => 850,3937007874, // 1,5cm
            'marginLeft' => 1133.858267717 // 2cm

        ]);

        /************** HEADER ****************/
        // We load the logo image into header
        $image = $this->pathname . 'images/seris_logo.jpg';
        $title_except = ["code de deontologie", "attestation", "autorisation d'exploitation droits d'image"];

        $header = $this->section->addHeader();
        $header->addImage($image, ['width' => 72, 'height' => 72, 'align' => 'center']);
        $header->addTextBreak(1);

        /************** BODY ****************/
        // GROUP ALL CONTENT BY GROUP PARAGRAPH
        $grp = $this->traitContent($content);

        // CONTEXT OF DOCUMENT WORD
        $count = 0;
        foreach ($grp as $key => $value) :

            if ($count === 0) :
                $traitValue = str_replace('<p style="text-align:justify;">', '', $value[0]);
                $this->section->addTitle(str_replace('</p>', '', $traitValue), 1);
                array_shift($value);

            elseif (in_array(strtolower($key), $title_except)) :
                $this->section->addPageBreak();
                $this->section->addTitle($key, 1);

            else:
                $this->section->addTitle($key, 2);

            endif;

            array_map(function($content) {
                // Adding Text element to the Section in format html
                Html::addHtml($this->section, $content);
            }, $value);

            $count++;

        endforeach;

        /*************** FOOTER ***************/
        $footer = $this->section->addFooter();
        $table = $footer->addTable(array('width' => '5000', 'unit' => 'pct'));
        $table->addRow();
        $table->addCell(2000)->addPreserveText($this->name, ['size' => 10], ['align' => 'left']);
        $table->addCell(2000)->addPreserveText('[ Paraphe ]', ['size' => 10], ['align' => 'right']);
        $footer->addPreserveText('Page {PAGE} sur {NUMPAGES}', ['size' => 8], ['align' => 'center']);

    }

    /**
     * @desc Delete all documents in the folder.
     */
    public function deleteAllDocuments()
    {

        $folder = $this->pathname;

        if (is_dir($folder)) :
            $scan = glob(rtrim($folder,'/').'/*.docx');

            foreach($scan as $index => $path) :
                if (is_file($path)) { unlink($path); }
            endforeach;

        endif;
    }

    private function traitContent($content)
    {
        $result = [];
        foreach ($content as $obj) :
            $content_prg = str_replace('<p>', '<p style="text-align:justify;">',$obj->liContenu);
            $result[$obj->liGrpParagraphe][] = $content_prg;

        endforeach;

        return $result;
    }

    private function escapeElementTextHtml($sections)
    {
        if (is_array($sections) && count($sections)) :
            foreach ($sections as $section) :
                /** @var $section Section */
                $this->escapeElementText($section->getElements());
            endforeach;

        endif;

    }

    private function escapeElementText($elements)
    {

        foreach ($elements as $element) :

            if ($element instanceof TextRun) :

                array_map(function($elem) {
                    $elem->setText(htmlspecialchars($elem->getText()));
                    return $elem;
                }, $element->getElements());

            endif;

        endforeach;

    }

}