<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 30/05/2018
 * Time: 11:32
 */

namespace Core\Services;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    CONST DS = DIRECTORY_SEPARATOR;

    private $documentDirectory;
    private $imageDirectory;
    private $subDirect;
    private $name;
    private $pathName;

    public function __construct($documentDirectory, $imageDirectory)
    {
        $this->subDirect = date("Y") .self::DS. date("m") .self::DS;
        $this->documentDirectory = $documentDirectory;
        $this->imageDirectory = $imageDirectory;

    }

    public function getTargetDirectory()
    {
        $file = new Filesystem();

        if (!$file->exists($this->pathName)) :
            $file->mkdir($this->pathName, 0755);
        endif;
    }

    public function upload(UploadedFile $file, $pathName = 'others', $customName = null, $maxWidth = 200)
    {
        $this->pathName = $pathName.self::DS.$this->subDirect;
        $this->name = $customName
            ? strtoupper($customName).'.'.$file->guessExtension()
            : md5(uniqid()).'.'.$file->guessExtension();

        // CREATE FOLDER ACCORDING TO TYPE
        $this->createFolderFile($file, $maxWidth);

        return  self::DS.$this->subDirect.$this->name;
    }

    public function createFolderFile(UploadedFile $file, $maxWidth)
    {
        $imgExtAllow = [ 'jpeg', 'jpg', 'png' ];
        $docExtAllow = [ 'pdf', 'docx', 'doc'];

        if (array_search(strtolower($file->guessExtension()), $imgExtAllow) !== false) {
            $this->pathName = $this->imageDirectory.self::DS.$this->pathName;

            $maxWidth && $file->guessExtension() !== 'png'
                ? $this->resizeImage($file, $maxWidth)
                : $file->move($this->pathName, $this->name);


        } elseif (array_search(strtolower($file->guessExtension()), $docExtAllow) !== false) {
            $this->pathName = $this->documentDirectory.self::DS.$this->pathName;
            $file->move($this->pathName, $this->name);
        }

    }

    public function resizeImage(UploadedFile $file, $maxWidth)
    {
        // MAKE DIRECTORY
        $this->getTargetDirectory();

        // TAKE CURRENT DIMENSION
        list($width, $height) = getimagesize($file);

        $resolve = ($maxWidth * 100) / $width;
        $heightWanted = ($height * $resolve) / 100;
        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($maxWidth,$heightWanted);
        imagecopyresampled($dst, $src, 0,0,0,0, $maxWidth, $heightWanted, $width, $height);
        imagedestroy($src);
        imagejpeg($dst, $this->pathName.$this->name, 100);

    }


}