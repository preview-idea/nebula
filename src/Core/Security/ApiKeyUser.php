<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 01/08/2018
 * Time: 13:57
 */

namespace Core\Security;


use Symfony\Component\Security\Core\User\UserInterface;

class ApiKeyUser implements UserInterface
{
    private $idUser;
    private $idMatricule;
    private $username;
    private $password;
    private $name;
    private $roles;
    private $email;
    private $salt;
    private $token;
    private $isActive;

    public function __construct($username, $password, $name,  $roles, $token, $isActive = true)
    {
        $this->username = $username;
        $this->password = $password;
        $this->name = $name;
        $this->roles = $roles;
        $this->token = $token;
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param mixed $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }

    /**
     * @return mixed
     */
    public function getIdMatricule()
    {
        return $this->idMatricule;
    }

    /**
     * @param mixed $idMatricule
     */
    public function setIdMatricule($idMatricule)
    {
        $this->idMatricule = $idMatricule;
    }


    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive)
    {
        $this->isActive = $isActive;
    }

    public function getRoles()
    {
        $roles = $this->roles;

        if (!$roles) { $roles[] = 'ROLE_MOSAIC_USER'; }
        return array_unique($roles);

    }

    public function setRoles($roles)
    {
        $this->roles[] = $roles;

    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}