<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 31/07/2018
 * Time: 14:34
 */

namespace Core\Security;

use Core\Services\ApiNebula;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class ApiKeyUserProvider implements UserProviderInterface
{

    private $nebula;
    private $logger;
    private $container;

    public function __construct(ApiNebula $nebula, LoggerInterface $logger, ContainerInterface $container)
    {
        $this->nebula = $nebula;
        $this->logger = $logger;
        $this->container = $container;
    }

    public function loadUserByUsername($username)
    {
        return $this->getUsernameForApiKey($username);
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof ApiKeyUser) :
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        endif;

        $username = $user->getUsername();

        return $this->getUsernameForApiKey($username);
    }

    public function supportsClass($class)
    {
        return ApiKeyUser::class === $class;
    }

    public function getUsernameForApiKey($username)
    {
        $switch_remote = null;
        $listUserSwith = ['dev','bdasilva'];

        if ($this->container->hasParameter($username)) :
            $switch_remote = $this->container->getParameter($username);
        endif;

        try {
            // verify environment and check if swtich_remote_user exists
            if ($this->container->get('kernel')->getEnvironment() !== 'prod') :
                if (in_array($username, $listUserSwith) && $switch_remote) :
                    $this->nebula->setLogin($switch_remote);
                endif;
            endif;

            $token = $this->nebula->getAccessToken();
            $tokenParts = explode('.',$token);
            $payload = json_decode((base64_decode($tokenParts[1])));

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            throw new CustomUserMessageAuthenticationException(
                'You have not access!'
            );
        }

        // Verify if has a liLoginLdap in the token and current environment
        if (is_object($payload) && property_exists($payload, 'liLoginLdap')) :

            $user = new ApiKeyUser($payload->liLoginLdap,null, $payload->liNomUser, $payload->roles, $token);
            $user->setIdUser($payload->idUser);
            $user->setIdMatricule($payload->idMatricule);

            return $user;

        endif;

        throw new UsernameNotFoundException(
            sprintf('Username "%s" does not exist.', $username)
        );
    }

}