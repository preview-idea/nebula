<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 02/08/2018
 * Time: 16:40
 */

namespace Core\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Loader\FilesystemLoader;

class FilesController extends Controller
{

    /**
     * @Route("/files/upload", name="files_upload_index")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function filesUploadIndexAction(Request $request)
    {
        return $this->render('@files/index.html.twig', [
            'page_title' => 'Gestion des Documents'
        ]);
    }

    /**
     * @Route("/files/upload/preview", name="files_upload_detail")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function filesUploadDetailAction(Request $request)
    {
        $filePath = $request->query->get('filePath');

        if (!$filePath) :
            throw $this->createNotFoundException();

        endif;

        $fileUploadPath = $this->getParameter('file_upload_path') . $filePath;

        $response = $this->file($fileUploadPath);
        $response->headers->set('Content-Description', 'File Transfer');
        $response->headers->set('Content-type', $response->getFile()->getMimeType());
        $response->headers->set('Content-Disposition', sprintf('inline; filename=%s', $response->getFile()->getFilename()));

        return $response;
    }

    /**
     * @Route("/files/upload/update", name="files_upload_udpate")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function filesUploadUpdateAction(Request $request)
    {
        $filePath = $request->get('filePath');
        $liCheminStock = $request->get('liCheminStock');
        $customName = $request->get('customName');

        if (!$filePath) :
            throw $this->createNotFoundException();

        endif;

        $fileUploadPath = $this->getParameter('file_upload_path');
        $pathFile =   DIRECTORY_SEPARATOR . date("Y") . DIRECTORY_SEPARATOR . date("m") . DIRECTORY_SEPARATOR;

        $currentFile = $this->file($fileUploadPath. $filePath)->getFile();

        try {
            $currentFile->move($fileUploadPath . $liCheminStock . $pathFile, $customName . sprintf('.%s', $currentFile->getExtension()));

        } catch (FileException $e) {
            return new JsonResponse($e->getMessage());
        }

        return new JsonResponse([ 'serverPathName' => $pathFile . $customName . sprintf('.%s', $currentFile->getExtension()) ]);
    }

    /**
     * @Route("/files/upload/new", name="files_upload_new")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function filesUploadNewAction(Request $request)
    {
        $doc = $request->files->get('fileUpload');
        $liCheminStock = $request->get('liCheminStock');
        $customName = $request->get('customName');

        if (!$doc) :
            throw $this->createNotFoundException();
        endif;

        $pathFile = $this->get('file_upload')->upload($doc, $liCheminStock, $customName);

        return new JsonResponse($pathFile);
    }

    /**
     * @Route("/files/upload/remove", name="files_upload_remove")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function filesUploadRemoveAction(Request $request)
    {

        $filePath = $request->get('filePath');

        if (!$filePath) :
            throw $this->createNotFoundException();
        endif;

        $fileUploadPath = $this->getParameter('file_upload_path') . $filePath;

        $file = new Filesystem();

        if (!$file->exists($fileUploadPath)) :
            throw $this->createNotFoundException();
        endif;

        $file->remove($fileUploadPath);
        return new JsonResponse(['message' => 'Suppression effectuée avec succès!']);

    }

    /**
     * @Route("/files/upload/form", name="files_upload_form")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function filesFormAction(Request $request)
    {
        $tableRef = $request->get('table_ref');

        if (!$tableRef) :
            throw $this->createNotFoundException();
        endif;

        $form = $this->get('api_nebula')->request(
            'GET',
            sprintf('/files/upload/forms/%s', $tableRef)
        );

        return new JsonResponse($form);

    }

}