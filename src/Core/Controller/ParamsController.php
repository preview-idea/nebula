<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 23/02/2018
 * Time: 10:21
 */

namespace Core\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ParamsController extends Controller
{

    /**
     * @Route("/params", name="table_params")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function getParams(Request $request)
    {
        $filter = $request->get('filter') ?? null;
        $tableParam = $request->get('param');

        if (!$tableParam) :
            throw $this->createNotFoundException();
        endif;

        $data =  $this->get('api_nebula')->request(
            'POST',
            sprintf('/params/%s', $tableParam),
            ["filter" => $filter]
        );

        return new JsonResponse($data);

    }

}