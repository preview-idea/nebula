<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/09/2018
 * Time: 15:34
 */

namespace Core\Controller;

use Core\Nuxeo\Client\Objects\Blob\Blob;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ApiNuxeoController extends Controller
{

    /**
     * @Route("/salaries/nuxeo/documents", name="nuxeo_documents")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function nuxeoDocumentsAction(Request $request)
    {
        $idMatricule = $request->get('idMatricule');

        if (!$idMatricule) :
            throw $this->createNotFoundException();
        endif;

        $documents = $this->get('api_nuxeo_automation')->getDocumentsByMatricule($idMatricule, true);

        return new JsonResponse($documents, 200);

    }

    /**
     * @Route("/salaries/nuxeo/documents/preview", name="nuxeo_documents_detail")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function detailDocumentsAction(Request $request)
    {

        $nuxeoPath = $request->get('nuxeoPath');

        if (!$nuxeoPath) :
            throw $this->createNotFoundException();
        endif;

        /** @var Blob $file */
        $file = $this->get('api_nuxeo_automation')->getDocumentByPath($nuxeoPath);

        $response =  new Response();

        $response->headers->set('Content-Description', 'File Transfer');
        $response->headers->set('Content-type', $file->getMimeType());
        $response->headers->set('Content-Disposition', sprintf('inline; filename=%s', $file->getFilename()));
        $response->headers->set('Content-Transfer-Encoding', 'binary');

        $response->setContent($file->getStream());

        return $response;

    }

    /**
     * @Route("/salaries/nuxeo/documents/zipfile", name="nuxeo_documents_zipfile")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function zipFileDocumentsAction(Request $request)
    {

        $idMatricule = $request->get('idMatricule');

        if (!$idMatricule) :
            throw $this->createNotFoundException();
        endif;

        $files = $documents = $this->get('api_nuxeo_automation')->getDocumentsForZipFile($idMatricule, true);
        $path_doc_zip = $this->getParameter('path_doc_zip');

        if (!is_dir($path_doc_zip)) :
            mkdir($path_doc_zip, 0700);
        endif;

        $zipName = $path_doc_zip . DIRECTORY_SEPARATOR . 'documents_dematis.zip';
        $zip = new \ZipArchive();

        if (count($files)) :

            $zip->open($zipName, \ZipArchive::CREATE);

            foreach ($files as $file) :

                /** @var Blob $fileNuxeo */
                $fileNuxeo = $this->get('api_nuxeo_automation')->getDocumentByPath($file['path']);

                if ($fileNuxeo instanceof Blob) :
                    $zip->addEmptyDir($file['dirFile']);
                    $zip->addFromString($file['dirFile'] . DIRECTORY_SEPARATOR . $fileNuxeo->getFilename(), $fileNuxeo->getStream());
                endif;

            endforeach;

            $zip->close();

        endif;

        $response = $this->file($zipName);
        $response->deleteFileAfterSend(true);

        return $response;

    }

}