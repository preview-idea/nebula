<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 15/05/2018
 * Time: 14:02
 */

namespace Core\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class ApiDwhController extends Controller
{
    /**
     * @Route("/dwh/views/{viewRef}", name="dwh_views")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function dwhViewsAction(Request $request)
    {
        $viewRef = $request->get('viewRef');
        $criteria = $request->get('criteria');

        $view = $this->get('api_dwh')->request(
            'GET',
            sprintf('/dwh/views/%s', $viewRef),
            $criteria
        );

        return new JsonResponse($view);

    }

    /**
     * @Route("/dwh/params", name="dwh_params")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function dwhParamAction(Request $request)
    {
        $criteria = $request->get('criteria');
        $filters = is_array($criteria['filter']) ? $criteria['filter'] : [];

        $response = $this->get('api_dwh')->request(
            'GET',
            sprintf('/dwh/params/%s', $criteria['param']),
            $filters
        );

        return new JsonResponse($response);

    }

}