<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 01/08/2018
 * Time: 09:22
 */

namespace Core\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;

class SecurityController extends Controller
{

    /**
     * @Route("/", name="mosaic_index")
     * @Method({"GET"})
     * @return Response
     */
    public function indexAction()
    {

        return $this->render('@salarie/accueil/index.html.twig', [
            'page_title' => 'Accueil'
        ]);
    }

    /**
     * @Route("/profile", name="app_profile")
     * @Method({"GET"})
     * @return Response
     */
    public function profileAction()
    {
        return $this->render('app/pages/profile.html.twig', [
            'page_title' => 'Profile'
        ]);
    }

    /**
     * @Route("/remote/users", name="remote_user_switch")
     * @Method({"GET"})
     * @return Response
     */
    public function remoteUserSwitchAction()
    {
        $utilisateurs = $this->get('api_nebula')->request('GET', '/admin/utilisateurs');
        return new JsonResponse($utilisateurs);

    }

    /**
     * @Route("/remote/users/switch", name="remote_user")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function remoteUserAction(Request $request)
    {
        $switch_remote = $request->get('LoginLdap');

        if (!$switch_remote) :
            $this->createNotFoundException();
        endif;

        if ($this->get('kernel')->getEnvironment() === 'prod') :
            return $this->redirectToRoute('salarie_index', [], 301);
        endif;

        $this->modifySwithRemoteUserYaml($switch_remote);
        return new JsonResponse(['message' => 'L\'utilisateur a bien été modifié'], 200);

    }

    /**
     * @param $switch_remote
     */
    private function modifySwithRemoteUserYaml($switch_remote)
    {
        $contentYaml = [];
        $userOrigin = $this->container->getParameter('remote_user');

        $path = sprintf(
            '%s/config/remote_user.yml',
            $this->container->getParameter('kernel.root_dir')
        );

        if (file_exists($path)) :
            $contentYaml = Yaml::parseFile($path);
        endif;

        // remove key switch_remote_user into the file if exists
        if ($contentYaml['parameters'] && key_exists('switch_remote_user', $contentYaml['parameters'])) :
            unset($contentYaml['parameters']['switch_remote_user']);
        endif;

        $contentYaml['parameters'][$userOrigin] = $switch_remote;

        $yaml = Yaml::dump($contentYaml);
        file_put_contents($path, $yaml);

    }

}