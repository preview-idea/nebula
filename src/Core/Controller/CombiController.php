<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 15/05/2018
 * Time: 14:02
 */

namespace Core\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class CombiController extends Controller
{
    /**
     * @Route("/combi", name="table_combi")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function getCombiAction(Request $request)
    {
        $filter = $request->get('filter') ?? null;
        $tableCombi = $request->get('combi');

        if (!$tableCombi) :
            throw $this->createNotFoundException();
        endif;

        $combi = $this->get('api_nebula')->request(
            'POST',
            sprintf('/combi/%s', $tableCombi),
            [ "filter" => $filter ]
        );

        return new JsonResponse($combi);
    }

}