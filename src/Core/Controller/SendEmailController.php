<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 15/05/2018
 * Time: 14:02
 */

namespace Core\Controller;

use JMS\Serializer\Handler\StdClassHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class SendEmailController extends Controller
{
    /**
     * @Route("/sendEmail", name="send_mail")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function sendEmailAction(Request $request)
    {

        $model = $request->get('model');
        $objModel = json_decode($request->get('objModel'));
        $template = $request->get('template');
        $message = $request->get('message');
        $recipient = $request->get('recipient');
        $toCopy = $request->get('toCopy');
        $annexe = $request->get('annexe');

        // Get template
        $template_email = $this->renderView(sprintf('CoreNebula:email:%s/%s.html.twig', $model, $template), [
                $model => property_exists($objModel, 'idContratOrigine') ? $objModel->idContratOrigine : $objModel
            ]
        );

        // Annexe document
        $file = null;
        if ($annexe) :

            switch ($model) {
                case 'contrat':
                    $file = $this->get('generate_doc')->getDocument($objModel->idContrat, 1);
                    break;

                case 'avenant' :
                    $file = $this->get('generate_doc')->getDocument($objModel->idContrat, 2);
                    break;

            }

        endif;

        // TRY TO SEND EMAIL
        try {
            $this->get('email_manager')->sendEmail(
                $template_email,
                $message,
                $recipient,
                $toCopy,
                $file
            );

            $result = [ 'message' => 'Le mail a été envoyé avec succès!' ];

        } catch (\Exception $e) {
            $result = [ 'error' => $e->getMessage() ];
        }

        return new JsonResponse($result);

    }

}