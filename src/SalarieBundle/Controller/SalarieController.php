<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 16/01/2018
 * Time: 10:26
 */

namespace SalarieBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SalarieController extends Controller
{

    /**
     * @Route("/salaries", name="salarie_index")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function salarieIndexAction(Request $request)
    {
        return $this->render('@salarie/index.html.twig', [
            'page_title' => 'Gestion des Salariés',
            'list_title' => 'Liste de salariés'
        ]);
    }

    /**
     * @Route("/salaries/new", name="salarie_new")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function salarieAddNewAction(Request $request)
    {
        return $this->render('@salarie/gestion/extra/verify_nir.html.twig', [
            'page_title' => 'Numéro de sécurité sociale'
        ]);
    }

    /**
     * @Route("/salaries/detail", name="salarie_detail")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function salarieGetOneAction(Request $request)
    {
        $idMatricule = $request->get('idMatricule');

        if (!$idMatricule) :
            throw $this->createNotFoundException();
        endif;

        $salarie = $this->get('api_nebula')->request(
            'GET',
            sprintf('/salaries/%d', $idMatricule)
        );

        return $this->render('@salarie/gestion/index.html.twig', [
            'page_title'        => 'Fiche Salarié',
            'salarie'           => $salarie

        ]);

    }

    /**
     * @Route("/salaries/update/{idMatricule}", name="salarie_update", requirements={"idMatricule"="\d+"})
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function salarieUpdateAction(Request $request)
    {

        $idMatricule = $request->get('idMatricule');

        if (!$idMatricule) :
            throw $this->createNotFoundException();
        endif;

        $salarie = $this->get('api_nebula')->request(
            'PUT',
            sprintf('/salaries/%d', $idMatricule),
            $request->request->all()
        );

        return new JsonResponse($salarie);

    }

    /**
     * @Route("/salaries/remove/{idMatricule}", name="salarie_remove", requirements={"idMatricule"="\d+"})
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function salarieRemoveAction(Request $request)
    {

        $idMatricule = $request->get('idMatricule');

        // REMOVE SALARIE
        if (!$idMatricule) :
            throw $this->createNotFoundException();
        endif;

        $response = $this->get('api_nebula')->request(
            'DELETE',
            sprintf('/salaries/%d', $idMatricule)
        );

        return new JsonResponse($response);
    }

}