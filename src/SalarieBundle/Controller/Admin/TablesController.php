<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 02/08/2018
 * Time: 16:40
 */

namespace SalarieBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TablesController extends Controller
{

    /**
     * @Route("/admin/tables", name="admin_tables_index")
     * @Method({"GET"})
     * @return Response
     */
    public function indexTablesParamAction()
    {
        return $this->render('@admin/tables/index.html.twig', [
            'page_title' => 'Gestion des Tables'

        ]);
    }

}