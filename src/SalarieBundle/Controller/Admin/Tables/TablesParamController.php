<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/04/2019
 * Time: 10:01
 */

namespace SalarieBundle\Controller\Admin\Tables;

use SalarieBundle\Controller\Admin\TablesController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TablesParamController extends TablesController
{

    /**
     * @Route("/admin/tables/params", name="table_param_list")
     * @Method({"GET"})
     * @return Response
     */
    public function listTableParamAction()
    {
        return new JsonResponse(
            $this->get('api_nebula')->request( 'GET', '/admin/tables/params')
        );
    }

    /**
     * @Route("/admin/tables/params", name="element_param_list")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function listElementsParamAction(Request $request)
    {
        $tableParam = $request->get('liAdminTable');

        if (!$tableParam) :
            throw $this->createNotFoundException();
        endif;

        $result = $this->get('api_nebula')->request(
            'GET',
            sprintf('/admin/tables/params/%s', $tableParam)
        );

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/tables/params/{tableParam}/new", name="element_param_new")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function newElementParamAction(Request $request)
    {

        $tableParam = $request->get('tableParam');

        if (!$tableParam) :
            throw $this->createNotFoundException();
        endif;

        $result = $this->get('api_nebula')->request(
            'POST',
            sprintf('/admin/tables/params/%s', $tableParam),
            $request->request->all()
        );

        return new JsonResponse($result, 201);

    }

    /**
     * @Route("/admin/tables/params/{tableParam}/update/{idElement}", name="tables_update", requirements={"idElement"="\d+"})
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function updateElementParamAction(Request $request)
    {
        $tableParam = $request->get('tableParam');
        $idElement = $request->get('idElement');

        if (!$tableParam || !$idElement) :
            throw $this->createNotFoundException();
        endif;

        $result = $this->get('api_nebula')->request(
            'PUT',
            sprintf('/admin/tables/params/%s/%d', $tableParam, $idElement),
            $request->request->all()
        );

        return new JsonResponse($result);

    }

    /**
     * @Route("/admin/tables/params/{tableParam}/remove/{idElement}", name="tables_remove", requirements={"idElement"="\d+"})
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function removeElementParamAction(Request $request)
    {
        $tableParam = $request->get('tableParam');
        $idElement = $request->get('idElement');

        if (!$tableParam || !$idElement) :
            throw $this->createNotFoundException();
        endif;

        $result = $this->get('api_nebula')->request(
            'DELETE',
            sprintf('/admin/tables/params/%s/%d', $tableParam, $idElement)
        );

        return new JsonResponse($result);

    }

}