<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/04/2019
 * Time: 10:01
 */

namespace SalarieBundle\Controller\Admin\Tables;

use SalarieBundle\Controller\Admin\TablesController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TablesCombiController extends TablesController
{

    /**
     * @Route("/admin/tables/combi", name="table_combi_list")
     * @Method({"GET"})
     * @return Response
     */
    public function listTableCombiAction()
    {
        return new JsonResponse(
            $this->get('api_nebula')->request('GET', '/admin/tables/combi')
        );
    }

    /**
     * @Route("/admin/tables/combi", name="element_combi_list")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function listElementsCombiAction(Request $request)
    {
        $tableCombi = $request->get('liAdminTable');

        if (!$tableCombi) :
            throw $this->createNotFoundException();
        endif;

        $result = $this->get('api_nebula')->request(
            'GET',
            sprintf('/admin/tables/combi/%s', $tableCombi)
        );

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/tables/combi/{tableCombi}/{idElement}", name="element_combi", requirements={"idElement"="\d+"})
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function elementCombiOneAction(Request $request)
    {
        $tableCombi = $request->get('tableCombi');
        $idElement = $request->get('idElement');

        if (!$tableCombi || !$idElement) :
            throw $this->createNotFoundException();
        endif;

        $result = $this->get('api_nebula')->request(
            'GET',
            sprintf('/admin/tables/combi/%s/%d',  $tableCombi, $idElement)
        );

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/tables/combi/{tableCombi}/new", name="element_combi_new")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function newElementCombiAction(Request $request)
    {

        $tableCombi = $request->get('tableCombi');

        if (!$tableCombi) :
            throw $this->createNotFoundException();
        endif;

        $result = $this->get('api_nebula')->request(
            'POST',
            sprintf('/admin/tables/combi/%s', $tableCombi),
            $request->request->all()
        );

        return new JsonResponse($result, 201);

    }

    /**
     * @Route("/admin/tables/combi/{tableCombi}/update/{idElement}", name="element_combi_update", requirements={"idElement"="\d+"})
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function updateElementCombiAction(Request $request)
    {
        $tableCombi = $request->get('tableCombi');
        $idElement = $request->get('idElement');

        if (!$tableCombi || !$idElement) :
            throw $this->createNotFoundException();
        endif;

        $result = $this->get('api_nebula')->request(
            'PUT',
            sprintf('/admin/tables/combi/%s/%d', $tableCombi, $idElement),
            $request->request->all()
        );

        return new JsonResponse($result);

    }

}