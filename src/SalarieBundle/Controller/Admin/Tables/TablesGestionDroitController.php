<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 05/04/2019
 * Time: 10:01
 */

namespace SalarieBundle\Controller\Admin\Tables;

use SalarieBundle\Controller\Admin\TablesController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TablesGestionDroitController extends TablesController
{

    /**
     * @Route("/admin/tables/droits", name="element_droit_list")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function listElementsDroitAction(Request $request)
    {
        $tableDroit = $request->get('liAdminTable');

        if (!$tableDroit) :
            throw $this->createNotFoundException();
        endif;

        $result = $this->get('api_nebula')->request(
            'GET',
            sprintf('/admin/utilisateurs/%s', $tableDroit)
        );

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/tables/droits/qualifcontrat/new", name="qualifcontrat_new")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function qualifContratNewAction(Request $request)
    {
        $result = $this->get('api_nebula')->request(
            'POST',
            sprintf('/admin/utilisateurs/qualifcontrat'),
            $request->request->all()
        );

        return new JsonResponse($result, 201);
    }

    /**
     * @Route("/admin/tables/droits/combiniveauxelements/new", name="combiniveauxelements_new")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function combiNiveauxElementsNewAction(Request $request)
    {
        $result = $this->get('api_nebula')->request(
            'POST',
            sprintf('/admin/utilisateurs/combiniveauxelements'),
            $request->request->all()
        );

        return new JsonResponse($result, 201);
    }

    /**
     * @Route("/admin/tables/droits/niveauxent", name="niveaux_ent_list")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function listNiveauxEntAction(Request $request)
    {

        $result = $this->get('api_nebula')->request(
            'GET',
            '/admin/utilisateurs/niveauxent'
        );

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/tables/droits/qualifcontrat", name="qualifcontrat_list")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function listQualifcontratAction(Request $request)
    {

        $result = $this->get('api_nebula')->request(
            'GET',
            '/admin/utilisateurs/qualifcontrat/unused'
        );

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/tables/droits/combiniveauxelements", name="combiniveauxelements_list")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function listCombiNiveauxElementsAction(Request $request)
    {

        $result = $this->get('api_nebula')->request(
            'GET',
            '/admin/utilisateurs/combiniveauxelements'
        );

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/tables/droits/combiniveauxelements/update/{idLigneCombiNiveauxElements}", name="combi_niveaux_elements_update", requirements={"idLigneCombiNiveauxElements"="\d+"})
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function combiNiveauxElementsUpdateAction(Request $request)
    {

        $idCombiNiveauxElements = $request->get('idLigneCombiNiveauxElements');

        if (!$idCombiNiveauxElements) :
            throw $this->createNotFoundException();
        endif;

        $result = $this->get('api_nebula')->request(
            'PATCH',
            sprintf('/admin/utilisateurs/combiniveauxelements/%d', $idCombiNiveauxElements),
            $request->request->all()
        );

        return new JsonResponse($result);

    }

    /**
     * @Route("/admin/tables/droits/combiniveauxelements/remove/{idLigneCombiNiveauxElements}", name="combi_niveaux_elements_remove", requirements={"idLigneCombiNiveauxElements"="\d+"})
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function combiNiveauxElementsRemoveAction(Request $request)
    {

        $idCombiNiveauxElements = $request->get('idLigneCombiNiveauxElements');

        if (!$idCombiNiveauxElements) :
            throw $this->createNotFoundException();
        endif;

        $response = $this->get('api_nebula')->request(
            'DELETE',
            sprintf('/admin/utilisateurs/combiniveauxelements/%d', $idCombiNiveauxElements)
        );

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/tables/droits/niveauxligparent", name="niveauxligparent_list")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function listNiveauxLigParentAction(Request $request)
    {
        $filter = $request->get('filter');

        $result = $this->get('api_nebula')->request(
            'POST',
            '/admin/utilisateurs/niveauxligparent',
            $filter

        );

        return new JsonResponse($result);
    }

    /**
     * @Route("/admin/tables/droits/qualifcontrat/detail", name="qualif_contrat_detail")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function qualifContratGetOneAction(Request $request)
    {
        $idQualifContrat = $request->get('idQualifcontrat');

        if (!$idQualifContrat) :
            throw $this->createNotFoundException();
        endif;

        $result = $this->get('api_nebula')->request(
            'GET',
            sprintf('/admin/utilisateurs/qualifcontrat/%d', $idQualifContrat)
        );

        return new JsonResponse($result);

    }

    /**
     * @Route("/admin/tables/droits/qualifcontrat/update/{idQualifcontrat}", name="qualif_contrat_update", requirements={"idQualifcontrat"="\d+"})
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function qualifContratUpdateAction(Request $request)
    {

        $idQualifContrat = $request->get('idQualifcontrat');

        if (!$idQualifContrat) :
            throw $this->createNotFoundException();
        endif;

        $result = $this->get('api_nebula')->request(
            'PATCH',
            sprintf('/admin/utilisateurs/qualifcontrat/%d', $idQualifContrat),
            $request->request->all()
        );

        return new JsonResponse($result);

    }

    /**
     * @Route("/admin/tables/droits/qualifcontrat/remove/{idQualifcontrat}", name="qualif_contrat_remove", requirements={"idQualifcontrat"="\d+"})
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function qualifContratRemoveAction(Request $request)
    {

        $idQualifContrat = $request->get('idQualifcontrat');

        if (!$idQualifContrat) :
            throw $this->createNotFoundException();
        endif;

        $response = $this->get('api_nebula')->request(
            'DELETE',
            sprintf('/admin/utilisateurs/qualifcontrat/%d', $idQualifContrat)
        );

        return new JsonResponse($response);
    }

}