<?php
/**
 * Created by PhpStorm.
 * User: bdasilva
 * Date: 30/05/2018
 * Time: 09:26
 */

namespace NebulaModules\Controller\Rapport;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RapportController extends Controller
{
    /**
     * @Route("/rapport", name="rapport_index")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function rapportAction(Request $request)
    {
        return $this->render("@rapport/index.html.twig", [
            "page_title" => "Reporting Exploitation"
        ]);
    }

    /**
     * @Route("/rapport/params", name="rapport_params_list")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function rapportParamsListAction(Request $request)
    {
        $param = $request->get("param");

        $listParams = $this->get('api_nebula')->request(
            'POST',
            sprintf('/rapport/params/%s', $param)
        );

        return new JsonResponse($listParams);
    }

    /**
     * @Route("/rapport/list", name="rapport_list")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function rapportListAction(Request $request)
    {
        $filter = $request->get("filter");

        $rapports = $this->get('api_nebula')->request(
            'GET',
            '/rapport',
            ["filter" => $filter]
        );

        return new JsonResponse($rapports);
    }

    /**
     * @Route("/rapport/detail/{idRapport}", name="rapport_detail")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function rapportEachOneAction(Request $request)
    {
        $idRapport = $request->get('idRapport');

        if (!$idRapport) :
            throw $this->createNotFoundException();
        endif;

        $rapport = $this->get("api_nebula")->request(
            'GET',
            sprintf("/rapport/%d", $idRapport)
        );

        return new JsonResponse($rapport);
    }
}