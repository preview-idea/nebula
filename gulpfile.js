var gulp = require('gulp'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    composer = require('gulp-uglify/composer'),
    uglifyes = require('uglify-es'),
    uglify = composer(uglifyes, console),
    cleanCss = require('gulp-clean-css'),
    del = require('del'),
    buster = require('gulp-buster'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin');

const paths = {
    styles: {
        custom: {
            src: 'web/assets/sass/custom.scss'
        },
        nebula: {
            src: [
                'web/assets/sass/nebula.scss',
                'web/assets/sass/nebula/**/*.scss'
            ]
        },
        dest: 'web/dist/css'

    },
    scripts: {
        datatables: {
            src: [
                // =========== scripts dataTables ========
                "web/assets/vendors/datatables.net/js/jquery.dataTables.min.js",
                "web/assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js",
                "web/assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js",
                "web/assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js",
                "web/assets/vendors/pdfmake/build/pdfmake.min.js",
                "web/assets/vendors/jszip/dist/jszip.min.js",
                "web/assets/vendors/pdfmake/build/vfs_fonts.js",
                "web/assets/js/dataTables/datetime-moment.js",
                "web/assets/vendors/DateJS/build/date.js",
                "web/assets/js/dataTables/dataTables.js",
            ]
        },
        custom: {
            src: 'web/assets/js/custom.js'
        },
        nebula: {
            src: [
                // ========= scripts essentials ==========
                'web/assets/js/checkIban/checkIban.js',
                'web/assets/js/CONSTS.js',
                'web/assets/js/moment/moment.cfg.js',
                'web/assets/js/plugins.js',

                // ============ scripts initial ==========
                'web/assets/js/nebula/*.js',
                'web/assets/js/nebula/admin/**/*.js',
                'web/assets/js/nebula/indexedDB/MosaicDB.js',
                'web/assets/js/nebula/indexedDB/**/*.js',
                'web/assets/js/nebula/filesUpload/**/*.js'
            ],
        },
        salarie: {
            src: [
                'web/assets/js/nebula/salarie/**/*.js'
            ],
        },
        calendrier: {
            src: [
                'web/assets/js/nebula/modules/calendrier/**/*.js'
            ],
        },
        rapport: {
            src: [
                'web/assets/js/nebula/modules/rapport/**/*.js'
            ],
        },
        dest: 'web/dist/js'
    },
    images: {
        src: 'web/assets/images/**/*.{jpg, jpeg, png}',
        dest: 'web/dist/images'
    }
};


// Clean all files in dist folder
const clean = () => del(['web/dist']);



/********* STYLES **********/
// Custom styles
async function stylesCustom() {
    return await gulp.src(paths.styles.custom.src)
        .pipe(sass({ outputStyle: 'compressed'}) )
        .pipe(cleanCss())
        .pipe(rename({ suffix: '.min' } ))
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(buster())
        .pipe(gulp.dest('web/dist'))
}

// Nebula styles
async function stylesNebula() {
    return await gulp.src(paths.styles.nebula.src)
        .pipe(sass({ outputStyle: 'compressed'}) )
        .pipe(cleanCss())
        .pipe(rename({ suffix: '.min' } ))
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(buster())
        .pipe(gulp.dest('web/dist'))
}


/********* SCRIPTS **********/
// Datatables scripts
async function scriptsDatatables() {
    return await gulp.src(paths.scripts.datatables.src)
        .pipe(concat('datatables.js'))
        .pipe(uglify().on('error', console.log))
        .pipe(rename( { suffix: '.min' } ))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(buster())
        .pipe(gulp.dest('web/dist'));
}

// Custom scripts
async function scriptsCustom() {
    return await gulp.src(paths.scripts.custom.src)
        .pipe(concat('custom.js'))
        .pipe(uglify().on('error', console.log))
        .pipe(rename( { suffix: '.min' } ))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(buster())
        .pipe(gulp.dest('web/dist'));
}

// Nebula scripts
async function scriptsNebula() {
    return await gulp.src(paths.scripts.nebula.src)
        .pipe(concat('nebula.js'))
        .pipe(uglify().on('error', console.log))
        .pipe(rename( { suffix: '.min' } ))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(buster())
        .pipe(gulp.dest('web/dist'));
}

// Salarié scripts
async function scriptsSalarie() {
    return await gulp.src(paths.scripts.salarie.src)
        .pipe(concat('salarie.js'))
        .pipe(uglify().on('error', console.log))
        .pipe(rename( { suffix: '.min' } ))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(buster())
        .pipe(gulp.dest('web/dist'));
}

// Calendrier scripts
async function scriptsCalendrier() {
    return await gulp.src(paths.scripts.calendrier.src)
        .pipe(concat('calendrier.js'))
        .pipe(uglify().on('error', console.log))
        .pipe(rename( { suffix: '.min' } ))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(buster())
        .pipe(gulp.dest('web/dist'));
}

// Rapport scripts
async function scriptsRapport() {
    return await gulp.src(paths.scripts.rapport.src)
        .pipe(concat('rapport.js'))
        .pipe(uglify().on('error', console.log))
        .pipe(rename( { suffix: '.min' } ))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(buster())
        .pipe(gulp.dest('web/dist'));
}


/********* IMAGES **********/
async function images() {
    return gulp.src(paths.images.src, { since: gulp.lastRun(images) })
        .pipe(imagemin({ optimizationLevel: 5 }))
        .pipe(gulp.dest(paths.images.dest));
}


// Watch scripts / styles
async function watch() {

    // Styles
    await gulp.watch(paths.styles.custom.src, stylesCustom);
    await gulp.watch(paths.styles.nebula.src, stylesNebula);

    // Scripts
    await gulp.watch(paths.scripts.datatables.src, scriptsDatatables);
    await gulp.watch(paths.scripts.custom.src, scriptsCustom);
    await gulp.watch(paths.scripts.nebula.src, scriptsNebula);
    await gulp.watch(paths.scripts.salarie.src, scriptsSalarie);
    await gulp.watch(paths.scripts.calendrier.src, scriptsCalendrier);
    await gulp.watch(paths.scripts.rapport.src, scriptsRapport);

    // Images
    await gulp.watch(paths.images.src, images);
}

const build = gulp.series(clean, gulp.parallel(
    stylesCustom, stylesNebula, scriptsDatatables, scriptsCustom, scriptsNebula,
    scriptsSalarie, scriptsCalendrier, scriptsRapport, images, watch
));

exports.clean = clean;
exports.stylesCustom = stylesCustom;
exports.stylesNebula = stylesNebula;
exports.scriptsDatatables = scriptsDatatables;
exports.scriptsCustom = scriptsCustom;
exports.scriptsNebula = scriptsNebula;
exports.scriptsSalarie = scriptsSalarie;
exports.scriptsCalendrier = scriptsCalendrier;
exports.scriptsRapport = scriptsRapport;
exports.images = images;
exports.watch = watch;
exports.build = build;

exports.default = build;
