/********* CUSTOMS FUNCTIONS FOR TYPE OF STRING ******/

// Convert string to date. Getting value in format('DD/MM/YYYY').
String.prototype.convertToDate = function() {
    return this.replace(/(\d{2})\/(\d{2})\/(\d{4})/, '$3-$2-$1');
};

// Check if it is a format date view e.g format('DD/MM/YYYY').
String.prototype.isFormatDateView = function() {
    if (this.match(/^\d{2}\/\d{2}\/\d{4}$/)) { return true; }
};

// Convert CamelCase to underscore or string of characters
String.prototype.convertCamelCaseFrom = function(spt) {
    if (!this.match(/([a-z])([A-Z])/g)) { return; }
     return this.replace(/([a-z])([A-Z])/g, sprintf('$1%s$2', spt || '_')).toLowerCase();
};

// Check if format BIC is valid
String.prototype.validBIC = function() {
    // Full pattern :  ^[A-Z]{6}[A-Z2-9][A-NP-Z1-9](X{3}|[A-WY-Z0-9][A-Z0-9]{2})?$

    if (this.length <= 6) {
        if (this.match(/^[A-Z]*$/)) { old_value = this;  }

    } else if (this.length === 7) {
        if (this.match(/^[A-Z]{6}[A-Z2-9]$/)) { old_value = this; }

    } else if (this.length === 8 ) {
        if (this.match(/^[A-Z]{6}[A-Z2-9][A-NP-Z1-9]$/)) { old_value = this; }

    } else if (this.length >= 9) {
        if (this.match(/^[A-Z]{6}[A-Z2-9][A-NP-Z1-9]X+$/)) { old_value = this; }
        else if (this.charAt(8) !== 'X') { old_value = this; }
    }

    return old_value;

};

// Check iff format IBAN is valid
String.prototype.checkIBAN = function() {

    // Remove spaces and to upper case
    let iban = this.replace( /\s+/g, '' ).toUpperCase(),
        ibancheckdigits = "",
        leadingZeroes = true,
        cRest = "",
        cOperator = "",
        countrycode, ibancheck, charAt, cChar, bbanpattern, bbancountrypatterns, ibanregexp, i, p;

    // Check for IBAN code length.
    // It contains:
    // country code ISO 3166-1 - two letters,
    // two check digits,
    // Basic Bank Account Number (BBAN) - up to 30 chars
    let minimalIBANlength = 5;
    if ( iban.length < minimalIBANlength ) { return false; }

    // Check the country code and find the country specific format
    countrycode = iban.substring( 0, 2 );
    bbancountrypatterns = {
        "AL": "\\d{8}[\\dA-Z]{16}",
        "AD": "\\d{8}[\\dA-Z]{12}",
        "AT": "\\d{16}",
        "AZ": "[\\dA-Z]{4}\\d{20}",
        "BE": "\\d{12}",
        "BH": "[A-Z]{4}[\\dA-Z]{14}",
        "BA": "\\d{16}",
        "BR": "\\d{23}[A-Z][\\dA-Z]",
        "BG": "[A-Z]{4}\\d{6}[\\dA-Z]{8}",
        "CR": "\\d{17}",
        "HR": "\\d{17}",
        "CY": "\\d{8}[\\dA-Z]{16}",
        "CZ": "\\d{20}",
        "DK": "\\d{14}",
        "DO": "[A-Z]{4}\\d{20}",
        "EE": "\\d{16}",
        "FO": "\\d{14}",
        "FI": "\\d{14}",
        "FR": "\\d{10}[\\dA-Z]{11}\\d{2}",
        "GE": "[\\dA-Z]{2}\\d{16}",
        "DE": "\\d{18}",
        "GI": "[A-Z]{4}[\\dA-Z]{15}",
        "GR": "\\d{7}[\\dA-Z]{16}",
        "GL": "\\d{14}",
        "GT": "[\\dA-Z]{4}[\\dA-Z]{20}",
        "HU": "\\d{24}",
        "IS": "\\d{22}",
        "IE": "[\\dA-Z]{4}\\d{14}",
        "IL": "\\d{19}",
        "IT": "[A-Z]\\d{10}[\\dA-Z]{12}",
        "KZ": "\\d{3}[\\dA-Z]{13}",
        "KW": "[A-Z]{4}[\\dA-Z]{22}",
        "LV": "[A-Z]{4}[\\dA-Z]{13}",
        "LB": "\\d{4}[\\dA-Z]{20}",
        "LI": "\\d{5}[\\dA-Z]{12}",
        "LT": "\\d{16}",
        "LU": "\\d{3}[\\dA-Z]{13}",
        "MK": "\\d{3}[\\dA-Z]{10}\\d{2}",
        "MT": "[A-Z]{4}\\d{5}[\\dA-Z]{18}",
        "MR": "\\d{23}",
        "MU": "[A-Z]{4}\\d{19}[A-Z]{3}",
        "MC": "\\d{10}[\\dA-Z]{11}\\d{2}",
        "MD": "[\\dA-Z]{2}\\d{18}",
        "ME": "\\d{18}",
        "NL": "[A-Z]{4}\\d{10}",
        "NO": "\\d{11}",
        "PK": "[\\dA-Z]{4}\\d{16}",
        "PS": "[\\dA-Z]{4}\\d{21}",
        "PL": "\\d{24}",
        "PT": "\\d{21}",
        "RO": "[A-Z]{4}[\\dA-Z]{16}",
        "SM": "[A-Z]\\d{10}[\\dA-Z]{12}",
        "SA": "\\d{2}[\\dA-Z]{18}",
        "RS": "\\d{18}",
        "SK": "\\d{20}",
        "SI": "\\d{15}",
        "ES": "\\d{20}",
        "SE": "\\d{20}",
        "CH": "\\d{5}[\\dA-Z]{12}",
        "TN": "\\d{20}",
        "TR": "\\d{5}[\\dA-Z]{17}",
        "AE": "\\d{3}\\d{16}",
        "GB": "[A-Z]{4}\\d{14}",
        "VG": "[\\dA-Z]{4}\\d{16}"
    };

    bbanpattern = bbancountrypatterns[ countrycode ];

    // As new countries will start using IBAN in the
    // future, we only check if the countrycode is known.
    // This prevents false negatives, while almost all
    // false positives introduced by this, will be caught
    // by the checksum validation below anyway.
    // Strict checking should return FALSE for unknown
    // countries.
    if ( typeof bbanpattern !== "undefined" ) {
        ibanregexp = new RegExp( "^[A-Z]{2}\\d{2}" + bbanpattern + "$", "" );
        if ( !( ibanregexp.test( iban ) ) ) {
            return false; // Invalid country specific format
        }
    }

    // Now check the checksum, first convert to digits
    ibancheck = iban.substring( 4, iban.length ) + iban.substring( 0, 4 );

    for ( i = 0; i < ibancheck.length; i++ ) {
        charAt = ibancheck.charAt( i );
        if ( charAt !== "0" ) {
            leadingZeroes = false;
        }
        if ( !leadingZeroes ) {
            ibancheckdigits += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf( charAt );
        }
    }

    // Calculate the result of: ibancheckdigits % 97
    for ( p = 0; p < ibancheckdigits.length; p++ ) {
        cChar = ibancheckdigits.charAt( p );
        cOperator = "" + cRest + "" + cChar;
        cRest = cOperator % 97;
    }
    return cRest === 1;
};


/********* CUSTOMS FUNCTIONS FOR TYPE OF ARRAY ******/

Array.prototype.sortArrayBy = function(sortBy, type) {
    let _arr = this.slice(0);

    switch(type) {
        case 'ASC':
        case 'asc':
            _arr.sort((a,b) => (a[sortBy] < b[sortBy]) ? 1 : ((b[sortBy] < a[sortBy]) ? -1 : 0));
            break;

        default:
            _arr.sort((a,b) => (a[sortBy] > b[sortBy]) ? 1 : ((b[sortBy] > a[sortBy]) ? -1 : 0));
            break;
    }

    return _arr;
};


/********* CUSTOMS FUNCTIONS ******/

(function($) {

    // Plugins get custom data in SELECT 2
    $.fn.customDataSelect2 = function(a) {
        if (this.data('select2') || this.hasClass('select2-hidden-accessible')) {
            if (this.select2('data')[0].custom_data) {
                return this.select2('data')[0].custom_data[a];
            }
        }
    };

    // Plugin to reset a block of form
    $.fn.resetFormBlock = function() {
        if (this[0].tagName === 'FORM') {
            this[0].reset();
            this.find('select.select2-hidden-accessible').change();
        }

    };

}( jQuery ));

// CREATE MODAL INFO JS
(function($) {
    $.createModalInfo = function(b) {
        const _defaults = {
            title: '',
            name: 'prevModalInfo',
            content: '<h1>Your content goes here!</h1>',
            modal_height: 'modal-lg'
        };

        const _b = $.extend({}, _defaults, b),
            _content_modal = createElementsToModalInfo(_b);

        $('body').prepend(_content_modal);
        $(`#${_b.name}`).modal().on('hidden.bs.modal', function() { $(this).remove(); });
        reloadJS();

    };

    function createElementsToModalInfo({ name, title, content, modal_height }) {
        let html = `<div class="modal fade" role="dialog" id="${name}">`;

        html += `<div class="modal-dialog ${modal_height}">`;
        html += '<div class="modal-content">';
        html += '<div class="modal-header">';
        html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';

        html += `<h4 class="modal-title" style="color: #B21C12;">${title}</h4>`;

        html += '</div>';
        html += `<div class="modal-body">${content}</div>`;

        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '<div class="clearfix"></div>';

        return html;

    }

})( jQuery );



// CREATE FILE TREE STRUCTURE
(function($) {
    $.fn.FTreeMosaic = function(method) {

        let settings = { // settings to expose
                animationSpeed      : 'fast',
                collapsed           : true,
                console             : false
            },
            methods = {
                init: function(options) {

                    // Get standard settings and merge with passed in values
                    $.extend(settings, options);

                    // Do this for every file tree found in the document
                    return this.each(function() {

                        let $fileList = $(this);

                        $fileList
                            .addClass('file-list')
                            .find('li')
                            .has('ul') // Any li that has a list inside is a folder root
                            .addClass('folder-root closed')
                            .not('[data-uid]')
                            .on('click', 'a[href="javascript:void(0)"]', function(e) { // Add a click override for the folder root links
                                e.preventDefault();

                                let _folder = $(this).parent(),
                                    _icon = $(this).find('i.fa'),
                                    _sub_folder = _folder.find('ul li.open');

                                _folder.toggleClass('closed').toggleClass('open');
                                _icon.toggleClass('fa-folder').toggleClass('fa-folder-open');

                                // close sub folders if parent is closed.
                                if (_sub_folder.length) {
                                    _sub_folder.toggleClass('closed').toggleClass('open');
                                    _sub_folder.find('i.fa').toggleClass('fa-folder').toggleClass('fa-folder-open');
                                }

                                return false;

                            });

                    });


                }
            };

        if (typeof method === 'object' || !method){
            return methods.init.apply(this, arguments);
        } else {
            $.on( "error", function() {
                console.log(method + " does not exist in the file exploerer plugin");
            } );
        }
    }

})(jQuery);