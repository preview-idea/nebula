/**
 *
 * @param {Object} opt
 * @constructor
 */
function AfficheCache(opt) {
    this.target =  opt.target;
    this.elems = opt.elements;
    this.val = opt.values;
    this.isCached = opt.isCached || false;

    this.actionElem();
}

AfficheCache.prototype = {

    actionElem: function () {

        if (this.target.val()) {
            if (this.val.indexOf(parseInt(this.target.val())) !== -1) {
                this.isCached
                    ? this.cacheElem(this.elems)
                    : this.resetElem(this.elems);
            }

            this.hideBlockIfNotElementVisible();

        }

        this.target.change(function() {
            this.val.indexOf(parseInt(this.target.val())) !== -1
                ? this.isCached
                    ? this.cacheElem(this.elems)
                    : this.resetElem(this.elems)
                : !this.isCached
                    ? this.cacheElem(this.elems)
                    : this.resetElem(this.elems);

        }.bind(this));

        reloadJS();

    },

    resetElem: function (el) {
        for(let i=0; i < el.length; i++) {

            //SHOW BLOCK IF IT'S HIDDEN
            this.showBlockIfHidden(el[i]);

            el[i].hasClass('flat')
                ? el[i].parent().parent().removeClass('hide')
                : el[i].parent().removeClass('hide');

            if (el[i].parent().hasClass('hide')) { el[i].parent().removeClass('hide'); }
            else if(el[i].hasClass('flat')) { el[i].parent().parent().show(); }
            else { el[i].parent().show(); } // Pour les éléments du clause_add qui sont cachés par le css.

            this.addRequired(el[i]);

        } // for

    },

    cacheElem: function(el) {

        for (let i=0; i < el.length; i++) {

            el[i].hasClass('flat')
                ? el[i].parent().parent().addClass('hide')
                : el[i].parent().addClass('hide');

            el[i].prop('required', false);
            el[i].parent().find('span.valid-nebula').remove();

            this.resetAllElements(el[i]);

        } // for

        this.hideBlockIfNotElementVisible();

    },

    addRequired: function(elem) {
        if (['checkbox','radio'].indexOf(elem.prop('type')) === -1 && elem.is(':visible')) {
            elem.prop('required', true);

            if (elem.parent().find('span.valid-nebula').length < 1) {
                elem.parent().append('<span class="valid-nebula"></span>')
            }
        }

    },

    resetAllElements: function(elem) {
        if (!URL_CURRENT.match(/avenants/) && elem.is(':hidden')) {

            if (elem.is('select')) { elem.prop('selectedIndex', 0).trigger('change'); }
            else if (elem.is('input[type!="checkbox"]')) { elem.val(''); }
            else { elem.hasClass('flat') ? elem.iCheck('uncheck') : elem.prop('checked', false); }
        }

    },

    showBlockIfHidden: function(elem) {
        if (elem.parents('div.x_panel').hasClass('hide')) {
            elem.parents('div.x_panel').removeClass('hide');
            elem.parents('div.x_panel').removeAttr('style'); // Pour la partie avenant.

        } else if (!elem.parents('div.x_panel').is(':visible')) {
            elem.parents('div.x_panel').show();
        }

    },

    hideBlockIfNotElementVisible: function() {

        this.elems.forEach(elem => {
            const divs = elem.parents('form').find('div');

            if (!divs.is(':visible') && elem.parents('div.x_panel').is(':visible')) {
                elem.parents('div.x_panel').addClass('hide');
            }

        });

    }

};


