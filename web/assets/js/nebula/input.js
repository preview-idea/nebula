$(function() {
    chargeFormLabel();
    inputMaskTel();

});

function chargeFormLabel() {

    let $fc = $('.form-control');

    $fc.each(function() {
        changeState($(this));
    });

    $fc.on('change blur', function() {
        changeState($(this));
    });

}

function changeState($f) {

    $f.val() ?
        $f.parent().addClass('has-value') :
        $f.parent().removeClass('has-value');

}

// AUTO ADD ICON REQUIRED IF PROPERTY REQUIRED EXISTS
function validNebula() {
    $('input, select, textarea').not(':hidden').each(function() {

        if ($(this).is(':required')) {
            $(this).after('<span class="valid-nebula"></span>');
        }

    })
}

function removeAccentInputText() {
    let $inputs = $('input[type=text]');

    $inputs.forEach(input => {
        input.on('input change', () => {
            $(this).val(() => {
                return removeAccent($(this).val())
            })
        })
    })
}

// iCheck
function inputFlat(elem) {
    elem.iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });
}

function inputMaskTel() {
    let input_tels = $('input[type="tel"]');

    input_tels.inputmask({mask: '99 99 99 99 99'})
}

function inputSwitchery(el) {
    var switchery = new Switchery(el, {
        color: '#26B99A'
    });
}