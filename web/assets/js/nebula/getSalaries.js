$(function() {
    console.log('init_search_salarie');

});

/**
 *
 * @param {Object} opt
 * @constructor
 */
function GetSalaries(opt){
    this.target = opt.target;
    this.minWord = opt.minWord || 4;

}

GetSalaries.prototype = {

    getListSalaries: function() {
        const exceptions = ['idSignataire'];

        this.target.forEach(function(target) {
            target.select2({
                ajax: {
                    url: exceptions.indexOf(target.prop('name')) !== -1 ? '/utilisateurs' : '/salaries/all',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            filter:  { params: params.term.toLowerCase() }
                        };
                    },
                    processResults: function (data) {
                        if (typeof data !== "number") {
                            return {
                                results: data.map(function (d) {
                                    return {
                                        id: d.idMatricule,
                                        text: d.liNomComplet,
                                        custom_data: { user_email: d.liEmail }
                                    }
                                })
                            }
                        } else {
                            return { results: null }
                        }

                    }
                },
                minimumInputLength: this.minWord,
                language: 'fr'

            });

        }.bind(this));

        this.target.forEach(function(target){
            target.change(function(){
                chargeFormLabel();
            });
        });
    },

    getNomComplet: function(withMatricule = false) {
        const exceptions = ['idSignataire'];

        this.target.forEach(function(target) {
           if (target.val()) {

               $.getJSON( exceptions.indexOf(target.prop('name')) !== -1 ? '/utilisateurs' : '/salaries/nomcomplet',{
                   filter: { idMatricule: parseInt(target.val()) }
               })
                   .done(function(data) {
                       const option = target.children('option:selected');

                        if (typeof data !== "number") {
                            const optionText = !withMatricule
                                ? data.liNomComplet
                                : data.liNomComplet.concat(' - ', target.val())

                            option.text(optionText);

                        } else {
                            notifyNebula(
                                sprintf('Désolé, aucun Utilisateur est attribué à ce matricule : %d', target.val()),
                                null,
                                'danger'
                            )
                        }

                       if (URL_CURRENT.match(/(contrats|avenants)\/detail/)) {
                           target.select2({
                               minimumResultsForSearch: 12,
                               language: 'fr'
                           });

                       } else {
                           this.getListSalaries();
                       }

                   }.bind(this))

                   .fail(function(error) {
                       console.log(error);
                   })
           }

        }.bind(this));
    },

    getListToSelect2Inactif: function() {

        this.target.forEach(function(target) {
            target.select2({
                ajax: {
                    url: '/salaries/inactif/list',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            filter:  { params: params.term.toLowerCase() }
                        };
                    },
                    processResults: function (data) {
                        if (typeof data !== "number") {
                            return {
                                results: data.map(function (d) {
                                    return {
                                        id: d.idMatricule,
                                        text: d.liNomComplet
                                    }
                                })
                            }
                        } else {
                            return { results: null }
                        }

                    }
                },
                minimumInputLength: this.minWord,
                language: 'fr'

            });

        }.bind(this));

        this.target.forEach(function(target){
            target.change(function(){
                chargeFormLabel();
            });
        });
    },

    getListToSelect2Actif: function() {

        this.target.forEach(function (target) {
            target.select2({
                ajax: {
                    url: '/salaries/actif/list',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            filter: { params: params.term.toLowerCase() }
                        };
                    },
                    processResults: function (data) {
                        if (typeof data !== "number") {
                            return {
                                results: data.map(function (d) {
                                    return {
                                        id: d.idMatricule,
                                        text: d.liNomComplet
                                    }
                                })
                            }
                        } else {
                            return { results: null }
                        }

                    }
                },
                minimumInputLength: this.minWord,
                language: 'fr'

            });

        }.bind(this));

    },

    getListToSelect2All: function() {

        this.target.forEach(function (target) {
            target.select2({
                ajax: {
                    url: '/salaries/all',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            filter: { params: params.term.toLowerCase() }
                        };
                    },
                    processResults: function (data) {
                        if (typeof data !== "number") {
                            return {
                                results: data.map(function (d) {
                                    return {
                                        id: d.idMatricule,
                                        text: d.liNomComplet,
                                        custom_data: {
                                            salarie: {
                                                idMatricule: d.idMatricule,
                                                liNomComplet: d.liNomComplet.replace(/\s-\s\d+$/, ''),
                                                liCheminPhoto: d.liCheminPhoto,
                                                liEtat: d.liEtat,
                                                liAgence: d.liAgence,
                                                liTelephone1: d.liTelephone1
                                            }
                                        }
                                    }
                                })
                            }
                        } else {
                            return { results: null }
                        }

                    }
                },
                minimumInputLength: this.minWord,
                language: 'fr'

            });

        }.bind(this));

    },

    getListToSelectSiteAll: function() {

        this.target.forEach(function (target) {
            target.select2({
                ajax: {
                    url: '/salaries/site/all',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            filter: { params: params.term.toLowerCase() }
                        };
                    },
                    processResults: function (data) {
                        if (typeof data !== "number") {
                            return {
                                results: data.map(function (d) {
                                    return {
                                        id: d.idMatricule,
                                        text: d.liNomComplet
                                    }
                                })
                            }
                        } else {
                            return { results: null }
                        }

                    }
                },
                minimumInputLength: this.minWord,
                language: 'fr'

            });

        }.bind(this));

    },

};