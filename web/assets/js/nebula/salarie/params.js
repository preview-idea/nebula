function init_params_salarie() {
    let codePostal = $('select[name="idCodepostal"]'),
        paysNaissance = $('select[name="idPaysNaissance"]'),
        paysNationalite = $('select[name="idPaysNationalite"]'),
        ville = $('select[name="idVille"]');

    // NATIONALITE
    paysNaissance.change(function() {
        if ($(this).val()) {
            paysNationalite.val($(this).val()).trigger('change');
        }
    });

    // Codepostal
    codePostal.change(function(){
        if ($(this).val()) {
            ville.children(':not(:first)').remove();
            listOfVille($(this).val());
        }

    });

    listOfPays();
    listOfNationalite();
    listOfPieceIdentite();
    listOfSituationFamille();
    listOfBddPlannet();
    listOfPriseCharge();


}

// LIST OF PAYS
function listOfPays() {
    const FRANCE = 78;

    let pays =  $('select[name="idPays"]');

    const LIST_PAYS = new ListOfParam({
        target: pays,
        name: 'adresse_pays'
    });

    if (URL_CURRENT.match(/^\/salaries\/new$/)) { LIST_PAYS.selectedValue = FRANCE }

}

// LIST OF PAYS DE NAISSANCE
function listOfPaysNaissance() {
    let pays =  $('select[name="idPays"]'),
        paysNaissance = $('select[name="idPaysNaissance"]');

    new ListOfParam({
        target: pays,
        name: 'adresse_pays',
        extra: [paysNaissance],
        onlyExtra: true
    });

}

// LIST OF BDD PLANNET
function listOfBddPlannet() {
    let bddPlannet = $('select[name="idBddPlannet"]');

    new ListOfParam({
        target: bddPlannet,
        name: 'bdd_plannet',
    });

}

// SEARCH DATA FROM DATABASE
function listCodepostal(pays) {
    let codepostal = $('select[name="idCodepostal"]');

    codepostal.select2({
        ajax: {
            url: '/params',
            dataType: 'json',
            data: function(params) {
                return {
                    param: 'adresse_codepostal',
                    filter: {
                        pays: pays,
                        code: params.term
                    }
                }
            },
            processResults: function(data) {
                return { results: typeof data !== "number"
                    ? data.map( d => ({ id: d.idCodepostal, text: d.liCodepostal }) )
                    : null
                }
            }
        },
        minimumInputLength: 5,
        language: 'fr'
    })

}

function listOfVille(codepostal) {
    let ville = $('select[name="idVille"]');

    new ListOfParam({
        target: ville,
        name: 'adresse_ville',
        filter: codepostal
    });

}

function listOfNationalite() {
    let paysNationalite = $('select[name="idPaysNationalite"]'),
        pays = $('select[name="idPays"]');

    new ListOfParam({
        target: pays,
        name: 'adresse_pays',
        libelleName: 'liNationalite',
        extra: [paysNationalite],
        onlyExtra: true
    });

}

function listOfPriseCharge() {
    let typePriseCharge = $('select[name="idTypePriseCharge"]');

    new ListOfParam({
        target: typePriseCharge,
        name: 'type_prise_charge'
    });

}

function listOfSituationFamille() {
    let situationFamille = $('select[name="idSituationFamille"]');

    new ListOfParam({
        target: situationFamille,
        name: 'situation_famille'
    });

}

function listOfPieceIdentite() {
    let typePiece = $('select[name="idTypePiece"]');

    new ListOfParam({
        target: typePiece,
        name: 'type_piece_identite'
    });

}
