function init_remove_salarie() {

    checkSalarieIsInBatch();

}

function checkSalarieIsInBatch() {
    let idMatricule = $('input[name="idMatricule"]').val();

    if (idMatricule) {
        $.getJSON('/batches/salarie/current', { idMatricule: idMatricule } )
            .done(function(data) {

                if (typeof data !== "number") {
                    disableUpdateActionSalarie('salarié', data);
                }

            })
            .fail(function(err) {
                console.error(err);
            });
    }

}

function disableUpdateActionSalarie(liTypeSuppression, objBatch) {

    //***** DISABLED ACTION *****
    let tabDisabledPage = [
            $('#update_form'),
            $('#remove_form'),
            $('.btn-add-contrat'),
            $('.btn-add-irp')
        ],
        tabDisabledMenu = [
            $('.btn-supprimer')
        ],
        block_avertissement_salarie = $('#block_avertissement_salarie');

    addAdvertissementBlock(block_avertissement_salarie, objBatch, liTypeSuppression);
    disabledButtonAndLink(tabDisabledPage);
    disabledButtonMenu(tabDisabledMenu);

}

function removeSalarie() {
    let confirm_msg = '<h5 class="text-center">Une fois que le salarié sera supprimé, il ne sera plus visible dans la liste.<br><br>\n\n' +
        '                <strong>Voulez-vous vraiment supprimer ce salarié ?</strong></h5>',
        inputMessage = '<form action="javascript:void(0)" name="motifsuppression" novalidate>' +
            '<div class="form-group col-md-12 col-sm-12 col-xs-12">' +
            '<label for="liMessage">Motif Suppression</label>' +
            '<input type="text" class="form-control" id="liMessage" name="liMessage"  maxlength="100" required>' +
            '<span class="valid-nebula"></span>' +
            '</div>' +
            '</form>',
        confirm_yes =  $('#confirm_yes');

    MODAL_CONFIRM.find('div.modal-body').html(confirm_msg + inputMessage);
    confirm_yes.prop('disabled', true);
    reloadJS();

    let form_modal = $('form[name="motifsuppression"]');
    form_modal.find('input').on('input change click', function() {
        confirm_yes.prop('disabled', !form_modal[0].checkValidity());
    });

    MODAL_CONFIRM.modal('show');
    MODAL_CONFIRM.prop('action', 'remove-salarie');

    // CONFIRMATION
    confirm_yes.unbind();
    confirm_yes.click(function() {
        if (MODAL_CONFIRM.prop('action') === 'remove-salarie') {
            removeSalarieBatch();
        }
    });

}

// REMOVE SALARIE
function removeSalarieBatch() {
    let idMatriculeMaj = parseInt($('input[name="idMatriculeMaj"]').val()),
        idMatricule = parseInt($('input[name="idMatricule"]').val()),
        liMessage = $('#liMessage').val(),
        opt = {
            idMatricule: idMatricule,
            idMatriculeMaj: idMatriculeMaj,
            idTypeTraitement: 5, // Suppression d'un salarié
            priority: 1, // Urgent
            liMessage: liMessage
        };

    $.getJSON(
        '/salaries/valideur', {
            idMatricule: idMatriculeMaj
        }
    )
        .done(function(data) {
            if (typeof data !== "number" && data.isQualifValideurBatch)
                opt.idMatriculeValideur = idMatriculeMaj;

            batchTraitement(opt);
        })
        .fail(function(err) {
            console.error(err);
            batchTraitement(opt);
        });

}
