function init_menuannexe_salarie() {
    getListOfContracts();

    if (URL_CURRENT.match(/^\/salaries\/detail$/)) {
        getListOfProtection();
        getListOfMandat();
        getLisOftCartePro();
    }
}

async function getListOfContracts() {
    let idMatricule = parseInt($('input[name="idMatricule"]').val()),
        block_contrat = $('#block_contrat_salarie');

    if (idMatricule && block_contrat.is(':visible')) {
        const _data = await $.getJSON( '/salaries/contrats/list', { idMatricule: idMatricule });

        if (typeof _data !== "number") {
            let content_list = $('#content_list_contrat'),
                content_count = $('#count_contrat'),
                link_acompte = $('a.acomptes-list'),
                // link_mutuelle = $('a.mutuelle-list'),
                $list = $.map(_data, function(d) {
                    const obj_contrat = {
                        url: `contrats/detail?idMatricule=${idMatricule}&idContrat=${d.idContrat}`,
                        url_inactif: '',
                        closure: ''
                    };

                    if (d.liEtat === 'Inactif') {
                        obj_contrat.url_inactif = `data-inactif=${obj_contrat.url.replace(/contrats/, 'contrats/inactif')}`;
                        obj_contrat.closure = d.liEtat.toLowerCase();
                        obj_contrat.url = 'javascript:void(0)';
                    }

                    return sprintf('<li>' +
                        '<a %s href="%s"><i class="fa fa-folder %s"></i> %s %s - %s %s</a></li>',
                        obj_contrat.url_inactif,
                        obj_contrat.url,
                        d.dtFincontrat ? 'cloturer' : obj_contrat.closure,
                        d.liTypecontrat,
                        d.liQualifcontrat,
                        d.dtDebutcontrat ? moment(d.dtDebutcontrat).format('DD/MM/YYYY') : '',
                        (d.dtFincontrat ? sprintf('au %s', moment(d.dtFincontrat).format('DD/MM/YYYY')) :
                                d.dtFincontratPrevue ? sprintf('au %s', moment(d.dtFincontratPrevue).format('DD/MM/YYYY')) : ''
                        )

                    );


                }).toString().replace(/,/g, '');

            content_list.html($list);
            content_count.html(sprintf('(%d)', _data.length));

            content_list.find('li a[href="javascript:void(0)"]').each(function() {
                $(this).click(function() {
                    manageContratInactif($(this).data('inactif'));
                })
            });

            if (URL_CURRENT.match(/^\/salaries\/detail$/)) {

                // Verify if exists contrat en current
                if (verifyContratEnCours(_data) === 0) {
                    link_acompte.prop('href', 'javascript:void(0)').find('span').text('0');
                    // link_mutuelle.prop('href', 'javascript:void(0)').find('span').text('0');

                } else {
                    getLisOftAcompte();
                    // getLisOftMutuelle();

                }

            }


        } else { console.log(_data); }

    }

}

function getLisOftCartePro() {
    let idMatricule = parseInt($('input[name="idMatricule"]').val()),
        block_autorisation = $('#block_autorisation_salarie');

    getListOfBlockByIdMatricule({
        id: idMatricule,
        block: block_autorisation,
        name: 'cartepro'
    })

}

function getListOfProtection() {
    let idMatricule = parseInt($('input[name="idMatricule"]').val()),
        block_protection_mandat = $('#block_protection_mandat_salarie');

    getListOfBlockByIdMatricule({
        id: idMatricule,
        block: block_protection_mandat,
        name: 'protection'
    })

}

function getListOfMandat() {
    let idMatricule = parseInt($('input[name="idMatricule"]').val()),
        block_protection_mandat = $('#block_protection_mandat_salarie');

    getListOfBlockByIdMatricule({
        id: idMatricule,
        block: block_protection_mandat,
        name: 'mandat'
    })

}

function getLisOftAcompte() {
    let idMatricule = parseInt($('input[name="idMatricule"]').val()),
        block_paie = $('#block_paie_salarie');

    getListOfBlockByIdMatricule({
        id: idMatricule,
        block: block_paie,
        name: 'acomptes'
    })

}


// function getLisOftMutuelle() {
//     let idMatricule = parseInt($('input[name="idMatricule"]').val()),
//         block_paie = $('#block_paie_salarie');
//
//     getMutuelleCurrent({
//         id: idMatricule,
//         block: block_paie,
//         name: 'mutuelle'
//     })
//
// }

function verifyContratEnCours(contrat) {
    let current = 0;

    if (Array.isArray(contrat) && contrat.length > 0) {

        for (let i=0; i < contrat.length; i++) {
            let date_fin = contrat[i].dtFincontrat ? moment(contrat[i].dtFincontrat) :
                contrat[i].dtFincontratPrevue ? moment(contrat[i].dtFincontratPrevue) : moment().add(1, 'days'),
                diff_date = moment().isBefore(date_fin);

            if (diff_date) { current++; }
        }
    }
    return current;
}

async function getListOfBlockByIdMatricule(opt) {

    if (opt.id && opt.block.is(':visible')) {
        const _data = await $.getJSON(sprintf('/salaries/%s/list', opt.name), { idMatricule: opt.id });

        if (typeof _data !== 'number') {
            let content_count = $(sprintf('#count_%s', opt.name));

            content_count.html(_data.filter(d => d.liEtat !== 'Inactif').length);
        }
    }

}

// async function getMutuelleCurrent(opt) {
//
//     if (opt.id && opt.block.is(':visible')) {
//         const _data = await $.getJSON('/salaries/mutuelles/current', { idMatricule: opt.id });
//
//         if (typeof _data !== "number") {
//             let link_mutuelle = $('a.mutuelle-list');
//
//             if (!_data.hasOwnProperty('idMutuelle')) {
//                 link_mutuelle.prop('href', link_mutuelle.prop('href').replace('liste', 'new'));
//             }
//         }
//     }
// }
