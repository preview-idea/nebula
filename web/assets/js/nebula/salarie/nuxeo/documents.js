function init_document_nuxeo() {
    getDocumentsNuxeo();
    dematisButtonFullScreen();
    downloadDocumentsOnZipFile();
}

async function getDocumentsNuxeo() {
    let idMatricule = parseInt($('input[name="idMatricule"]').val()),
        contentMenu = $('#nuxeoMenu'),
        contentModal = $('#nuxeoModal');

    if (idMatricule) {
        const _docs = await $.get('/salaries/nuxeo/documents', { idMatricule });

        if (typeof _docs !== "number") {
            let sortedDocs = sortObjectByKeyRecursive(_docs);

            contentMenu.append(createFTreeNuxeo(sortedDocs));
            contentModal.append(createFTreeNuxeo(sortedDocs));
            $('ul.file-tree-nuxeo').FTreeMosaic();
        }

    }

}

function dematisButtonFullScreen() {
    let btn_dematis_full_screen = $('.btn-dematis-full-screen');

    btn_dematis_full_screen.unbind();
    btn_dematis_full_screen.click(function() {
        let modal_dematis_full_screen = $('#dematisFullScreen');

        modal_dematis_full_screen.unbind();
        modal_dematis_full_screen.modal('show');

    });
}

function createFTreeNuxeo(docs) {

    if (Object.keys(docs).length) {
        const tree_ul = document.createElement('ul'),
            $folders = Object.keys(docs),
            $sub_folders = Object.keys(docs).map(f => docs[f]);

        for (let i=0; i < $folders.length; i++) {
            let tree_folder = getFoldersToListNuxeo($folders[i]);

            tree_folder.appendChild(getFoldersToListNuxeo($sub_folders[i]));
            tree_ul.appendChild(tree_folder);
        }

        tree_ul.setAttribute('class', 'file-tree-nuxeo');

        return tree_ul;
    }

}

function getFoldersToListNuxeo(folder) {
    const tree_ul = document.createElement('ul');

    if (typeof folder === 'object') {
        for (let prop in folder) {
            if (folder.hasOwnProperty(prop)) {
                let tree_sub_folder = createFoldersToFTreeNuxeo(prop),
                    $files = Object.keys(folder[prop]).map(f => folder[prop][f]);

                if ($files.length) {
                    const file_ul = document.createElement('ul');

                    for (let j=0; j < $files.length; j++) {
                        file_ul.appendChild(createFileFTreeNuxeo($files[j]));
                    }
                    tree_sub_folder.appendChild(file_ul);
                }

                tree_ul.appendChild(tree_sub_folder);
            }
        }
        return tree_ul;

    } else {
        return createFoldersToFTreeNuxeo(folder);
    }

}

function createFoldersToFTreeNuxeo(name) {
    const tree_li = document.createElement('li'),
        tree_a = document.createElement('a'),
        tree_i = document.createElement('i'),
        tree_name = document.createTextNode(name);

    tree_i.setAttribute('class', 'fa fa-folder');

    tree_a.appendChild(tree_i);
    tree_a.appendChild(tree_name);
    tree_a.setAttribute('href', 'javascript:void(0)');

    tree_li.appendChild(tree_a);

    return tree_li;
}

function createFileFTreeNuxeo(file) {
    const tree_li = document.createElement('li'),
        tree_a = document.createElement('a'),
        tree_i = document.createElement('i'),
        tree_name = document.createTextNode(file.name);

    tree_i.setAttribute('class', 'fa fa-file-pdf-o');
    tree_a.setAttribute('data-uid', file.uid);

    tree_a.appendChild(tree_i);
    tree_a.appendChild(tree_name);
    tree_a.setAttribute('href', 'javascript:void(0)');

    tree_li.appendChild(tree_a);

    tree_li.addEventListener('click', function() {
        previewDocuemntNuxeo(file.href, file.name)
    });

    return tree_li;
}

function previewDocuemntNuxeo(nuxeoPath, filename) {

    if (nuxeoPath) {
        const pdf_url = URL_CURRENT.replace(/(inactif\/)?detail$/, sprintf('nuxeo/documents/preview?nuxeoPath=%s', nuxeoPath)),
            pdf_object = sprintf('<object type="application/pdf" data="%s#toolbar=1&navpanes=1&scrollbar=0" width="100%" ></object>', pdf_url);

        $.createModalPDF({
            name: 'prevModalDocumentNuxeo',
            title: filename,
            message: pdf_object,
            closeButton: true,
            scrollable: false
        });

    }
}

function downloadDocumentsOnZipFile() {
    let idMatricule = parseInt($('input[name="idMatricule"]').val()),
        btn_doc_zip = $('a.btn-dematis-doc-zip');

    btn_doc_zip.unbind();
    btn_doc_zip.click(() => {
        let content_doc = $('div#block_dematis_salarie ul.file-tree-nuxeo');

        if (content_doc.children().length) {
            notifyNebula('Le téléchargement sera fait automatiquement dans quelques secondes!', 'Dematis zip', 'info');
            location.assign(`/salaries/nuxeo/documents/zipfile?idMatricule=${idMatricule}`);
        }
    })
}