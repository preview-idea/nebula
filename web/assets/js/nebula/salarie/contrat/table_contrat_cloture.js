function init_table_contrat_cloture() {

    init_list_year_contrat_cloture();
    enableFilterContratCloture();
    create_table_contrat_cloture();

}

function init_list_year_contrat_cloture() {
    let idYear = $('#idYear'),
        year_debut = moment().subtract(2, 'years').year(),
        year_current = moment().add(2, 'years').year();

    for (;year_debut <= year_current; year_debut++) {
        idYear.append(new Option(year_debut.toString(), year_debut.toString()));
    }

}

function enableFilterContratCloture() {
    let btn_filter = $('#filter-contrat'),
        filter_contrat = $('form[name="filter_contrat"]'),
        idMonth = $('#idMonth'),
        idYear = $('#idYear');

    idMonth.change(function() {
        btn_filter.prop('disabled', !filter_contrat[0].checkValidity());
    });

    idYear.change(function() {
        btn_filter.prop('disabled', !filter_contrat[0].checkValidity());
    });
}

function create_table_contrat_cloture() {
    let btn_filter = $('#filter-contrat'),
        filter_contrat = $('form[name="filter_contrat"]'),
        table_contrat =  $("#datatable-contrat");

    btn_filter.unbind();
    btn_filter.click(function () {

        if (filter_contrat[0].checkValidity()) {

            let dataTab = [];

            const obj_form = traitCommonElements(filter_contrat.find('select'));

            $.post(
                '/salaries/contrats/cloture',
                obj_form
            )
                .done(data => {
                    if (typeof data !== "number") {

                        // FORMAT FOR DATATABLES
                        data.forEach(function (d) {

                            dataTab.push([
                                d.idMatricule,
                                d.idContrat,
                                d.idAgenceRhpi,
                                d.liAgence,
                                d.liNomComplet,
                                d.liMotiffincontrat,
                                d.dtDebutcontrat ? new Date(d.dtDebutcontrat).format('d/m/Y') : '',
                                d.dtFincontrat ? new Date(d.dtFincontrat).format('d/m/Y') : '',
                                processCloture(d.isPlanningCloture),
                                d.idMatricule,
                                d.idContrat,
                                d.isActif
                            ]);

                        });

                        const TABLE_CONTRAT_CLOTURE = table_contrat.DataTable({
                            destroy: true,
                            data: dataTab,
                            order: [[2, 'asc']],
                            columnDefs: [{
                                targets: [1,2,3],
                                render: $.fn.dataTable.render.ellipsis( 20, true )
                            }]
                        });

                        table_contrat.find('tbody').on('dblclick', 'tr', function(){

                            let data = TABLE_CONTRAT_CLOTURE.row( this ).data();
                            if (data)
                                (data[9])
                                    ? window.open(URL_CURRENT.replace('/cloture', '') + sprintf('/detail?idMatricule=%d&idContrat=%d', data[7], data[8]))
                                    : manageContratInactif(URL_CURRENT.replace('/cloture', '') + sprintf('/inactif/detail?idMatricule=%d&idContrat=%d', data[7], data[8]));

                        });

                        openOneRowInDataTable(table_contrat);

                    }
                    else if (data === 404) {

                        table_contrat.DataTable({
                            destroy: true,
                            data: []
                        });
                    }
                })
                .fail(err => console.error(err) )

        }

    });

}
