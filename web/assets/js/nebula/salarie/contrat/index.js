$(function() {

    if (URL_CURRENT.match(/^\/(salaries\/contrats\/(detail|validate))?$/)) {
        // extra/send_email.js
        init_send_email_contrat();
    }

    //script pour toutes les créations à partir du salarié
    if (URL_CURRENT.match(/^\/salaries\/contrats\/avenants\/(new|validate)$/)) {

        // redirect.js
        init_redirect_to_contrat_if_in_batch();

    }

    // Scripts en common pour les contrats et avenants
    if (URL_CURRENT.match(/^\/salaries\/contrats\//)) {

        // combi.js
        init_combi_contrat();

        // preview.js
        init_preview_contrat();

        // periode_des.js
        init_periode_des_contrat();

    }

    // Script pour le tableau du contrat
    if (URL_CURRENT.match(/^\/salaries\/contrats$/)) {

        // table_contrat.js
        init_table_contrat();

    }

   
    // Script pour le tableau du contrat cloture
    if (URL_CURRENT.match(/^\/salaries\/contrats\/cloture$/)) {

        // table_contrat_cloture.js
        init_table_contrat_cloture();

    }

    // Scripts uniquement pour la page new, detail et validate
    if (URL_CURRENT.match(/^\/salaries\/contrats\/(new|detail|validate)/)) {

        // fiche_contrat.js
        init_fiche_contrat();

        // download.js
        init_download_contrat();

    }

});