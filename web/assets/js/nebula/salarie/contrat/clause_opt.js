function init_clause_opt_contrat() {
    let listClauseOpt = $('#clausescontrat').find(':checkbox');

    // LIST OF CLAUSES
    listClauseOpt.each(function(){
        $(this).on('ifChanged', function() {
            if ($(this).is(':checked') && !$(this).is(':disabled')) {

                addElements(this);

            } else {
                if (!$(this).is(':disabled')) {
                    $('form[name="clause_opt_' + this.id + '"]').parent().remove();
                }

                // Réactive la validation du formNebula.js
                enableValidationContrat();

            }
        })

    });

}


//=== ADD TYPE PRIME ====
function addElements(element) {

    $.post('/salaries/contrats/block/clause_opt', {
        clause_opt: {
            idClausescontrat: {
                idClausescontrat: element.id,
                liClausescontrat: element.name,
                isInfoSupp: element.value
            }
        }
    })
    .done(function(resp, stats){
        if (stats === 'success') {
            let content = $('#listClauseOpt'),
                hasDouble = false;

            // STOP THE DOUBLE CONTENT
            content.find('form').each(function(){
                if (this.name === $(resp).find('form').prop('name')) {
                    return hasDouble = true;
                }
            });

            if (!hasDouble) { content.append(resp);  }

            if (parseInt(element.id) === 10) {
                getListDeditFormation();
                deditFormation();
            }

            // Réactive la validation du formNebula.js
            enableValidationContrat();

            reloadJS();
        }

    });
}

// CLAUSE DEDIT-FORMATION
function deditFormation(){
    let coutDeditFormation = $('input[name="nbCoutDeditFormation"]'),
        liCoutDeditFormation = $('input[name="liCoutDeditFormation"]');

        coutDeditFormation.on('blur', function() {

            convertNumber({
                value: $(this).val(),
                target: liCoutDeditFormation
            });

            chargeFormLabel();
        });


}

function getListDeditFormation() {
    let qualifcontrat = $('select[name="idQualifcontrat"]'),
        deditformation = $('select[name="idQualifDeditFormation"]');

    new ListOfParam({
        target: qualifcontrat,
        name: 'qualifcontrat',
        extra: [deditformation],
        onlyExtra: true
    });
}
