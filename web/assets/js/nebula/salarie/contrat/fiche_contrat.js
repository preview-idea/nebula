if (URL_CURRENT.match(/contrats\/(new|detail|validate)$/)) {

    // Instance form nebula contrat
    INS_FN_CONTRAT = new FormNebula({
        objLinked: ['repartition', 'primes', 'clause_add', 'clause_opt'],
        except: ['analytique', 'duplicate_sheet', 'cloture_contrat'] // forms exception
    });
}

function init_fiche_contrat() {

    // Enable validation Form Nebula used for a save/duplicate contract in the page /salaries/contrats/new.
    enableValidationContrat();

    // Update contrat
    if (URL_CURRENT.match(/contrats\/(detail|validate)/)) {

        INS_FN_CONTRAT.update.click(function() {
            let idContrat = $('input[name="codeContrat"]').val();

            getModalModifyDtSituation(idContrat);

        });

    }

    // Redirection
    if (URL_CURRENT.match(/contrats\/(new|validate)$/)) {

        INS_FN_CONTRAT.redirection = function(_resp) {
            let idMatricule = _resp.idMatricule.idMatricule,
                idMatriculeMaj = _resp.idMatriculeMaj.idMatricule,
                idContrat = _resp.idContrat;

            if (idMatricule && idMatriculeMaj && idContrat) {

                // ADD REQUEST IN BATCH.
                batchTraitement({
                    idMatricule: idMatricule,
                    idMatriculeMaj: idMatriculeMaj,
                    idContrat: idContrat,
                    idTypeTraitement: 1, // Generation d'un contrat
                    priority: 1 // Urgent
                });

            } else {
                console.log(_resp);
                return notifyNebula('Demande de génération impossible. Contactez le support', 'Server error 500', 'error');
            }

        }
    }
}

function getModalModifyDtSituation(idContrat) {
    let form_block_info = $('form[name="block_info"]'),
        dtSituation = form_block_info.find('input[name="dtSituation"]'),
        modal_content = '<form action="javascript:void(0)" name="modify_dtSituation" novalidate>' +
            '<div class="form-group text-center col-xs-10" >' +
                '<label for="dtSituation">Date de situation</label>' +
                `<input type="text" class="form-control" name="dtSituation" id="dtSituation" maxlength="10" pattern="\\d{2}\\/\\d{2}\\/\\d{4}" required>` +
                '<i class="fa fa-calendar"></i><span class="valid-nebula"></span>' +
            '</div>' +
        '</form>',
        confirm_yes = $('#confirm_yes');

    MODAL_CONFIRM.find('div.modal-body').html(modal_content);
    confirm_yes.prop('disabled', true);
    reloadJS();

    MODAL_CONFIRM.modal('show');
    MODAL_CONFIRM.prop('action', 'modify-dtSituation');

    let form_modal = $('form[name="modify_dtSituation"]');
    form_modal.find('input[name="dtSituation"]').on('input change', function() {

        // Set the new value to input dtSituation
        dtSituation.val($(this).val());
        confirm_yes.prop('disabled', !form_modal[0].checkValidity());

    });

    // CONFIRMATION
    confirm_yes.unbind();
    confirm_yes.click(function() {
        if (MODAL_CONFIRM.prop('action') === 'modify-dtSituation') {
            if (INS_FN_CONTRAT.isOk === true) { INS_FN_CONTRAT.updateForm(idContrat); }
        }
    });
}