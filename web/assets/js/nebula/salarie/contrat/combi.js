function init_combi_contrat() {
    if (URL_CURRENT.match(/contrats\/(new|detail|validate)/)
        || URL_CURRENT.match(/avenants\/(new|validate)/)
    ) {
        let positionnement = $('select[name="idPositionnementposte"]'),
            societe = $('select[name="idSociete"]'),
            agence = $('select[name="idAgence"]'),
            typeContrat = $('select[name="idTypecontrat"]'),
            tempsTravail = $('select[name="idTempsTravail"]'),
            natureContrat = $('select[name="idNaturecontrat"]'),
            decompteTravail = $('select[name="idDecompteTempsTravail"]'),
            etablissement = $('select[name="idEtablissement"]');

        /** TYPE OF CONTRACT */
        if (typeContrat.val()) { getListNatureContrat(typeContrat.val()); }

        typeContrat.on('change', function() {
            if ($(this).val()) {
                natureContrat.children(':not(:first)').remove();
                getListNatureContrat($(this).val());
                resetBlockClassification();
            }

        });

        /** AGENCE AND DECOMPTE DE TRAVAIL */
        if (societe.val()) {
            getListAgence(societe.val());
            if (tempsTravail.val()) { getListDecomptetempstravail(tempsTravail.val(), societe.val()) }
        }

        societe.change(function() {
            if ($(this).val()) {
                if (positionnement.val()) {
                    agence.children(':not(:first)').remove();  // CLEAR OLD LIST
                    getListAgence($(this).val());
                }

                if (tempsTravail.val()) {
                    decompteTravail.children(':not(:first)').remove();
                    getListDecomptetempstravail(tempsTravail.val(), $(this).val());
                }
            }

        });

        /** DECOMPTE DE TRAVAIL */
        if (tempsTravail.val() && societe.val()) { getListDecomptetempstravail(tempsTravail.val(), societe.val()) }
        tempsTravail.change(function() {
            if ($(this).val()) {
                if (societe.val()) {
                    decompteTravail.children(':not(:first)').remove();
                    getListDecomptetempstravail($(this).val(), societe.val());
                }
            }
        });

        /** ETABLISSEMENT */
        if (agence.val() && societe.val()) { getListEtablissement(agence.val(), societe.val()); }
        agence.change(function() {
            if ($(this).val() && societe.val()) {
                etablissement.children(':not(:first)').remove(); // CLEAR OLD LIST
                getListEtablissement($(this).val(), societe.val());
            }
        });

    }

}

// LIST OF NATURE CONTRAT
function getListNatureContrat(idTypecontrat) {
    let nature = $('select[name="idNaturecontrat"]');

    new ListOfCombi({
        target: nature,
        name: 'relation_type_nature',
        filter: idTypecontrat
    });
}

// LIST OF COEFF CONTRAT
function getListCoeffContrat(categorie) {
    let coeff = $('select[name="idCoeffcontrat"]'),
        convention = $('select[name="idConventioncollective"]');

    new ListOfCombi({
        target: coeff,
        name: 'categorie_coeff',
        customData: ['nbSalaireMensuel'],
        filter: { categorie: parseInt(categorie.val()), convention: parseInt(convention.val()) }
    });
}

// LIST OF AGENCE
function getListAgence(idSociete) {
    let agence = $('select[name="idAgence"]');

    new ListOfCombi({
        target: agence,
        name: 'societe_etablissement_agence',
        filter: { societe: parseInt(idSociete) }
    });
}

// LIST OF ETABLISSEMENT
function getListEtablissement(idAgence, idSociete) {
    let etablissement = $('select[name="idEtablissement"]');

    new ListOfCombi({
        target: etablissement,
        name: 'societe_etablissement_agence',
        filter: { agence: parseInt(idAgence), societe: parseInt(idSociete) }
    });
}

// LIST OF DECOMPTE TEMPS TRAVAIL
function getListDecomptetempstravail(tempsTravail, idSociete) {
    let decompte = $('select[name="idDecompteTempsTravail"]');

    new ListOfCombi({
        target: decompte,
        name: 'comete_temps_travail',
        filter: { tempsTravail: parseInt(tempsTravail), societe: parseInt(idSociete) }
    });
}

// RESET BLOC CLASSIFICATION
function resetBlockClassification() {
    let form_classific = $('form[name="classification"]');

    if (window.location.pathname.match(/contrats\/(new|detail|validate)$/)) {
        form_classific.resetFormBlock();
    }
}

