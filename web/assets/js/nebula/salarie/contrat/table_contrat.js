function init_table_contrat() {

    createTableForContrat();

}

function createTableForContrat() { "use strict";
    let tableContrat =  $("#datatable-contrat"),
        contrats = tableContrat.data('table-contrats'),
        dataTab = [];

    if (Array.isArray(contrats) && contrats.length) {

        // FORMAT FOR DATATABLES
        // Pour ne pas générer des dates avec des valeurs NULL, on les passees par une condition.
        contrats.forEach(function(d) {
            dataTab.push([
                d.idMatricule,
                d.idContrat,
                d.liNomComplet,
                d.liTypecontrat,
                d.liNaturecontrat,
                d.liQualifcontrat,
                d.dtDebutcontrat ? new Date(d.dtDebutcontrat).format('d/m/Y') : '',
                d.dtFincontrat ? new Date(d.dtFincontrat).format('d/m/Y') :
                    d.dtFincontratPrevue ? new Date(d.dtFincontratPrevue).format('d/m/Y') : '',
                d.liNomSignataire
            ]);
            
        });// forEach

        // SHOW CONTENT
        var table = tableContrat.DataTable({
            data: dataTab,
            columnDefs: [{
                targets: [2,3,4,5,8],
                render: $.fn.dataTable.render.ellipsis( 20, true )
            }]
        });

        tableContrat.find('tbody').on('dblclick', 'tr', function(){

            var data = table.row( this ).data();
            window.location.assign(URL_CURRENT + sprintf('/validate?idMatricule=%d&idContrat=%d', data[0], data[1]));
            NProgress.inc();

        });

        openOneRowInDataTable(tableContrat);

    }

}