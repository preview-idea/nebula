function init_preview_contrat() {

    // Button pré-visualiser
    $('#prev_form').on('click', function(){
        let data = $('form:visible'),
            contrat = {},
            primes = [],
            clause_opt = [],
            repartition = [],
            forms = [];

        // ENABLE BUTTON SEND AND VALID
        enableButtonSend();

        // Recupère les informations saisient sur le formulaire
        for (let i=0; i < data.length; i++) {
            if (!data[i].name.match(/block_info/)) {

                if (!isEmpty(traitPreviewElements(data[i]))) {
                    if (data[i].name.match(/^clause_opt_/)) {
                        clause_opt.push(traitObjects(data[i]));

                    } else if (data[i].name.match(/^primes_/)) {
                        primes.push(traitObjects(data[i]));

                    } else if (data[i].name.match(/^repartition_/)) {
                        repartition.push(traitObjects(data[i]));

                    } else {
                        forms[data[i].name] = traitPreviewElements(data[i]);
                    }

                }

            }

        } // for

        if (primes.length > 0) {
            forms['primes'] = primes;
        }

        if (clause_opt.length > 0) {
            forms['clause_opt'] = clause_opt;
        }

        // if (repartition.length > 0) {
        //     forms['repartition'] = repartition;
        // }

        // reset position slider
        $('.carousel').carousel({}).carousel(0);

        contrat.forms = forms;
        return  previewForm(contrat.forms);

    });

}

function enableButtonSend() {
    let  send_form = $('#send_form'),
        valid_form = $('#valid_form'),
        chevron_right = $('.carousel-control.right'),
        chevron_left = $('.carousel-control.left'),
        indicators = $('ol.carousel-indicators li');

    // Disable button send_form
    send_form.prop('disabled', true);
    valid_form.prop('disabled', true);


    // IF ARROW OF MODAL IS CLICKED
    chevron_right.on('click', function() {

        setTimeout(function(){
            let right = $('div.item.active').index();

            if (parseInt(right) === 2) {
                send_form.prop('disabled', false);
                // valid_form.prop('disabled', false);
            }

        },1000)


    });

    chevron_left.on('click', function(){

        setTimeout(function(){
            let left = $('div.item.active').index();

            if (parseInt(left) === 1) {
                send_form.prop('disabled', false);
                // valid_form.prop('disabled', false);
            }

        }, 1000)

    });

    // IF INDICATORS OF MODAL IS CLICKED
    let stat = {};
    indicators.on('click', function(){

        switch ($(this).index()) {
            case 1:
                stat.first = true;
                break;
            case 2:
                stat.second = true;
                break;
        }

        if (stat.first === true && stat.second === true) {
            send_form.prop('disabled', false);
            // valid_form.prop('disabled', false);

        }

    });


}

function traitObjects(e) {
    let r = {};

    for (let i = 0; i < e.length; i++) {
        if (e[i].value || (e[i].type === 'select-one' && e[i].options[e[i].selectedIndex].value)) {

            if (e[i].type === 'checkbox') {
                r[e[i].name] = (e[i].checked) ? 'Oui' : 'Non';
                r['la' + e[i].name.substr(2, 100)] = getLabelElement(e[i]);

            } else if (e[i].name.match(/^idClausescontrat/)) {
                r['title'] = getCheckboxText('clause_opt_' + e[i].value);

            } else if (e[i].name.match(/^idPrimecontrat/)) {
                r['title'] = getCheckboxText('primes_' + e[i].value);

            } else if (e[i].name.match(/^id/)) {
                e[i].type === 'select-one' ?
                    r[e[i].name] = getOptionsText(e[i]) :
                    r[e[i].name] = e[i].value;

                r['la'+ e[i].name.substr(2,100)] = getLabelElement(e[i]);

            } else {
                r[e[i].name] = e[i].value.replace('.',',');
                r['la'+ e[i].name.substr(2,100)] = getLabelElement(e[i]);

            }
        }
    } // for

    return r

}

function traitPreviewElements(e) {
    let r = {};

    for (let i = 0; i < e.length; i++) {

        if (
            (e[i].value && ['liSalaireBase'].indexOf(e[i].name) === -1) ||
            (e[i].type === 'select-one' && e[i].options[e[i].selectedIndex].value)
        ) {

            if (e[i].type === 'checkbox') {
                r[e[i].name] = (e[i].checked) ? 'Oui' : 'Non';
                r['la'+ e[i].name.substr(2,100)] = getLabelElement(e[i]);

            } else if (e[i].name.match(/^id/)) {
                (e[i].type === 'select-one') ?
                    r[e[i].name] = getOptionsText(e[i]) :
                    r[e[i].name] = e[i].value;

                r['la'+ e[i].name.substr(2,100)] = getLabelElement(e[i]);

            } else {
                r[e[i].name] = e[i].value.replace('.',',');
                r['la'+ e[i].name.substr(2,100)] = getLabelElement(e[i]);

            }

        }


    } // for

    return r;
}

function previewForm(forms) {
    let div_content = $('#prevFormContrat div.item'),
        tail = Object.keys(forms).length,
        limit =  3,
        media = tail / limit,
        count = 0;

    div_content.each(function(){
        $(this).html('');

        for (let cont=0; cont < media; cont++ ) {
            if (count < tail) {

                // Title du formulaire
                $(this).append('    <h4 style="color: #B21C12">' + getTitleForm(Object.keys(forms)[count]) + '</h4>');
                $(this).append('<div class="clearfix"></div>');

                // Elements du formulaire
                if (Array.isArray(Object.values(forms)[count])) {
                    let form = Object.values(forms)[count];

                    // Primes et Clauses optionnelles
                    for (let i=0; i < form.length; i++) {
                        let elemens = getContentForm(form[i]),
                            content = '    <div class="col-md-6 col-sm-6 col-xs-12">\n';
                            content += '        <p style="font-weight: 600; text-decoration: underline">' + form[i]['title'] + '</p>\n';
                            content += '        <ul>\n';

                        for (let index in elemens) {
                            if (elemens.hasOwnProperty(index)) {
                                content += '           <li>' + index + ' : <strong>' +  elemens[index] +'</strong></li>\n';
                            }

                        } // for
                        content += '        </ul>\n';
                        content += '    </div>';
                        $(this).append(content);
                    } // for


                } else {
                    // Elements commons
                    let elems =  getContentForm(Object.values(forms)[count]);
                    for (let prop in elems) {
                        if (elems.hasOwnProperty(prop)) {
                            $(this).append('    <div class="col-md-6 col-sm-6 col-xs-12">\n' +
                                '        <p>' + prop + ' : <strong>' +  elems[prop] +'</strong></p>\n' +
                                '    </div>'
                            );
                        }
                    }
                }

                $(this).append('<div class="clearfix"></div>');
                count++;
            }
        } // for

        // INSERT HR TO DIVISION
        let titles = $(this).find('h4').not(':first');
        titles.each(function() {
            $(this).before('<hr>');
        });


    });

}

function getTitleForm(name) {
    let blocks = $('div.x_title h2');

    // Recupère les noms des blocks
    for (let x=0; x < blocks.length; x++) {
        let reg = new RegExp('^' + name, 'i');

        if (removeSpecialCharacters(blocks[x].innerText).match(reg)) {
            return blocks[x].innerText;

        } else if (removeSpecialCharacters(blocks[x].innerText, true).match(reg)) {
            return blocks[x].innerText;
        }

    } // for
}

function getContentForm(form) {
    let keys = [],
        values = [],
        elements = {};

    for (let prop in form) {
        if (form.hasOwnProperty(prop)) {

            if (prop.match(/^la/)) {
                keys.push(form[prop]);

            } else if (prop.match(/^(id|is|dt|nb|li)/)) {
                values.push(form[prop]);

            }
        }
    } // for

    for (let x=0; x < keys.length; x++) {
        elements[keys[x]] = values[x];
    }
    return elements;

}
