function init_preview_salarie() {
    openModalPreviewBlock();
}

function openModalPreviewBlock() {
    let _btn_preview = $('a.btn-preview-block');

    _btn_preview.unbind();
    _btn_preview.click(function() {
        let block = $(this).parents('div.x_panel'),
            title = block.find('div.x_title h2').text(),
            form = block.find('form'),
            trait_form = traitObjectsForPreviewBlockInSalarie(form[0].elements),
            content = contentPreviewFormInSalarie(trait_form);

        $.createModalInfo({
            title: `Prévisualisation du bloc ${title}`,
            content
        });

    })
}

function traitObjectsForPreviewBlockInSalarie(e) {
    let r = {};

    for (let i = 0; i < e.length; i++) {
        if (e[i].value || (e[i].type === 'select-one' && e[i].options[e[i].selectedIndex].value)) {

            if (['checkbox', 'radio'].indexOf(e[i].type) !== -1) {

                if ((e[i].type === 'radio' && ['M', 'Mme'].indexOf(getLabelElement(e[i])) !== -1) && e[i].checked) {

                    parseInt(e[i].value) === 1
                        ? r[e[i].name] = 'Monsieur'
                        : r[e[i].name] = 'Madame';

                    r['la' + e[i].name.substr(2, 100)] = 'Civilité';

                } else if (e[i].type === 'checkbox') {
                    r[e[i].name] = e[i].checked ? 'Oui' : 'Non';
                    r['la' + e[i].name.substr(2, 100)] = getLabelElement(e[i]);
                }

            } else if (e[i].name.match(/^id/)) {
                e[i].type === 'select-one' ?
                    r[e[i].name] = getOptionsText(e[i]) :
                    r[e[i].name] = e[i].value;

                r['la'+ e[i].name.substr(2,100)] = getLabelElement(e[i]);

            } else {
                r[e[i].name] = e[i].value.replace('.',',');
                r['la'+ e[i].name.substr(2,100)] = getLabelElement(e[i]);

            }
        }
    } // for

    return r

}

function contentPreviewFormInSalarie(form) {
    let _elements = getContentForm(form),
        _content = '<div class="clearfix"></div>';

    _content += '    <div class="col-xs-offset-1 col-xs-11">\n';

    for (let prop in _elements) {
        if (_elements.hasOwnProperty(prop)) {
            _content += '    <div class="col-md-6 col-sm-6 col-xs-12">\n' +
                '        <p>' + prop + ' : <strong>' +  _elements[prop] +'</strong></p>\n' +
                '    </div>';
        }
    }
    _content += '    </div>';
    _content += '<div class="clearfix"></div>';

    return _content;
}