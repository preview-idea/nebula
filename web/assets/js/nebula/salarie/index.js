$(function() {

    //Script accueil
    if (URL_CURRENT.match(/^\/$/)) {

        // accueil.js
        init_accueil();

        // message_alert.js
        init_message_alert();

    }

    // Scsript indicateur
    if (URL_CURRENT.match(/^\/salaries\/indicateurs$/) || URL_CURRENT.match(/^\/$/)) {

        // indicators.js
        init_indicators_salarie();
    }

    //script pour toutes les créations/validations à partir du salarié
    if (URL_CURRENT.match(/^\/salaries\/(cartepro|contrats(\/avenants)?|irp|acomptes)\/(new|validate)/)) {

        // redirect.js
        init_redirect_to_salarie_if_in_batch();

    }

    // Script pour le tableau du salarié
    if (URL_CURRENT.match(/^\/salaries$/)) {

        // table_salarie.js
        init_table_salarie();

    }

    // Scripts uniquement pour la fiche salarié
    if (URL_CURRENT.match(/^\/salaries\/(new|detail)/)) {

        if (!$('input[name="idNirDef"]').val()) {

            // control_nir.js
            init_control_nir_salarie()

        } else {

            // menu_info.js
            init_menu_info_salarie();

            // extra/upload_image.js
            init_upload_image_salarie();

            // identite.js
            init_identite();

            // coordonnees.js
            init_coordonnees_salarie();

            // justificatif.js
            init_justificatif();

            // page_detail.js
            init_page_detail_salarie();

            // personnes.js
            init_personnes_salarie();

            // enfants.js
            init_enfants_salarie();

            // rib.js
            init_rib_salarie();

            // preview.js
            init_preview_salarie();

            // params.js
            init_params_salarie();

            // fiche_salarie.js
            init_fiche_salarie();

        }

    }

    // Scripts uniquement pour la fiche salarié (actif ou inactif)
    if (URL_CURRENT.match(/^\/salaries(\/inactif)?\/detail/)) {

        // menuannexe.js
        init_menuannexe_salarie();

    }

    // Scripts uniquement pour la page détail d'un salarié
    if (URL_CURRENT.match(/^\/salaries\/detail$/)) {

        // extra/logs.js
        init_logs_salarie();

        // extra/histo_salarie.js
        init_histo_salarie();

        // remove.js
        init_remove_salarie();

    }

    // Scripts pour la réactivation du salarié
    if (URL_CURRENT.match(/^\/salaries\/(inactif\/detail|reactive)$/)) {

        // coordonnees.js
        init_coordonnees_salarie();

        // identite.js
        init_identite();

        // extra/reactive.js
        init_reactive_salarie();

        // preview.js
        init_preview_salarie();

    }

    // Script pour la visualisation de la protection sur toutes les photos
    if (URL_CURRENT.match(/^\/salaries/)) {

        // extra/visuel_protection.js
        init_visuel_protection();
    }

    // Script pour le tableau du salarié
    if (URL_CURRENT.match(/^\/salaries\/horscee$/)) {

        // table_salarie_hors_cee.js
        init_table_salarie_hors_cee();

    }

    // Script pour le statut du salarié dans le block_info sur tous les pages
    if (!URL_CURRENT.match(/^\/salaries\/new$/) && URL_CURRENT.match(/(new|detail|liste|validate)$/)) {

        // extra/status.js
        init_status_salarie();

    }

    // Script pour le tableau des demandes batch
    if (URL_CURRENT.match(/^\/salaries\/demandes\/indicateur$/)) {

        init_table_batch();

    }

});

