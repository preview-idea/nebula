function init_identite() {

    inputMaskForIdNir();

    autoCompletNIR();
    blockInfoNir();
    dateBirthDay();
    liPrenomWithoutAccent();
    traitDepartement();
    traitVilleNaissance();
    autoCompletNom();

}

// BLOCK INFOS NIR VALID
function blockInfoNir() {
    let nir = $('input[name="idNirDef"]'),
        isProv = $('input[name="isNirProv"]');

    if (window.location.pathname.match(/salaries\/new/)) {
        disableCheckbox(isProv);
        disableInput(nir);

    } else {
        if (!isProv.is(':checked')) {
            disableCheckbox(isProv);
            disableInput(nir);
        }
    }
}

// COMPLET NOM USAGE
function autoCompletNom() {
    let nom = $('input[name="liNom"]'),
        nom_usage = $('input[name="liNomUsage');

    // REMOVE ACCENT AND UPPERCASE
    majRemoveAccent([$('input[name^="liNom"]'), nom_usage]);

    // AUTO COMPLETE NOM USAGE
    nom.on('input blur', function(){
        nom_usage.val(this.value);
        chargeFormLabel();
    });
}

// INPUT (Date naissance et âge).
function dateBirthDay() {
    let _x = $('input[name="dtNaissance');

    if (_x.val()) { showAge(_x.val()) }

    _x.on("change", function(){

        if ($(this).val()) {
           showAge(this.value);
        }
    });

}

function majRemoveAccent(elems) {

    if (Array.isArray(elems) && elems.length) {

        elems.forEach(function(elem) {

            elem.on('input blur', function() {
                $(this).val(function() {
                    let n =  removeSpecialCharacters($(this).val());
                    return n.toUpperCase();
                });
            });

        });

    } else if (elems instanceof jQuery) {

        elems.on('input blur', function() {
            $(this).val(function() {
                let n =  removeSpecialCharacters($(this).val());
                return n.toUpperCase();
            });
        });

    }


}

// Show Age
function showAge(dt_naissance) {

    if (dt_naissance.isFormatDateView()) {
        let dt = moment(dt_naissance.convertToDate()),
            years = moment().diff(dt, 'years');

        $('#age').val(years + ' ans');
        chargeFormLabel();
    }

}

// Remove Accents input liPrenom
function liPrenomWithoutAccent() {
    let prenoms = $('input[name^="liPrenom"]');

    prenoms.each(function() {
        let $prenom = $(this);

        $prenom.on('input blur', function() {
            $(this).val(function() {
                let p = removeSpecialCharacters($(this).val());

                return p.replace(/^([a-zA-Z])|[\s,-]+([a-zA-Z])/g, function($1) {
                    return $1.toUpperCase();
                })
            })

        })
    })

}

// Departement de naissance
function traitDepartement() {
    let depart = $('input[name="liDepNaissance"]'),
        paysNaissance = $('select[name="idPaysNaissance"]'),
        isProv = $('input[name="isNirProv"]'),
        Etranger = 99,
        exception_pays = [15,78,151,235],
        France = 78;

    if (isProv.is(':checked')) {
        paysNaissance.change(function() {
            if ($(this).val()) {
                parseInt($(this).val()) !== France
                    ? depart.val(function() { return Etranger; })
                    : depart.val('');

                chargeFormLabel();
            }
        });
    }

    depart.on('input blur',function(){
        let current = $(this).val(),
            old_val;

        if ($(this).val()) {
            if (!paysNaissance.val()) {
                paysNaissance.focus();
                return notifyNebula("Vous devez d'abord renseigner le pays de naissance", 'Pays de naissance', 'info');
            }

            if (paysNaissance.val() && exception_pays.indexOf(parseInt(paysNaissance.val())) === -1) {
                depart.val(Etranger);

            } else {
                if (current < Etranger || current.match(/^2[aAbB]/)) { old_val = current.toUpperCase(); }
                $(this).val(old_val);
            }
            chargeFormLabel();
        }

    });

}

function autoCompletNIR() {
    let isProv = $('input[name="isNirProv"]'),
        nirDef = $('input[name="idNirDef"]').val(),
        civil = $('form[name="identite"] input[name="idCivilite"]'),
        depart = $('input[name="liDepNaissance"]'),
        paysNaiss = $('select[name="idPaysNaissance"]'),
        dtNaiss = $('form[name="identite"] input[name="dtNaissance"]'),
        France = 78,
        exception_depart = [91, 92, 93, 94, 95, 96],
        Etranger = 99;

    const NIR = nirDef.replace(/\s+/g, ''),
        DEPART = getDepartmentForAutoCompletNir(NIR.charAt(5), NIR.charAt(6));

    if (!isProv.is(':checked')) {

        if (URL_CURRENT.match(/salaries\/new/)) {

            // CIVILITE
            civil.each(function() {
                if ($(this).val() === NIR.charAt(0)) {
                    if (!$(this).is(':checked')) { $(this).iCheck('check'); }
                }
            });

            // DEPARTEMENT
            depart.val(DEPART);

            // PAYS DE NAISSANCE
            if (DEPART !== Etranger) {
                if (exception_depart.indexOf(DEPART) !== -1) {
                    let exception_pays = [15, 78, 151, 235]; // Par ordre on a : Algérie, France, Maroc et Tunisie

                    listOfPaysNaissance();
                    paysNaiss.on('select2:opening', function() {
                        paysNaiss.find('option').not(':first').map((index, opt) => {
                            if (exception_pays.indexOf(parseInt(opt.value)) === -1) {  $(opt).remove();  }
                        })
                    })

                } else {
                    paysNaiss.append(new Option('France', France.toString(), true, true)).trigger('change');
                }

            } else {
                listOfPaysNaissance();
                paysNaiss.on('select2:opening', function() {
                    paysNaiss.find(sprintf('option[value="%s"]', France )).remove();
                });
            }

            chargeFormLabel();
        }

        disableCheckbox(civil);
        disableInput(depart);

        // BLOCK DATE DE NAISSANCE
        let dt_naiss_nir = Number(NIR.charAt(3) + NIR.charAt(4));

        if ([20,50].indexOf(dt_naiss_nir) === -1) {
            dtNaiss.on('input change', function() {
                let $dt = $(this).val(),
                    dt_jours = $dt.substring(0,2),
                    dt_mois = NIR.charAt(3) + NIR.charAt(4),
                    dt_anne = NIR.charAt(1) + NIR.charAt(2),
                    calc_dt = moment().subtract(dt_anne, 'year').year().toString().substring(0,2) + dt_anne;

                if ($dt.length > 2) { $(this).val(sprintf('%d/%d/%d', dt_jours, dt_mois, calc_dt)) }

            });
        }

    } else {

        listOfPaysNaissance();

        // BLOCK DATE DE NAISSANCE IN PROVISOIRE
        dtNaiss.on('input change', function() {
            let $dt = $(this).val(),
                dt_jours = $dt.substring(0,2),
                dt_mois = NIR.charAt(3) + NIR.charAt(4),
                dt_anne = NIR.charAt(1) + NIR.charAt(2),
                calc_dt = moment().subtract(dt_anne, 'year').year().toString().substring(0,2) + dt_anne,
                dt_nir_def = sprintf('%d/%d/%d', dt_jours, dt_mois, calc_dt);

            if ($dt.isFormatDateView() && $(this).val() === dt_nir_def) {

                $(this).val('');

                // BLOCK ALL INPUTS IN THE BODY
                disableFormBlock();

                notifyNebula(
                    'Un salarié avec ce NIR existe déjà, vous ne pouvez pas le positionner en "provisoire". En cas de doute contactez le support!',
                    null,
                    'error'
                )

            }

        });

    }

}

function getDepartmentForAutoCompletNir(a, b) {

    if (Number(a) === 2 && b.match(/^[aAbB]$/)) {
        return b.toUpperCase() === "A" ? 19 : 18

    } else {
        return Number(a + b)
    }

}

function traitVilleNaissance() {
    let villeNaissance = $('input[name="liVilleNaissance"]');

    villeNaissance.on('input', function() {
        $(this).val(function() {
            let $val = removeSpecialCharacters($(this).val());

            return $val.replace(/\d+/, '').toUpperCase();
        })
    })

}