function init_rib_salarie() {
    let payement = $('input[name="isPaiementVirement"]');

    if (payement.is(':checked')) {
        let rib = $('form[name="rib"] .isVirement');

        rib.removeClass('hide');

        rib.find(':input').each(function(){
            $(this).prop('required', true);
            $(this).parent().append('<span class="valid-nebula"></span>');
        })
    }

    showVirement();
    validationRIB();
}

function showVirement() {
    let isVirement = $('input[name="isPaiementVirement"]');

    isVirement.on('ifChanged', function() {
        let rib = $('form[name="rib"] .isVirement');

        if (!$(this).is(':checked')) {
            rib.addClass('hide');
            $('form[name="rib"]')[0].reset();
            $(this).prop('checked', false);

            rib.find(':input').each(function() {
                $(this).val('');
                $(this).prop('required', false);
                $(this).parent().find('span').remove();
            })

        } else {
            rib.removeClass('hide');

            rib.find(':input').each(function(){
                $(this).prop('required', true);
                $(this).parent().append('<span class="valid-nebula"></span>');
            });
        }

        // Réactive la validation du formNebula.js
        enableValidationSalarie();

    });

}

function validationRIB() {
    let form_rib = $('form[name="rib"]'),
        bic = form_rib.find('input[name="liBic"]'),
        titulaire = form_rib.find('input[name="liTitulaireCompte"]'),
        domiciliation = form_rib.find('input[name="liDomiciliationBancaire"]'),
        iban = form_rib.find('input[name="liIban"]');

    // Validation BIC
    bic.on('input', function() {
        let upper = this.value;
        $(this).val(upper.validBIC());
    });

    // BIC
    bic.inputmask({
        mask: '*{1,}',
        definitions: {
            '*': {
                validator: '[a-zA-Z0-9]',
                casing: 'upper'
            }
        }
    });

    // Check IBAN
    iban.on('input change', function() {
        let upper = this.value.replace(/[\s|_]+/g, '');

        if (upper.match(/^[A-Z]{2}\d{2}/)) {
            IBAN.isValid(upper)
                ? iban.prop('pattern', '^[A-Z]{2}\\d{2}(.*)')
                : iban.prop('pattern', '^[0-9]{50}');
        }

    });

    // IBAN
    iban.inputmask({
        mask: "*{4} *{4} *{4} *{4} *{4} *{4} *{4} *{3}",
        definitions: {
            '*': {
                validator: '[a-zA-Z0-9]',
                casing: 'upper'
            }
        }
    });

    // TITULAIRE DU COMPTE
    titulaire.inputmask({
        mask: '*{1,}',
        definitions: {
            '*': {
                validator: '[a-zA-Z0-9\\s]',
                casing: 'upper'
            }
        }
    });

    // DOMICILIATION
    domiciliation.inputmask({
        mask: '*{1,}',
        definitions: {
            '*': {
                validator: '[a-zA-Z0-9\\s]',
                casing: 'upper'
            }
        }
    });


}