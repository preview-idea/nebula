function init_enfants_salarie() {
    verifyObjChild();
    localStorage.removeItem('enfants');

    if (URL_CURRENT.match(/^\/salaries\/new/)) {
        newAddChild();
        newRemoveChild();
    }

    if (URL_CURRENT.match(/^\/salaries\/detail/)) {
        detailAddChild();
        detailRemoveChild();
    }
}

/* PAGE NEW */
// BTN SAVE NUMBER OF CHILD IN LOCALSTORAGE
function newAddChild() {
    addChildFromModal();

}

// VERIFY IF ENFANTS EXISTS IN LOCALSTORAGE
function verifyObjChild(){
    let obj_enfant = JSON.parse(localStorage.getItem('enfants')) || [],
        nbEnfant = $('input[name="nbEnfant"]');

    if (obj_enfant.length) { addListChild(obj_enfant); }

    nbEnfant.keypress(function(e) {
        if (e.keyCode === 13) { verifyNumberEnfant() }
    })
}

// BUTTON ADD LIST OF ENFANT
async function addListChild(obj_enfant) {

    // Save obj_enfant on LocalStorage
    localStorage.setItem('enfants', JSON.stringify(obj_enfant));

    const _data = await $.post('/salaries/enfants/new', { enfants: obj_enfant })

    if (_data) {
        const content = $('#liste_enfants');

        content.html(_data);
        listOfPriseCharge();

        !URL_CURRENT.match(/^\/salaries\/detail/)
            ? newRemoveChild()
            : detailRemoveChild();

        const select_enfant = $('form[name^="enfants_"] select');
        const input_flat = $('#liste_enfants input.flat');

        select_enfant.each(function() {
            $(this).select2({
                minimumResultsForSearch: 12,
                language: 'fr'
            })
        });

        inputFlat(input_flat);
        reloadJS();
    }


}

// BTN REMOVE LIST OF ENFANT
function newRemoveChild() {
    const btn_remove_enfant = $('#remove_enfant');

    btn_remove_enfant.unbind();
    btn_remove_enfant.click(function() {
        let confirm_msg = '<h5 class="text-center"> Vous êtes sûr de vouloir supprimer cette information? </h5>',
            $check = $('#elementEnfant input[name="idEnfant"]'),
            current_enfant = $('#liste_enfants form[name^="enfants_"]'),
            obj_enfant = [],
            isChecked = false,
            current_obj = JSON.parse(localStorage.getItem('enfants')) || $.map(current_enfant, function(enfant) {
                let obj = [];
                return obj.push(traitPersonElements(enfant));
            });

        $check.each(function() {
            let $checked = $(this);

            if ($(this).is(':checked')) {
                isChecked = true;

            } else if (current_obj) {
                current_obj.forEach(function (enfant) {
                    if (enfant.idEnfant === $checked.val() || enfant.idEnfant === parseInt($checked.val())) {
                        obj_enfant.push(enfant);
                    }
                })
            }
        });

        if (isChecked) {
            let confirm_yes = $('#confirm_yes');

            // SHOW MODAL CONFIRMATION
            MODAL_CONFIRM.find('div.modal-body').html(confirm_msg);
            MODAL_CONFIRM.modal('show');

            confirm_yes.unbind();
            confirm_yes.click(function() {

                if (obj_enfant.length) {
                    localStorage.setItem('enfants', JSON.stringify(obj_enfant));
                    addListChild(obj_enfant);

                } else {
                    localStorage.removeItem('enfants');
                    $.post('/salaries/enfants/new')
                        .done(function (res) {
                            let content = $('#liste_enfants');
                            content.html(res);
                            notifyNebula("L'élément a bien été supprimé", 'Personne à charge', 'success');
                            chargeFormLabel();

                        });
                }

            });

            $('#confirm_no').click(function(){
                $check.iCheck('uncheck');
            });

        } else {
            notifyNebula('Vous devez sélectionner un élément', 'Personne à charge', 'info');
        }

    });
}

// ADD CHILDREN FROM MODAL
function addChildFromModal() {
    const btn_add_enfant = $('#add_enfant');

    btn_add_enfant.unbind();
    btn_add_enfant.click(function() {
        let obj_enfant = [],
            current_enfant = $('form[name^="enfants_"]'),
            form_modal = $('.modal-body form[name="enfants_"]');

        current_enfant.each(function(){
            obj_enfant.push(traitPersonElements(this));
        });

        // SET NEW VALUE FOR ENFANTS LOCALSTORAGE
        addListChild(obj_enfant);

        // RESET FORM FROM MODAL
        form_modal[0].reset();
        form_modal.find('select.select2-hidden-accessible').change();

    })

}

// TRAIT ELEMENT ON FORM ENFANT MODAL
function traitPersonElements(e) {
    let r = {},
        idEnfant = $(e).parents('#elementEnfant').find('input[name="idEnfant"]').val();

    if (typeof idEnfant !== 'undefined') {
        r['idEnfant'] = idEnfant.match(/^[A-Z]/) ? idEnfant : parseInt(idEnfant);

    } else {
        r['idEnfant'] = uniqidLocal();
        r['isLocal'] = true;
    }

    for (let i=0; i < e.length; i++) {
        if (e[i].value || (e[i].type === 'select-one' && e[i].options[e[i].selectedIndex].value)) {

            if (e[i].name.match(/^id/)) {
                r[e[i].name] = e[i].type === 'select-one'
                    ? { [e[i].name]: parseInt(e[i].options[e[i].selectedIndex].value), ['li' + e[i].name.substring(2,100)]: getOptionsText(e[i]) }
                    : parseInt(e[i].value);

            } else if (e[i].name.match(/^dt/)) {
                let date = moment(e[i].value.convertToDate());
                r[e[i].name] = date.toISOString();

            } else {
                r[e[i].name] = e[i].value

            }

        }

    } // for

    return r;
}

/* FUNCTION VERIFY INPUT NUMBER ENFANT */
function verifyNumberEnfant() {
    let number = $('input[name="nbEnfant"]').val();

    if (number) {
        let obj_enfant = [];

        for (let i=0; i < number; i++) {
            obj_enfant.push({
                idEnfant: uniqidLocal(),
                idSexe: { idSexe: 1 },
                idTypePriseCharge: { idTypePriseCharge: 1, liTypePriseCharge: 'Enfant à charge fiscalement' },
                isLocal: true
            });

        } // for

        addListChild(obj_enfant);

    } else {
        notifyNebula('Vous devez saisir un chiffre', 'Personne à charge', 'info');
    }

}


/***** PAGE DETAIL ****/
function detailAddChild() {
    addChildFromModal();
}

function detailRemoveChild() {
    const btn_remove_enfant = $('#remove_enfant');

    btn_remove_enfant.unbind();
    btn_remove_enfant.click(function() {
        let confirm_msg = '<h5 class="text-center"> Vous êtes sûr de vouloir supprimer cette information? </h5>',
            $check = $('#elementEnfant input[name="idEnfant"]'),
            idMatricule = parseInt($('input[name="idMatricule"]').val()),
            enfants = [],
            isChecked = false;

        $check.each(function() {

            if ($(this).is(':checked')) {

                if ($(this).val().match(/^[A-Z]/)) {
                    const obj_enfant = JSON.parse(localStorage.getItem('enfants')) || [];

                    // REMOVE ELEMENT FROM THE LIST
                    $(this).parents('#elementEnfant').remove();

                    if (obj_enfant.length) {
                        const remove_checked = obj_enfant.filter(obj => obj.idEnfant !== this.value);
                        const count_obj = remove_checked.filter(obj => obj.idEnfant.match(/^[A-Z]/));

                        // CLEAR LOCAL OF STORAGE IF NOT EXISTS ID TYPE STRING (not saved in BDD).
                        if (count_obj.length) {
                            localStorage.setItem('enfants', JSON.stringify(remove_checked));

                        } else {
                            localStorage.removeItem('enfants');
                            $.post('/salaries/enfants/new')
                                .done(function(res) {
                                    let content = $('#liste_enfants');
                                    content.html(res);
                                    chargeFormLabel();

                                });
                        }

                    }

                    isChecked = null;
                    notifyNebula("L'élément a bien été supprimé", 'Personne à charge', 'success');


                } else {
                    isChecked = true;
                    enfants.push(parseInt(this.value));

                    // SHOW MODAL CONFIRMATION
                    MODAL_CONFIRM.find('div.modal-body').html(confirm_msg);
                    MODAL_CONFIRM.modal('show');
                }

            }

        });

        if (isChecked) {
            let confirm_yes = $('#confirm_yes');

            confirm_yes.off('click', newRemoveChild);
            confirm_yes.click(function() {

                $.post('/salaries/enfants/remove', {
                    idMatricule: idMatricule,
                    enfants: enfants
                })
                    .done(function(resp) {

                        // REMOVE ELEMENT IN LIST
                        $check.each(function() {
                            if (enfants.indexOf(parseInt(this.value)) !== -1) {
                                $(this).parents('#elementEnfant').remove();
                            }
                        });

                        let current_enfant = $('#liste_enfants form[name^="enfants_"]');

                        if (current_enfant.length === 0) { addListChild([]); }

                        notifyNebula(resp, 'Personne à charge', null, 'success');
                        reloadJS();

                    })
                    .fail(function(error) {
                        console.log(error);
                    })

            });

            $('#confirm_no').click(function() {
                $check.iCheck('uncheck');
            });

        } else if (isChecked === false) {
            notifyNebula('Vous devez sélectionner un élément', 'Personne à charge', 'info');
        }

    });
}