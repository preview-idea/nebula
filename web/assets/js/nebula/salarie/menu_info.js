function init_menu_info_salarie() {
    let btn_imprimer = $('.btn-imprimer'),
        btn_perte_site = $('.btn-perte-site'),
        btn_supprimer = $('.btn-supprimer'),
        btn_email = $('.btn-email'),
        adresse_mail = $('input[name="liMail"]').val();

    // IMPRIMER SALARIE
    btn_imprimer.unbind();
    btn_imprimer.click(function() {
        modalGenerationFicheSalarie();
    });

    // GENERATION PERTE SITE
    btn_perte_site.unbind();
    btn_perte_site.click(function() {
        downloadPerteSite();
    });

    // DELETE SALARIE
    btn_supprimer.unbind();
    btn_supprimer.click(function() {

        removeSalarie();

    });

    // SEND EMAIL
    btn_email.unbind();
    btn_email.click(function() {
        let _url = sprintf('https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=%s', adresse_mail);
        window.open(_url , '_blank');
    })
}

function modalGenerationFicheSalarie() {

    let modal_fiche_salarie = $('#fiche_salarie'),
        download_fiche = $('#download_fiche');

    manageSelectAll();

    download_fiche.unbind();
    download_fiche.click(function () {
        downloadFicheSalarie();
    });

    modal_fiche_salarie.modal('show');

}

async function downloadFicheSalarie() {

    let _data = await $.post(URL_CURRENT.replace('detail', 'fiche/download'), generateObjectBlock());

    if (typeof _data !== "number") {
        window.location.assign(`/salaries/document?file=${_data}`);
    }

}

async function downloadPerteSite() {

    let idMatricule = parseInt($('input[name="idMatricule"]').val());

    let _data = await $.post(URL_CURRENT.replace('detail', 'pertesite/download'), { idMatricule: idMatricule });

    if (parseInt(_data) === 404) {
        notifyNebula('erreur 404', 'Classeur Social', 'info');
    }
    else if (typeof _data !== "number") {
        window.location.assign(`/salaries/document?file=${_data}`);
    }

}

function generateObjectBlock() {

    let checkbox_block = $('form[name="block_fiche"] input:checkbox:not("#isSelectAll")'),
        idMatricule = parseInt($('input[name="idMatricule"]').val()),
        obj = {
            idMatricule: idMatricule,
            blocks: {}
        };

    checkbox_block.each(function () {
        obj['blocks'][$(this).attr('id')] = $(this).is(':checked') ? 1 : 0;
    });

    return obj;

}

function manageSelectAll() {

    let checkbox_block = $('form[name="block_fiche"] input:checkbox:not("#isSelectAll")'),
        isSelectAll = $('#isSelectAll'),
        download_fiche = $('#download_fiche');

    checkbox_block.unbind();
    checkbox_block.on('ifChanged', function () {
        isSelectAll.iCheck((checkbox_block.filter(':checked').length > 0) ? 'check' : 'uncheck');
        download_fiche.prop('disabled', (checkbox_block.filter(':checked').length === 0))
    });

    isSelectAll.unbind();
    isSelectAll.on('ifClicked', function () {
        checkbox_block.iCheck(($(this).is(':checked')) ? 'uncheck' : 'check');
    });
}


