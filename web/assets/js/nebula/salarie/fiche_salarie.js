if (URL_CURRENT.match(/salaries\/(new|detail)$/)) {
    // Instance form nebula salarie
    INS_FN_SALARIE = new FormNebula({
        objLinked: ['enfants', 'personnes'], // Objects linked
        except: ['block_fiche', 'planification_form', 'entretien_form', 'absence_form']
    });

}

function init_fiche_salarie() {
    // For update form salarie
    if (URL_CURRENT.match(/salaries\/detail/)) {

        INS_FN_SALARIE.update.click(function() {
            let idMatricule = parseInt($('input[name="idMatricule"]').val());

            if (INS_FN_SALARIE.isOk === true) { INS_FN_SALARIE.updateForm(idMatricule); }

        });

    }

    // For redirect page salarie
    if (URL_CURRENT.match(/^\/salaries\/new$/)) {

        INS_FN_SALARIE.redirection = function(_resp) {
                let idMatricule = _resp.idMatricule;

            if (idMatricule) {
                const  pages = [
                    { _name: 'accueil',  _btn: $('#page_accueil'),    _url: '/salaries' },
                    { _name: 'detail',   _btn: $('#page_detail'),     _url: `/salaries/detail?idMatricule=${idMatricule}` },
                    { _name: 'contrat',  _btn: $('#page_contrat'),    _url: `/salaries/contrats/new?idMatricule=${idMatricule}` }
                ];

                pages.map(page => page._btn.click(() => location.assign(page._url)));

                // Show modal
                $("#redirection").modal('show');
            }

        }
    }
}