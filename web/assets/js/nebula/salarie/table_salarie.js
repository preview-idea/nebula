function init_table_salarie() {


    /************ MASK ********/
    $('#idNirDef').inputmask({
        mask: '9 99 99 99 999 999 99'
    });

    createTableForSalarie();

}

function createTableForSalarie() { "use strict";
    let tableSalarie =  $("#datatable-salarie");

    // CREATE INSTANCE DATABASE FOR CONST VARIABLE
    const DATATABLE_SALARIE = tableSalarie.DataTable({
        ajax: {
            url: '/salaries',
            type: 'POST',
            dataSrc: data => dataTablesFormatDateFromAjax(data)
        },
        columns: [
            { data: 'idMatricule',      title: "Matricule" },
            { data: 'liNomUsage',       title: "Nom complet" },
            { data: 'liMail',           title: "Mail" },
            { data: 'liAgence',         title: "Agence" },
            { data: 'dtDebutcontrat',   title: "Date début contrat" },
            { data: 'dtFincontrat',     title: "Date fin contrat" },
            { data: 'liTypecontrat',    title: "Type contrat" },
            { data: 'liQualifcontrat',  title: "Qualification" },
            { data: 'idContrat',        title: "Contrat" },
            { data: 'liVille',          title: "Ville" },
            { data: 'liCodepostal',     title: "Code Postal" },
            { data: 'liNomVoie',        title: "Adresse" },
            { data: 'liAdresse2',       title: "Complément d'adresse" },
            { data: 'liPays',           title: "Pays" }
        ],
        columnDefs: [
            {
                targets: [1,2,3],
                render: $.fn.dataTable.render.ellipsis( 20, true )
            }
        ]
    });

    tableSalarie.find('tbody').on('dblclick', 'tr', function() {
        let data = DATATABLE_SALARIE.row( this ).data();

        location.assign(sprintf('/salaries/detail?idMatricule=%d', data.idMatricule));
        NProgress.inc();

    });

    openOneRowInDataTable(tableSalarie);

}
