function init_personnes_salarie(){
    detailRemovePerso();
    addRequiredInInputs();

}

function detailRemovePerso() {
    $('#remove_personne').click(function() {
        let idPersonneContacter = parseInt($('#elementPersonne input[name="idPersonneContacter"]').val()),
            idMatricule = parseInt($('input[name="idMatricule"]').val());

        if (confirm('Vous êtes sûr de vouloir supprimer ces informations?')) {

            $.post('/salaries/personnes/remove', {
                idMatricule: idMatricule,
                idPersonneContacter: idPersonneContacter
            })
                .done(function(data) {

                    if (typeof data !== "number") {
                        let form = $('form[name="personnes_"]'),
                            content = form.find('div.content-personnes');

                        // RESET FORM PERSONNE
                        content.addClass('hide');
                        resetFormPersonne();
                        $('#remove_personne').parent().remove();

                        notifyNebula(data, 'Personne à prévenir', 'success');
                        chargeFormLabel();
                    }


                })
                .fail(function(error) {
                    console.log(error);
                })

        }

    });
}

function addRequiredInInputs() {
    let form = $('form[name^="personnes_"]'),
        civilite = form.find('[name="idCivilite"]'),
        content = form.find('div.content-personnes'),
        nom = form.find('input[name="liNomPersonneContacter"]'),
        prenom = form.find('input[name="liPrenomPersonneContacter"]'),
        lien_parente = form.find('input[name="liRelationSalariePac"]'),
        telephone = form.find('[name="liTelephone1"]');

    civilite.each(function() {
        let $check = $(this);

        if (URL_CURRENT.match(/salaries\/detail$/)) {

            // Needed to enable/disable button "send"
            $check.trigger('change');

            if ($check.is(':checked')) {
                content.removeClass('hide');
                nom.prop('required', true);
                prenom.prop('required', true);
                telephone.prop('required', true);
                lien_parente.prop('required', true);
            }

        }

        // Enabled unchecked option.
        $check.on('ifClicked', function() {
            if ($(this).is(':checked')) {
                $(this).iCheck('uncheck');
                form.resetFormBlock();
            }
        });

    });

    civilite.on('ifChanged', function() {

        $(this).is(':checked') ? content.removeClass('hide') : content.addClass('hide');

        nom.prop('required', $(this).is(':checked'));
        prenom.prop('required', $(this).is(':checked'));
        telephone.prop('required', $(this).is(':checked'));
        lien_parente.prop('required', $(this).is(':checked'));

        enableValidationSalarie();
        chargeFormLabel();

    })

}

function resetFormPersonne() {
 let form_personne = $('form[name="personnes_"]');

    form_personne.find('input, select').each(function() {
        if (['checkbox', 'radio'].indexOf($(this).prop('type')) !== -1) {
            $(this).iCheck('uncheck');

        } else if ($(this).is('select')) {
            $(this).prop('selectedIndex', 0).trigger('change');

        } else {
            if ($(this).parent().hasClass('has-value')) {
                $(this).parent().removeClass('has-value')
            }

            $(this).val('');
        }

    });
}