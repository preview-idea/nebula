function init_coordonnees_salarie() {
    traitEtrangers();
    authorizationSendEmail();
}

function traitEtrangers() {
    let form_coord = $('form[name="coordonnees"]'),
        coord_pays = form_coord.find('select[name="idPays"]'),
        coord_ville = form_coord.find('select[name="idVille"]'),
        codePostal = form_coord.find('select[name="idCodepostal"]'),
        codePostalEtrang = form_coord.find('input[name="liCodepostalEtranger"]'),
        villeEtrang = form_coord.find('input[name="liVilleEtranger"]'),
        France = 78;

    // Show code étranger if pays is not France
    if (coord_pays.val()) {
        if (parseInt(coord_pays.val()) === France) {
            codePostal.parent().removeClass('hide');
            coord_ville.parent().removeClass('hide');
            listCodepostal(coord_pays.val());

        } else {
            codePostalEtrang.parent().removeClass('hide');
            villeEtrang.parent().removeClass('hide');
        }
    }

    // Show codepostal if pays is France
    // Verify if on change pays
    coord_pays.change(function() {

        if ($(this).val()) {
            let pays_selected = parseInt($(this).val());

            if (pays_selected === France) {

                // FRANCE
                codePostal.children(':not(:first)').remove();
                coord_ville.children(':not(:first)').remove();
                codePostal.parent().removeClass('hide');
                coord_ville.parent().removeClass('hide');
                verifySpanNebula([codePostal, coord_ville], true);

                // ETRANGER
                codePostalEtrang.val('');
                villeEtrang.val('');
                codePostalEtrang.parent().addClass('hide');
                villeEtrang.parent().addClass('hide');
                verifySpanNebula([villeEtrang], false);

            } else {

                // FRANCE
                codePostal.parent().addClass('hide');
                coord_ville.parent().addClass('hide');
                verifySpanNebula([codePostal, coord_ville], false);

                // ETRANGER
                codePostal.val('');
                coord_ville.val('');
                codePostalEtrang.parent().removeClass('hide');
                villeEtrang.parent().removeClass('hide');
                verifySpanNebula([villeEtrang], true);

            }
            listCodepostal($(this).val());
            chargeFormLabel();
        }

    });

}

function verifySpanNebula(target, add) {

    target.forEach(function(t) {
        if (add === true) {
            t.prop('required', true);

            if (!t.parent().has('span.valid-nebula').length) {
                t.parent().append('<span class="valid-nebula"></span>');
            }

        } else {
            t.prop('required', false);

            if (t.parent().has('span.valid-nebula').length) {
                t.parent().find('span.valid-nebula').remove();
            }
        }
    })

}

function authorizationSendEmail() {
    let email = $('input[name="liMail"]'),
        send_y = $('input[name="isEnvoiMail"]'),
        pattern_mail = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    email.on('input blur', function() {
        let _value = $(this).val();

        addRequirementIfValue($(this));

        if (_value) {
            _value.match(pattern_mail)
                ? send_y.iCheck('check')
                : send_y.iCheck('uncheck');

            $(this).val( () => {
                return removeAccent(_value);
            })

        }
    });

}



