function init_indicators_salarie() {
    getListIndicators();
}

function getListIndicators() {

    $.getJSON('/salaries/indicateurs/list')
        .then(_data => {
            if (typeof _data !== "number") {

                if (_data.length) {
                    traitListIndicator(_data)
                }
            }
        })

}

function traitListIndicator(_data) {
    let _preload = [],
        _result = [],
        count = 0;

    for (let prop in _data) {

        if (_data.hasOwnProperty(prop)) {

            const _params = _data[prop].liParamApiJson ? _data[prop].liParamApiJson : null;


            try {

               $.getJSON(_data[prop].liUrl, _params)
                   .then(_nb => {

                       if (typeof _nb !== 'number') {
                           const _indicator = createIndicator(_data[prop], _nb.indicator, count++);
                           _result[_data[prop].nbOrder] = _indicator._html
                           _preload[_data[prop].nbOrder] = _indicator._preload
                       }

                   })

            } catch (err) {
                console.log(err)
            }
        }

    } // for

    // Add content after preload completed
    OJBNebulaPreloaded.customLoad = function() {
        $("#idIndicatorList").append(_result);
        _preload.forEach(pre => pre.start());
    }

}

function createIndicator(indicator, nbIndicator, index) {
    let html = '',
        param,
        seuilClass,
        classIndicateur = '';

    if (nbIndicator || nbIndicator === 0) {

        if ('liParamApiJson' in indicator && indicator.liParamApiJson) {
            param = '?' + $.param(indicator.liParamApiJson);
        }

        // Ajout d'une class css si l'indicateur depasse le seuil requis
        if ('liSeuilClass' in indicator && indicator.liSeuilClass) {
            seuilClass = indicator.liSeuilClass;
        }

        if (seuilClass && 'seuil' in seuilClass && 'class' in seuilClass) {
            classIndicateur = nbIndicator > seuilClass.seuil ? ' ' + seuilClass.class : '';
        }

        html =
            `<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">` +
            (indicator.liUrlPageDetail && nbIndicator > 0
                ? '<a href="' + indicator.liUrlPageDetail + (param ? param : '') + '">'
                : '') + '<span class="count_top"><i class="' + indicator.liPictogramme + '"></i> ' + indicator.liAppliIndicateurs + '</span>' +
            '<div class="count indicat_' + index + classIndicateur +'">' + nbIndicator + '</div>' +
            (indicator.liUrlPageDetail && nbIndicator > 0 ? '</a>' : '') +
            '</div>';


    }

    return  {
        _html: html,
        _preload: preloadIndicator(nbIndicator, index)
    };
}

function preloadIndicator(n, i) {
    let count = 0;

    if (n > 10 && n <= 9999999) { count = n - 10; }

    return {
        start: function() {
            const preload = {
                [i]: setInterval(function() {
                    $('div.indicat_' + i).html(count);

                    if (count >= n) { clearInterval(preload[i]); }
                    count++

                }, 60)
            }

        }
    }

}
