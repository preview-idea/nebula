function init_accueil() {

    getListAll();
    manageOneSalarie();
    manageTooltip();
}

// DISPARITION DES TOOLTIPS A L'OUVERTURE D'UNE POP UP
function manageTooltip() {

    let modal = $('.modal');

    modal.on('show.bs.modal', function() {
        $('[data-toggle="tooltip"]').tooltip('hide');
        $('.my-tooltip').tooltip('hide');
    });

    modal.on('hidden.bs.modal', function() {
        $('[data-toggle="tooltip"]').tooltip();
        $('.my-tooltip').tooltip();
    });
}

// GESTION DE LA LISTE DES SALARIES DANS LE CHAMPS DE RECHERCHE
function getListAll() {
    let idSearch = $('#idSearch');

    const all = new GetSalaries({
        target: [idSearch]
    });

    all.getListToSelect2All();

}

// GESTION DE LA SELECTION D'UN SALARIE
function manageOneSalarie() {
    let idSearch = $('#idSearch'),
        idMatricule = $('input[name="idMatricule"]'),
        liNomComplet = $('input[name="liNomComplet"]'),
        liAgence = $('input[name="liAgence"]'),
        liTelephone1 = $('input[name="liTelephone1"]'),
        idMatriculeMaj = $('input[name="idMatriculeMaj"]'),
        photo_info = $('#photo_info'),
        idReactiver = $('#idReactiver'),
        idFiche = $('#idFiche'),
        idCartepro = $('#idCartepro'),
        idAcompte = $('#idAcompte'),
        idAddContrat = $('#idAjouterContrat'),
        contratDiv = $('.current-contrat'),
        salarieDiv = $('.current-salarie'),
        refreshSalarie = $('#refreshSalarie'),
        titleSalarie = $('.current-salarie h2'),
        titleSalarieText = titleSalarie.html();

    refreshSalarie.click(function() {  refreshSalarieDiv() });

    idSearch.on('change', function() {
        const salarie = $(this).customDataSelect2('salarie');
        const matricule = salarie.idMatricule;
        const url_img = salarie.liCheminPhoto
            ? `uploads/images/salaries/${salarie.liCheminPhoto}`
            : 'assets/images/user.png';

        // reset
        resetDisabled();

        // **** INFO SALARIE ****
        photo_info.find('img').attr('src', url_img);

        idMatricule.val(matricule).trigger('change');
        liNomComplet.val(salarie.liNomComplet).trigger('change');
        liAgence.val(salarie.liAgence).trigger('change');
        liTelephone1.val(salarie.liTelephone1).trigger('change');

        init_visuel_protection($('.page-accueil h2:first'));
        // ***********************

        // **** BOUTON SALARIE ****
        if (salarie.liEtat === 'Actif') {
            idFiche.attr('href',sprintf('salaries/detail?idMatricule=%s', matricule));
            idReactiver.parent().hide();
            idCartepro.attr('href',sprintf('salaries/cartepro/liste?idMatricule=%s', matricule));
            idCartepro.parent().show();
            idAddContrat.attr('href',sprintf('salaries/contrats/new?idMatricule=%s', matricule));
            idAddContrat.parent().show();
            titleSalarie.html(titleSalarieText);
        }
        else {
            idFiche.attr('href',sprintf('salaries/inactif/detail?idMatricule=%s', matricule));
            idCartepro.parent().hide();
            idAcompte.parent().hide();
            idAddContrat.parent().hide();
            idReactiver.unbind('click');
            idReactiver.click(function () {
                sendBatchToReact($(this).val(), idMatriculeMaj.val())
            });
            idReactiver.parent().show();
            titleSalarie.html(titleSalarieText + ' (Inactif)');
        }
        // ************************

        salarieDiv.show();


       if (matricule) {

           // *** GESTION DU CONTRAT EN COURS ***
           $.getJSON('/salaries/contrats/current', {
               idMatricule: matricule

           })
               .done(data => {
                   if (typeof data !== "number") {
                       if (Array.isArray(data) && data.length) {
                           let currentContrat,
                               linkContrat = $('#linkContrat');

                           // CHOIX DU CONTRAT EN COURS A PRENDRE
                           if (data.length === 1) {
                               currentContrat = data[0];

                           } else {
                               currentContrat = data.find(data => data.idSocieteAx === 'SEC')
                                    || data.find(data => data.idSocieteAx === 'FAC')
                                    || data.find(data => data.idSocieteAx === 'SMS')

                               if (!currentContrat) {
                                   currentContrat = data[0];
                               }
                           }

                           if (currentContrat) {

                               // AFFICHAGE DU LIEN VERS LE CONTRAT
                               linkContrat.attr('href', sprintf('salaries/contrats/detail?idMatricule=%s&idContrat=%s', matricule, currentContrat.idContrat));
                               linkContrat.html(
                                   sprintf('<i class="fa fa-file-text-o fa-2x" style="margin-right: 4px; color: #b7b7b7"></i> %s - %s%s - %s - %s',
                                       currentContrat.liAgence,
                                       moment(currentContrat.dtDebutcontrat).format('DD/MM/YYYY'),
                                       currentContrat.dtFincontrat ? sprintf(' au %s', moment(currentContrat.dtFincontrat).format('DD/MM/YYYY')) :
                                               currentContrat.dtFincontratPrevue ? sprintf(' au %s', moment(currentContrat.dtFincontratPrevue).format('DD/MM/YYYY')) : '',
                                       currentContrat.liTypecontrat,
                                       currentContrat.liQualifcontrat
                                   ));

                               // BOUTON SALARIE
                               if (salarie.liEtat === 'Actif') {
                                   idAcompte.attr('href', sprintf('salaries/acomptes/liste?idMatricule=%s', matricule));
                                   idAcompte.parent().show();
                               }

                               // BOUTON CONTRAT
                               let telechargerContrat = $('#idTelechargerContrat'),
                                   supprimerContrat = $('#idSupprimerContrat'),
                                   cloturerContrat = $('.btn-cloturer'),
                                   ajouterAvenant = $('#idAjouterAvenant');

                               // TELECHARGEMENT
                               telechargerContrat.unbind('click');
                               telechargerContrat.click(function () {
                                   downloadDocument(currentContrat.idContrat);
                               });

                               // SUPRESSION
                               supprimerContrat.unbind('click');
                               supprimerContrat.click(function () {
                                   removeContrat(currentContrat.idContrat);
                               });

                               // CLOTURER
                               clotureContrat(
                                   cloturerContrat,
                                   currentContrat.idContrat,
                                   matricule,
                                   new Date(currentContrat.dtDebutcontrat).format('d/m/Y')
                               );

                               // ADD AVENANT
                               ajouterAvenant.attr('href',sprintf('salaries/contrats/avenants/new?idMatricule=%s&idContrat=%s', matricule, currentContrat.idContrat));

                               // TRANSFERT CONTRAT
                               getConentModalFromTypeTransfert(
                                   matricule,
                                   currentContrat.idContrat,
                                   new Date(currentContrat.dtDebutcontrat).format('d/m/Y'),
                                   currentContrat.idAgence,
                                   'refreshSalarieDiv'
                               );

                               $('.my-tooltip').tooltip();

                               // DUPLIQUER CONTRAT
                               dupliquerContrat(matricule, currentContrat.idContrat);

                               checkSalarieIsInBatchFromAccueil(matricule, currentContrat.idContrat, true);
                               contratDiv.show();
                           }
                           else {
                               manageNoContratForSalarie(matricule);
                           }

                       }
                       else
                       {
                           manageNoContratForSalarie(matricule);
                       }
                   }
                   else {
                       manageNoContratForSalarie(matricule);
                   }

               })
               .fail(err => console.error(err))
           // ***********************************
       }

    });
}

function resetDisabled() {
    let idAdvertissementSalarie = $('#idAdvertissementSalarie'),
        idAdvertissementContrat = $('#idAdvertissementContrat'),
        tabButtonLink = [
            $('#idTransfererContrat'),
            $('#idCloturerContrat'),
            $('#idSupprimerContrat'),
            $('#idAjouterAvenant'),
            $('#idAjouterContrat')
        ];

    idAdvertissementSalarie.addClass('hide');
    idAdvertissementContrat.addClass('hide');

    tabButtonLink.forEach(element => {
        element.removeClass('disabled');
        if (element.parents('span').length) element.unwrap();
        if (element.parents('fieldset').length) element.unwrap();
    });

}

function manageNoContratForSalarie(matricule) {
    let idAcompte = $('#idAcompte'),
        contratDiv = $('.current-contrat');

    checkSalarieIsInBatchFromAccueil(matricule);
    idAcompte.parent().hide();
    contratDiv.hide();
}

function dupliquerContrat(idMatricule, idContrat) {
    let idDupliquerContrat = $('#idDupliquerContrat');

    idDupliquerContrat.unbind('click');
    idDupliquerContrat.click(function() {  $('#duplicateSheet').modal('show') });

    getListActifToDuplicateContrat();
    validFormDuplicate();
    duplicateFormContrat(idMatricule, idContrat);

}

function refreshSalarieDiv() {
    let idSearch = $('#idSearch');

    idSearch.trigger('change');
}

function clotureContrat(cloturerContrat, idContrat, idMatricule, dateDebut) {
    let form_cloture = $('form[name="cloture_contrat"]')[0];

    cloturerContrat.unbind('click');
    cloturerContrat.click(function() {

        let dt_fin = $('form[name="cloture_contrat"] input[name="dtFincontrat"]'),
            motif = $('form[name="cloture_contrat"] select[name="idMotiffincontrat"]'),
            salarieSorti = $('form[name="cloture_contrat"] input[name="isSalarieSorti"]');

        localStorage.setItem('idContrat', idContrat);
        localStorage.setItem('idMatricule', idMatricule);
        salarieSorti.iCheck('uncheck');

        if (motif.val().length || dt_fin.val().length) {
            resetFormClosure(form_cloture);
            verifyFormCloture(form_cloture);
        }

        // GET LIST MOTIF FIN CONTRAT
        getListMotiffincontratCloture();

        // Verify if date fin is not less from date debut
        checkDateFinCloture(dateDebut);

        $('#cloturerContrat').modal('show');

        sendCloture(idContrat, idMatricule,'', 'refreshSalarieDiv', 'salaries/contrats/');
    });

    $('input, select').on('input change', function() { verifyFormCloture(form_cloture) });
}

function checkSalarieIsInBatchFromAccueil(idMatricule = null, idContrat = null, isContrat = false) {

    if (idMatricule) {

        $.getJSON('/batches/salarie/current', { idMatricule: idMatricule } )
            .done(data => {
                if (typeof data !== "number") {
                    disableUpdateActionSalarieAccueil();
                    if (isContrat) disableUpdateActionContratAccueil(false);
                }
                else {
                    if (isContrat) checkContratIsInBatchFromAccueil(idContrat);
                }

            })
            .fail(err => {
                console.error(err);
                if (isContrat) checkContratIsInBatchFromAccueil(idContrat);
            });
    }


}

function checkContratIsInBatchFromAccueil(idContrat) {

    $.getJSON(
        '/batches/contrat/current', {
            idContrat: idContrat
        }
    )
        .done(function(data) {
            if (typeof data !== "number") {
                disableUpdateActionContratAccueil(true);
            }

        })
        .fail(function(err) {
            console.error(err);
        });

}

function disableUpdateActionContratAccueil(needAdverstissementContrat) {

    //***** DISABLED ACTION *****
    let tabDisabledPage = [
            $('#idTransfererContrat'),
            $('#idCloturerContrat'),
            $('#idSupprimerContrat'),
            $('#idAjouterAvenant')
        ],
        idAdvertissementContrat = $('#idAdvertissementContrat');

    if (needAdverstissementContrat) idAdvertissementContrat.removeClass('hide');
    disabledButtonAndLink(tabDisabledPage, false, true);

}

function disableUpdateActionSalarieAccueil() {

    //***** DISABLED ACTION *****
    let tabDisabledPage = [
            $('#idAjouterContrat')
        ],
        idAdvertissementSalarie = $('#idAdvertissementSalarie'),
        idAdvertissementContrat = $('#idAdvertissementContrat');

    idAdvertissementSalarie.removeClass('hide');
    idAdvertissementContrat.addClass('hide');
    disabledButtonAndLink(tabDisabledPage, false, true);

}

async function getMessageCalendarAlertForSTC() {
    const _data = await $.getJSON('/calendrier/etapes/alert', { params: { type: 'stc' } });

    if (_data && 'dtEtapeDate' in _data) {
        createContentMessageCalendarAlert(_data, false, 'stc');
    }
}

async function getMessageCalendarAlert() {
    const _data = await $.getJSON('/calendrier/etapes/alert');

    if (_data && 'dtEtapeDate' in _data) {
        createContentMessageCalendarAlert(_data, true);
    }

}

function createContentMessageCalendarAlert(calendrierAlert, timing = false, type = '') {
    let content_alert_message = $('div#alert_message_calendar'),
        type_alert = '',
        time_alert = '',
        _html = '';

    switch(type) {
        case 'stc' :
            type_alert = 'alert-danger';
            time_alert = '-stc';
            break;

        default:
            type_alert = 'alert-warning';
    }

    _html += `<div class="alert ${type_alert} alert-dismissible fade in content-alert${time_alert}" id="calendrier_alert" role="alert">` +
    '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
    '        <span aria-hidden="true">×</span>' +
    '    </button>' +
    '    <!-- CALENDRIER CONTENT -->' +
    '    <div class="calendrier-alert-content">' +
    '       <div class="calendrier-alert-icon"><i class="fa fa-calendar"></i></div>' +
    '       <div class="calendrier-alert-message">' +
    `           <span class="calendrier-alert-process"><strong>${calendrierAlert.liProcess}</strong></span>` +
    `           <span class="calendrier-alert-etape"><u>${calendrierAlert.liEtape} le ${moment(calendrierAlert.dtEtapeDate).format('dddd, DD MMMM Y à HH:mm')}</u></span>` +
    `           <span class="calendrier-alert-commentaire">${calendrierAlert.liCommentaire ? calendrierAlert.liCommentaire : ''}</span>` +
    '      </div>';

    if (timing) {
        _html += `<div class="calendrier-time" id="content-time${time_alert}">` +
        '       <span class="time-title" >Temps estimé :</span>' +
        `       <span class="time-progress"></span>` +
        '    </div>';
    }

    _html += '    </div>' +
    '</div>';

    content_alert_message.append(_html);

    if (timing) {
        getTimingToCalendrierALert(calendrierAlert.dtEtapeDate, time_alert);
    }


}

function getTimingToCalendrierALert(dtEtapeDate, time_alert) {
    let calendrierContent = $(`div.content-alert${time_alert}`),
        calendrierTime = calendrierContent.find(`div#content-time${time_alert} span.time-progress`),
        countDownDate = new Date(dtEtapeDate).getTime();

    // Update the count down every 1 second
    var countdownfunction = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now an the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element
        let has_days = days > 0 ? `${days}j ` : '',
            has_hours = hours > 0 ? `${hours}h ` : '';

        calendrierTime.html(`${has_days}${has_hours} ${minutes}m ${seconds}s`);

        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(countdownfunction);
            calendrierContent.parent().remove();
        }
    }, 1000);

}

