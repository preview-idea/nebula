function init_message_alert() {

    getMessageCalendarAlertForSTC();
    getMessageCalendarAlert();

}

async function getMessageCalendarAlertForSTC() {
    const _data = await $.getJSON('/calendrier/etapes/alert', { params: { type: 'stc' } });

    if (typeof _data === 'object' && 'dtEtapeDate' in _data) {
        createContentMessageCalendarAlert(_data, false, 'stc');
    }
}

async function getMessageCalendarAlert() {
    const _data = await $.getJSON('/calendrier/etapes/alert');

    if (typeof _data === 'object' && 'dtEtapeDate' in _data) {
        createContentMessageCalendarAlert(_data, true);
    }

}

function createContentMessageCalendarAlert(calendrierAlert, timing = false, type = '') {
    let content_alert_message = $('div#alert_message_calendar'),
        type_alert = '',
        time_alert = '',
        _html = '';

    switch(type) {
        case 'stc' :
            type_alert = 'alert-danger';
            time_alert = '-stc';
            break;

        default:
            type_alert = 'alert-warning';
    }

    _html += `<div class="alert ${type_alert} alert-dismissible fade in content-alert${time_alert}" id="calendrier_alert" role="alert">` +
        '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
        '        <span aria-hidden="true">×</span>' +
        '    </button>' +
        '    <!-- CALENDRIER CONTENT -->' +

        '    <div class="calendrier-alert-content">' +
        '       <i class="fa fa-calendar"></i>' +
        '       <div class="calendrier-alert-message">' +
        `           <span class="calendrier-process"><strong>${calendrierAlert.liProcess}</strong></span>` +
        `           <span class="calendrier-etape"><u>${calendrierAlert.liEtape} le ${moment(calendrierAlert.dtEtapeDate).format('dddd, DD MMMM Y à HH:mm')}</u></span>` +
        `           <span data-toggle="tooltip" title='${(calendrierAlert.liCommentaire.length >= 100 ? calendrierAlert.liCommentaire : "")}' class="calendrier-commentaire">${calendrierAlert.liCommentaire.substring(0, 100)} ${(calendrierAlert.liCommentaire.length >= 100 ? "..." : "")}</span>` +
        '      </div>';

    if (timing) {
        _html += `<div class="calendrier-alert-time" id="content-time${time_alert}">` +
            '       <span class="time-title" >Temps estimé :</span>' +
            `       <span class="time-progress"></span>` +
            '    </div>';
    }

    _html += '    </div>' +
        '</div>';

    content_alert_message.append(_html);

    if (timing) {
        getTimingToCalendrierALert(calendrierAlert.dtEtapeDate, time_alert);
    }


}

function getTimingToCalendrierALert(dtEtapeDate, time_alert) {
    let calendrierContent = $(`div.content-alert${time_alert}`),
        calendrierTime = calendrierContent.find(`div#content-time${time_alert} span.time-progress`),
        countDownDate = new Date(dtEtapeDate).getTime();

    // Update the count down every 1 second
    var countdownfunction = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now an the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element
        let has_days = days > 0 ? `${days}j ` : '',
            has_hours = hours > 0 ? `${hours}h ` : '',
            has_minutes = minutes > 0 ? `${minutes}m` : '',
            has_seconds = seconds > 0 ? `${seconds}s ` : '';

        calendrierTime.html(`${has_days}${has_hours} ${has_minutes} ${has_seconds}`);


        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(countdownfunction);
            calendrierContent.parent().remove();
        }
    }, 1000);

}