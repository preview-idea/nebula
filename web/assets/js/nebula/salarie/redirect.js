function init_redirect_to_salarie_if_in_batch() {
    let idMatricule = $('input[name="idMatricule"]').val();

    if (idMatricule) {
        $.getJSON('/batches/salarie/current', { idMatricule: idMatricule } )
            .done(function(data) {
                if (typeof data !== "number") {

                    //redirect to contrat detail if contrat is in batch
                    setTimeout(function() {
                        location.assign(`/salaries/detail?idMatricule=${idMatricule}`);
                    },0)

                }

            })
            .fail(function(err) {
                console.error(err);
            });
    }

}