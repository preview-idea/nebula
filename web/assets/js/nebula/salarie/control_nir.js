function init_control_nir_salarie() {

    inputMaskForIdNir();
    nirValidator();
    isNirProv();

    // Open modal nir
    $('#controlNir').modal('show');
}


// assuranceCode must be a string
function nirValidator() {
    const nirRegex = /[123478][0-9]{2}(0[1-9]|1[0-2]|[68][123]|[52]0)([0-9]{2}|2A|2B)[0-9]{3}[0-9]{3}[0-9]{2}/;

    let btn_valid = $('#valid_nir'),
        form_nir = $('form[name="control_nir"]'),
        modal_nir = $('#controlNir'),
        btn_new = $('#new_salarie');

    btn_valid.click(function() {
        let idNirDef = form_nir.find('input[name="idNirDef"]'),
            assuranceCode = idNirDef[0].value.replace(/\s/g, ''),
            content_result = $('#result_nir');

        // REMOVE CONTENT RESULT NIR
        idNirDef.on('input change', function() {
            if (content_result.html().length) { content_result.html(''); }

        });

        if (nirRegex.test(assuranceCode)) {
            let nir = Number(
                assuranceCode
                    .replace("2A", "19")
                    .replace("2B", "18")
                    .slice(0, assuranceCode.length - 2)
            );

            if ((97 - nir % 97) === Number(assuranceCode.slice(-2))) {
                checkNIROnDB(assuranceCode);

            } else {
                notifyNebula(
                    'Impossible de faire valider ce NIR',
                    'Vérification du NIR',
                    'danger'
                );
            }

        } else {
            notifyNebula(
                'Le numéro de sécurité sociale que vous avez saisi n\'est pas valide!',
                'Vérification du NIR',
                'danger'
            );
            return form_nir[0].reset();
        }
    });

    // IF MODAL IS CLOSED
    modal_nir.on('hidden.bs.modal.',function() {
        location.assign('/');
    });

    // BUTTON NOUVEAU
    btn_new.click(function() {
        form_nir.submit();
    });

}

function isNirProv() {
    let form_nir = $('form[name="control_nir"]'),
        nir = form_nir.find('input[name="idNirDef"]');

    nir.on('input', function() {
        let isProv = form_nir.find('input[name="isNirProv"]'),
            btn_valid = $('#valid_nir'),
            btn_new = $('#new_salarie'),
            prov_array = [3,4,7,8];

        if ($(this).val()) {

            if (prov_array.indexOf(parseInt(this.value.charAt(0))) !== -1) {

                isProv.iCheck('check');

                isProv.on('ifChanged', function() {
                    setTimeout(function() {
                        isProv.iCheck('check');
                    })
                })

            } else {
                isProv.unbind();
                isProv.iCheck('uncheck');
            }

            btn_valid.prop('disabled', !form_nir[0].checkValidity());

            if (btn_new.is(':visible')) {  btn_new.addClass('hide') }

        }
    });

}

async function checkNIROnDB(nir) {
    let form_nir = $('form[name="control_nir"]');

    if (nir && form_nir[0].checkValidity()) {
        let _data = await $.post('/salaries/check/nir', { idNirDef: nir });

        !_data
            ? form_nir.submit()
            : createContentListNir(_data);

    }

}

function createContentListNir(_data) {
    let content = $('#result_nir'),
        isProv = $('input[name="isNirProv"]'),
        btn_new = $('#new_salarie');

    content.removeClass('hide').fadeIn('slow');
    content.html(_data);

    if (isProv.is(':checked')) { btn_new.removeClass('hide') }

    isProv.on('ifChanged', function() {

        $(this).is(':checked')
            ? btn_new.removeClass('hide')
            : btn_new.addClass('hide');

        if (content.html().length) { content.html(''); }

        isProv.unbind();

    });

}

function inputMaskForIdNir() {

    /************ MASK ********/
    $('input[name="idNirDef"]').inputmask({
        mask: '9 99 99 (99|2*) 999 999 99',
        definitions: {
            '*': {
                validator: '[aAbB0-9]',
                casing: 'upper'
            }
        }
    });

}