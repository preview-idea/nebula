function init_justificatif() {
    addRequiredIfPieceExists();
    verifyDates();
}

function addRequiredIfPieceExists() {
    let idTypePiece = $('select[name="idTypePiece"]');

    if (idTypePiece.val()) { getRequiredToJustificatif(idTypePiece.val()); }

    idTypePiece.change(function() {
        getRequiredToJustificatif($(this).val());

    })
}

function getRequiredToJustificatif(typePiece) {
    let liNumeroPiece = $('input[name="liNumeroPiece"]'),
        dtDebutValidite = $('input[name="dtDebutValidite"]'),
        dtFinValidite = $('input[name="dtFinValidite"]');

    if (typePiece) {
        liNumeroPiece.prop('required', true);
        dtDebutValidite.prop('required', true);
        dtFinValidite.prop('required', true);

    } else {
        liNumeroPiece.prop('required', false);
        dtDebutValidite.prop('required', false);
        dtFinValidite.prop('required', false);
    }
}

function verifyDates() {
    let dtDebutValidite = $('input[name="dtDebutValidite"]'),
        dtFinValidite = $('input[name="dtFinValidite"]');

    // IF WE CHANGE DATE FIN
    dtFinValidite.change(function() {

        if ($(this).val().isFormatDateView()) {

            if (dtDebutValidite.val().isFormatDateView()) {
                let date_current = moment().add(1,'weeks'),
                    dtFin = moment($(this).val().convertToDate()),
                    dtDebut = moment(dtDebutValidite.val().convertToDate()),
                    diff_dates = moment($(this).val().convertToDate()).diff(moment().subtract(1, 'days'), 'days');

                if (dtFin.isSameOrBefore(dtDebut)) {
                    $(this).val('');
                    notifyNebula(
                        'La date fin doit être supérieure à la date du début de validité.',
                        'Justificatif',
                        'info'
                    );

                } else if (moment().isAfter(dtFin)) {
                    $(this).val('');
                    notifyNebula(
                        'Attention! La pièce est déjà expirée!!',
                        'Justificatif',
                        'error'
                    );

                } else  if (dtFin.isBefore(date_current)) {
                    let get_date = diff_dates === 7 ? 'une semaine.' : diff_dates + ' jour(s).';

                    notifyNebula(
                        'Attention! La pièce expire dans ' + get_date,
                        'Justificatif',
                        diff_dates < 4 ? 'error' : 'danger'
                    );
                }

            }

        }
    });

}
