function init_table_salarie_hors_cee() {

    createTableForSalarieHorsCee();

}

function createTableForSalarieHorsCee() { "use strict";
    let tableSalarie =  $("#datatable-salarie"),
        order = [1, 'asc'];

    // CREATE INSTANCE DATABASE FOR CONST VARIABLE
    const DATATABLE_SALARIE = tableSalarie.DataTable({
        ajax: {
            url: '/salaries/horscee',
            type: 'POST',
            serverSide: true,
            dataSrc: data => dataTablesFormatDateFromAjax(data)
        },
        columns: [
            { data: 'idMatricule',      title: 'Matricule' },
            { data: 'liNomUsage',       title: 'Nom complet' },
            { data: 'liAgence',         title: 'Agence' },
            { data: 'dtDebutValidite',  title: 'Date début validité' },
            { data: 'dtFinValidite',    title: 'Date fin validité' },
            { data: 'liTypePiece',      title: 'Type pièce' },
            { data: 'liNumeroPiece',    title: 'Numéro pièce' },
            { data: 'liNationalite',    title: 'Nationalité' },
            { data: 'liTypecontrat',    title: 'Type contrat' },
            { data: 'dtDebutcontrat',   title: 'Date début contrat' },
            { data: 'dtFincontrat',     title: 'Date fin contrat' },
            { data: 'column_status',    title: 'Statut' },
        ],
        columnDefs: [
            {
                targets: [1,2,3],
                render: $.fn.dataTable.render.ellipsis( 20, true )
            },
            {
                targets: [8,9,10],
                visible: false,
                searchable: false
            }
        ],
        order: order
    });

    tableSalarie.find('tbody').on('dblclick', 'tr', function() {
        let data = DATATABLE_SALARIE.row( this ).data();

        window.open(`/salaries/detail?idMatricule=${data.idMatricule}`);

    });

    openOneRowInDataTable(tableSalarie);

}
