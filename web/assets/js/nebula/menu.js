$(function() {

    let sub_menu = $('li.sub_menu'),
        gestionContratMenu = $('#gestionContratMenu'),
        iconGestionContrat = $('#iconGestionContrat'),
        chevronRight = 'fa-chevron-right',
        chevronDown = 'fa-chevron-down';

    if (sub_menu.parent().parent().hasClass('current-page')) {
        sub_menu.parent().slideDown();
    }

    gestionContratMenu.click(function () {
        if($(this).parent('li').hasClass('active')) {
            iconGestionContrat.removeClass(chevronRight);
            iconGestionContrat.addClass(chevronDown);
        }
        else {
            iconGestionContrat.removeClass(chevronDown);
            iconGestionContrat.addClass(chevronRight);
        }
    });

});