$(function(){
    console.log('init_hideBlocks');
});

/**
 *
 * @param {Object} opt
 * @constructor
 */
function HideBlocks(opt) {
    this.target = opt.target; // selector type of select
    this.value = opt.values; // array
    this.elements = opt.elements; // array
    this.isHide = opt.isHide; // boolean

    this.init();
}

HideBlocks.prototype = {

    init: function() {

        if (this.target.val()) {
            this.hideParent();

        } else {
            this.target.change(function() {
                this.hideParent();

            }.bind(this))
        }

    },

    hideParent: function() {

        for (let x=0; x < this.elements.length; x++) {

            if (this.value.indexOf(parseInt(this.target.val())) !== -1) {
                if (this.isHide) {
                    this.elements[x].parent().parent().addClass('hide');
                    this.hideElems(this.elements[x].find('input, select'));

                } else {
                    this.elements[x].parent().parent().removeClass('hide');
                    this.resetElems(this.elements[x].find('input, select'));
                }

            } else {
                this.elements[x].parent().parent().removeClass('hide');
                this.resetElems(this.elements[x].find('input, select'));
            }

        } // for
    },

    hideElems: function(el) {

        if (!URL_CURRENT.match(/avenants\/new/)) {
            el.each(function(){
                $(this).is('select') && $(this).is(':hidden') ?
                    $(this).prop('selectedIndex', 0).trigger('change') :
                    $(this).is('input') && $(this).is(':hidden') ?
                        $(this).val('') :
                        $(this).iCheck('uncheck');

                $(this).prop('required', false);
                $(this).parent().find('span.valid-nebula').remove();

            });
        }

    },

    resetElems: function(el) {

        el.each(function() {
            if (['checkbox','radio'].indexOf($(this).prop('type')) === -1 && $(this).is(':visible')) {
                $(this).prop('required', true);
                $(this).parent().append('<span class="valid-nebula"></span>')
            }

        });

    }
};