function init_drop_zone_fileUpload() {
    createInstanceDropzone();
}

function createInstanceDropzone() {
    let btn_remove_file = '<svg class="icon-file-trash" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve"><rect class="noFill" width="22" height="22"/><g><g><path class="fill" d="M16.1,3.6h-1.9V3.3c0-1.3-1-2.3-2.3-2.3h-1.7C8.9,1,7.8,2,7.8,3.3v0.2H5.9c-1.3,0-2.3,1-2.3,2.3v1.3c0,0.5,0.4,0.9,0.9,1v10.5c0,1.3,1,2.3,2.3,2.3h8.5c1.3,0,2.3-1,2.3-2.3V8.2c0.5-0.1,0.9-0.5,0.9-1V5.9C18.4,4.6,17.4,3.6,16.1,3.6z M9.1,3.3c0-0.6,0.5-1.1,1.1-1.1h1.7c0.6,0,1.1,0.5,1.1,1.1v0.2H9.1V3.3z M16.3,18.7c0,0.6-0.5,1.1-1.1,1.1H6.7c-0.6,0-1.1-0.5-1.1-1.1V8.2h10.6V18.7z M17.2,7H4.8V5.9c0-0.6,0.5-1.1,1.1-1.1h10.2c0.6,0,1.1,0.5,1.1,1.1V7z"/></g><g><g><path class="fill" d="M11,18c-0.4,0-0.6-0.3-0.6-0.6v-6.8c0-0.4,0.3-0.6,0.6-0.6s0.6,0.3,0.6,0.6v6.8C11.6,17.7,11.4,18,11,18z"/></g><g><path class="fill" d="M8,18c-0.4,0-0.6-0.3-0.6-0.6v-6.8c0-0.4,0.3-0.6,0.6-0.6c0.4,0,0.6,0.3,0.6,0.6v6.8C8.7,17.7,8.4,18,8,18z"/></g><g><path class="fill" d="M14,18c-0.4,0-0.6-0.3-0.6-0.6v-6.8c0-0.4,0.3-0.6,0.6-0.6c0.4,0,0.6,0.3,0.6,0.6v6.8C14.6,17.7,14.3,18,14,18z"/></g></g></g></svg>',
        max_file_size = 2,
        types_accepted = 'application/pdf';

    Dropzone.options.fileUploadDropzone = {
       paramName: 'fileUpload',
       maxFilesize: max_file_size,
       uploadMultiple: false,
       acceptedFiles: types_accepted,
       addRemoveLinks: true,
       autoQueue: false,

       // Custom messages
       dictRemoveFile: `<span class="remove-file-upload" >${btn_remove_file}</span>`,
       dictFileTooBig: `La taille maximum est de ${max_file_size}M`,

       init: function() {
           let $self = this;

           // Instance global for file upload
           window.INS_DZ_FILEUPLOAD = $self;

           $self.on('success', function(file, path) {
               notifyNebula(
                   'Document enregistré avec succès.',
                   'Dépôt du document',
                   'success'
               );
           })

           // Error
           $self.on('error', function(file, resp, xhr) {
               this.defaultOptions.error(file, 'Une erreur est survenue.');
           })

           // Add
           $self.on('addedfile', function(file) {

               if (!types_accepted.includes(file.type)) {
                   this.emit('removeFile', file);
                   notifyNebula(
                       'Ce type de document n\'est pas autorisé.',
                       'Dépôt du document',
                       'error'
                   );

               }

               // Update file if other file is placed on the drop zone
               // Remove that part if we can put several files
               if (this.files[2]) {
                   this.removeFile(this.files[1])
               }

               if (this.files[1]) {
                   this.files[0].mockFile
                       ? this.files[0].previewElement.classList.add('hide')
                       : this.removeFile(this.files[0])

               }

               // Thumbnail PDF
               this.emit('thumbnail', file, '/assets/images/icon-pdf.png')
           })

           // Remove
           $self.on('removedfile', function(file) {
               if (file.serverPathName) { removeFileFromServer(file.serverPathName); }
           })

           // Send
           $self.on('sending', function(file, xhr, formData) {
               const extraFields = customFormDataBeforeSendFile();

               formData.append('liCheminStock', extraFields.liCheminStock )
               formData.append('customName', extraFields.customName)

           })

           /********** ALL INSTANCE FOR DROPZONE HERE **************/
           // salarie/entretien/fiche_entretien.js
           init_fiche_entretien();

       }
    }

}

async function removeFileFromServer(filePath) {
    let select_type_doc = $('select[name="idTypeDoc"]');

    if (select_type_doc.val()) {
        let cheminStock = select_type_doc.customDataSelect2('liCheminStock');

        const _data = await $.post('/files/upload/remove', { filePath: cheminStock.concat(filePath) })

        if (typeof _data !== "number") {
            if (_data.message) {
                notifyNebula(
                    _data.message,
                    'File upload',
                    'success'
                );
            }
        }

    }
}

function customFormDataBeforeSendFile() {
    let form_doc_file = $('form[name="form_doc_file"]'),
        form_entretien = $('form[name="entretien_form"]'),
        select_type_doc = $('select[name="idTypeDoc"]'),
        select_salarie = form_doc_file.find('select[name="idMatricule"]'),
        dt_realisation = form_doc_file.find('input[name="dtRealisation"]');

    // For salarie detail and dashboard (entretien)
    if (!select_salarie.length) {
        select_salarie = form_entretien.find('select[name="idMatricule"]');
        dt_realisation = form_entretien.find('input[name="dtRealisation"]');
    }

    return  {
        liCheminStock: select_type_doc.customDataSelect2('liCheminStock'),
        customName: getOptionsText(select_salarie)
            .concat('_', new Date(dt_realisation.val().convertToDate()).format("Ymd"))
            .replace(/[-)(]+/g, '').replace(/\s+/g, '_')
            .toUpperCase()
    }

}
