function createFormElementFiles(select_type_doc) {

    // Create variable global
    window.STATE_FORM_FICHIERS = {};

    // Get table ref
    select_type_doc.change(function() {
        let $value = $(this).val();

        if ($value) {
            let tableRef = select_type_doc.customDataSelect2('liTableRef');
            createFormFiles(tableRef);
        }

    })

}
function createFormFiles(tableRef) { "use strict";

    // Design pattern singleton
    if (STATE_FORM_FICHIERS.table_ref !== tableRef) {
        STATE_FORM_FICHIERS.table_ref = tableRef;
        getElementsFormFiles();
    }

    async function getElementsFormFiles() {
        const _data = await $.post('/files/upload/form', STATE_FORM_FICHIERS);

        if (typeof _data !== 'number') {
            if (_data && 'columns' in _data) {
                createInstanceBuilderElements(_data.columns)
            }
        }
    }

    function createInstanceBuilderElements(columns) {
        let _content_files = $('div#file_upload_content div#file_upload_form');

        STATE_FORM_FICHIERS.builder = new BuildFormElementsDOM({ name: 'doc_file_form', class_div: 'col-xs-12' })
        STATE_FORM_FICHIERS.builder.setElements(columns)

        _content_files.html(STATE_FORM_FICHIERS.builder.createForm());

        // Add plugin icheck for all input checkbox
        inputFlat(_content_files.find('input[type="checkbox"]'));
        reloadJS();

        // Add plugin select2 for all selects
        _content_files.find('select').each(function() {
            let $select = $(this);

            // Load list of select from table ref
            $select.attr('table-ref') && !$select.attr('disabled')
                ? new ListOfParam({ target: $select, name: $select.attr('table-ref') })
                : $select.select2({ minimumResultsForSearch: 12, language: 'fr' });

        });

        // List of salaries
        searchSalarieForFileUpload();

        // Check form file
        checkValidityFormFileUpload();
    }

}

function checkValidityFormFileUpload() {
    let form_doc_file_form = $('form[name="doc_file_form"]'),
        form_file_upload_doc = $('form[name="file_upload_doc"]'),
        bnt_form_file = $('#send_form_file');

    if (form_doc_file_form.is(':visible')) {

        form_doc_file_form.find('input:visible, select:visible').on('input change', function() {
            disableAndEnableSendFormFileButton();
        })

        disableAndEnableSendFormFileButton();

        function disableAndEnableSendFormFileButton() {
            bnt_form_file.prop('disabled', !(form_doc_file_form[0].checkValidity() && form_file_upload_doc.find('div.dz-file-preview').length) );
        }
    }

}

