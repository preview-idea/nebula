$(function() {

    // Script pour la page admin utilisateurs
    if (URL_CURRENT.match(/^\/files\/upload/)) {

        // params.js
        init_param_files();

        // drop_zone.js
        init_drop_zone_fileUpload();

    }

});