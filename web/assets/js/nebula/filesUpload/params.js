function init_param_files() {

    getListTypeDoc();

}

function getListTypeDoc() {
    let select_type_doc = $('div#file_upload_content select[name="idTypeDoc"]');

    if (select_type_doc.is(':visible')) {
        new ListOfParam({
            target: select_type_doc,
            name: 'type_doc',
            customData: ['liCheminStock', 'liTableRef']
        })

        // Get elements (columns) for form -> form_document.js
        createFormElementFiles(select_type_doc);

    }
}

function searchSalarieForFileUpload() {
    let select_salarie = $('div#file_upload_content select[name="idMatricule"]');

    if (select_salarie.is(':visible')) {
        const GET_SALARIE = new GetSalaries({
            target: [ select_salarie ]
        });

        // ADD SELECT2 TO SEARCH A SALARIE
        GET_SALARIE.getListToSelectSiteAll();

        if (select_salarie.val()) {
            GET_SALARIE.getNomComplet(true);
        }

        // Check if salarie has planif in progress
        select_salarie.change(function() {
            getPlanificationCurrentBySalarie($(this).val());
        })

    }
}
