$(function(){
    console.log('list_of_combi');

});

/**
 *
 * @param {Object} opt
 * @constructor
 */
function ListOfCombi(opt) {
    this.target = opt.target;
    this.name = opt.name;
    this.targetName = opt.target.prop('name');
    this.libelleName = opt.libelleName || this.targetName.replace('id', 'li');
    this.filter = opt.filter || null;
    this.results = opt.results || 12;
    this.customData = opt.customData || null;
    this.extra = opt.extra || null;
    this.onlyExtra = opt.onlyExtra || false;
    this.showData = opt.showData || false;
    this.sortByLib = opt.sortByLib || false;
    this.allowClear = opt.allowClear || false;

    this.getListToSelect2();

}

ListOfCombi.prototype = {

    getListToSelect2: function() {

        $.getJSON('/combi', {
            filter: this.filter,
            combi: this.name

        })
            .done(function(data){

                if (typeof data !== "number") {
                    const $list = $.map(data, function(d) {
                        if (d[this.libelleName]) {
                            return {
                                id: d[this.targetName],
                                text: d[this.libelleName],
                                selected: data.length === 1,
                                custom_data: this.getCustomData(d)
                            }
                        }

                    }.bind(this));

                    if (!this.onlyExtra) {
                        this.target.select2({
                            data: $list,
                            placeholder: '',
                            allowClear: this.allowClear,
                            minimumResultsForSearch: this.results,
                            language: 'fr'
                        });

                    }

                    if (this.extra) { this.createExtraList($list); }

                    if (this.sortByLib) {
                        this.target.select2({
                            sorter: data => data.sort((a, b) => a.text.toLowerCase() < b.text.toLowerCase() ? 0 : -1)
                        })
                    }

                    if (data.length === 1) { this.target.trigger('change') }

                    if (this.showData) { console.log(data) }

                    chargeFormLabel();

                }

            }.bind(this));

    },

    getCustomData: function(d) {

        if (this.customData) {
            let test = {};

            for (let prop in d) {
                if (d.hasOwnProperty(prop)) {
                    if (this.customData.indexOf(prop) !== -1) {
                        test[prop] = d[prop];
                    }
                }
            }
            return test;
        }

    },

    createExtraList: function(list) {

        this.extra.forEach(function(ex) {
            ex.select2({
                data: list,
                placeholder: '',
                allowClear: this.allowClear,
                minimumResultsForSearch: this.results,
                language: 'fr'

            });

        }.bind(this))

    }

};