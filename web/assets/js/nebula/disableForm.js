$(function() {

    if (URL_CURRENT.match(/avenants\/detail/)
        || URL_CURRENT.match(/salaries\/inactif\/detail/))
    {

        init_disable_form_changes();

    }


});

function init_disable_form_changes() {

    addActionAfterPreload(function() {
        disableFormBlock();
    });

}

function disableFormContratDetail() {
    let btn_modify = $('#update_form');

    !btn_modify.length
        ? init_disable_form_changes()
        : btn_modify.removeClass('hide');
}