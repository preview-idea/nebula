function createSettingsDatePickerFromPeriod({ dtDebut, dtFin, minDate, maxDate }) {
    const dt_debut_rapport = eval(dtDebut)
    const dt_fin_rapport = eval(dtFin)

    const _settings = {
        startDate: moment(),
        minDate: MIN_DATE_RANGE,
        maxDate: MAX_DATE_RANGE,
        ranges: {},
        timeZone: 'Europe/Paris',
        drops: 'auto',
        forceUpdate: true,
        single: true,
        expanded: true,
        periods: ['day','month','quarter','year'],
        period: 'day',
    }

    // Remove content date range picker if it exists
    $('div.daterangepicker').parent().remove();

    $('input[name="dtDebutRapport"], input[name="dtFinRapport"]').each(function() {
        const elem = $(this)
        const _name = elem.prop('name')

        // Remove event blur in this element
        elem.unbind('blur');

        // Define minDate if true
        if (moment.isMoment(dt_debut_rapport)) {

            // Define start date for dtDebut
            if (_name.match(/^dtDebut/)) {
                _settings.startDate = dt_debut_rapport
            }

            if (minDate) {
                const min_date = eval(minDate)
                _settings.minDate = min_date

                if (_name.match(/^dtDebut/)) {
                    elem.on('blur', function() {
                        const $dtValue = moment($(this).val().convertToDate())

                        // Force start date if minDate is defined
                        if ($dtValue.isBefore(min_date)) {
                            $(this).val(min_date.format('L'))
                        }

                    })
                }

            }

            if (_name.match(/^dtDebut/)) {
                elem.on('blur', function() {
                    const $dtValue = moment($(this).val().convertToDate())
                    const dtFin_rapport = $('input[name="dtFinRapport"]').val()

                    if (dtFin_rapport.isFormatDateView()) {
                        const _dt_fin_rapport = moment(dtFin_rapport.convertToDate())

                        // Check date début
                        if ($dtValue.isAfter(_dt_fin_rapport)) {
                            notifyNebula("La date de début ne peut pas être supérieure à la date fin", null, 'info')
                            $(this).val(dt_debut_rapport.format('L'))
                        }
                    }

                })
            }

        }

        // Define maxDate if true
        if (moment.isMoment(dt_fin_rapport)) {

            // Define start date for dtFin
            if (_name.match(/^dtFin/)) {
                _settings.startDate = dt_fin_rapport
            }

            if (maxDate) {
                const max_date = eval(maxDate)
                _settings.maxDate = max_date

                if (_name.match(/^dtFin/)) {
                    elem.on('blur', function() {
                        const $dtValue = moment($(this).val().convertToDate())

                        // Force start date if maxDate is defined
                        if ($dtValue.isAfter(max_date)) {
                            $(this).val(max_date.format('L'))
                        }

                    })
                }

            }

            if (_name.match(/^dtFin/)) {
                elem.on('blur', function() {
                    const $dtValue = moment($(this).val().convertToDate())
                    const dtDebut_rapport = $('input[name="dtDebutRapport"]').val()

                    // Check date fin
                    if (dtDebut_rapport.isFormatDateView()) {
                        const _dt_debut_rapport = moment(dtDebut_rapport.convertToDate())

                        if ($dtValue.isBefore(_dt_debut_rapport)) {
                            notifyNebula("La date fin ne peut pas être infèrieure à la date de début", null, 'info')
                            $(this).val(dt_fin_rapport.format('L'))
                        }
                    }
                })
            }

        }

        // Destroy daterangerpicker before settings
        if (elem.data('daterangepicker')) { elem.removeData('daterangepicker') }
        
        elem.unbind('focus, click')

        // Create instance date range picker
        elem.daterangepicker(_settings, function(start, end, period) {

            if (period) {

                switch (period) {
                    case 'day':
                        $(this).val(start.format('L')).change()

                        break;

                    default:
                        traitValuesDateFromPeriod($(this), start, end)

                }
            }

        })

    })

    chargeFormLabel();

}

function traitValuesDateFromPeriod(elem, start, end) {
    const $name = elem.prop('name')

    if ($name.match(/^dtDebut/)) {
        const _dtFin = elem.parents('form').find('input[name^="dtFin"]');

        elem.val(start.format('L')).change()
        _dtFin.val(end.format('L')).change()

    } else if ($name.match(/^dtFin/)) {
        const _dtDebut = elem.parents('form').find('input[name^="dtDebut"]');

        elem.val(end.format('L')).change()
        _dtDebut.val(start.format('L')).change()
    }
}

function convertDateRapportByTypePeriode(dates = [], type = '') {

    return Array.isArray(dates) && dates.length
        ? dates.map(date => getDateRapportConverted(date))
        : getDateRapportConverted(dates)

    function getDateRapportConverted(date) {
        if (date.isFormatDateView()) { date = date.convertToDate() }

        switch (type) {
            case "YEAR":
                return moment(date).format('YYYY')

            case "MONTH":
                return moment(date).format('YYYYMM')

            case "WEEK":
                return moment(date).format('GGGGWW')

            case "DAY":
                return moment(date).format('YYYYMMDD')

            case "QUARTER":
                return moment(date).format('YYYYQ')

            default:
                return moment(date).format('L')
        }
    }

}