function init_params_rapport() {

    listOfRapport();
    listOfPerimetre();
    getListRapportFormTheme()

}

function getListRapportFormTheme() {
    const select_theme = $('select[name="idTheme"]'),
        li_mot_cle = $('#liMotCle');

    select_theme.unbind()
    select_theme.change(function() {
        li_mot_cle.val("");
        listOfRapport({ filter: { idTheme: $(this).val() }});
    });

}

// LIST OF RAPPORT
async function listOfRapport(filter) {
    const _data = await $.post('/rapport/list', filter);

    if (_data && typeof _data !== "number") {
        if (_data.message) {
            notifyNebula(_data.message, "Rapport", 'info');
        } else {
            const select_rapport =  $('select[name="idRapport"]'),
                $list = _data.map(d => {
                    return {
                        id: d.idRapport,
                        text: d.liRapport,
                        custom_data: { jsPeriode: d.jsPeriode }
                    }
                })

            // Reset select before add list
            select_rapport.find('option:not(:first)').remove();

            // Create select2
            select_rapport.select2({
                data: _.uniq($list),
                minimumResultsForSearch: 10,
                language: 'fr'
            })
        }
    }
}

async function listOfPerimetre() {
    let select_agences = $('select[name="idAgence"]');

    if (select_agences.is(':visible')) {
        const listHierarchie = await $.get('/app/niveaux/hierarchie')
        const listAgences = await $.getJSON('/params', { param: "agence" })

        if (Array.isArray(listAgences) && listAgences.length) {
            const filterListAgence = listAgences.map(agence => agence.idAgence)
            const groupElements = aggregateElementsByNiveauxHierarchie(listHierarchie, { items: filterListAgence, filter_by: 50 });

            // Add perimeter on AppUserMosaic
            AppUserMosaic.perimeter = filterListAgence

            if (typeof groupElements === 'object') {
                Object.values(groupElements).sortArrayBy('title').forEach(item => {
                    recursiveCreateOptionForSelect(item, select_agences)
                })
            }

        }
    }
}

function recursiveCreateOptionForSelect(obj, target, result = [], space = '') {

    if (typeof obj === 'object') {

        if (obj.id && obj.title) {
            const list_id_agence = obj.level !== 50 ? recursiveToGetAllIdAgence(obj) : [obj.id]
            target.append(`<option value="${list_id_agence.join(',')}">${space.concat(obj.title)}</option>`)

            space += '&nbsp;&nbsp;&nbsp;&nbsp;'
            Object.values(obj).forEach(item =>  recursiveCreateOptionForSelect(item, target, result, space))
        }

    }

}

function recursiveToGetAllIdAgence(obj, result = []) {
    if (typeof obj === 'object') {
        Object.values(obj).forEach(item => item.level === 50 ? result.push(item.id) : recursiveToGetAllIdAgence(item, result))
        return result
    }
}


function aggregateElementsByNiveauxHierarchie(elements, criteria = { items: [], filter_by: null }) {
    const { items, filter_by } = criteria

    if (items && filter_by) {
        const find_elements = elements.filter(elem => items.indexOf(elem.id_element) !== -1 && filter_by === elem.id_niveau)
        const parent_elements = getParentElementsForFilter(elements, find_elements)

        elements = parent_elements.concat(find_elements)
    }

    let result = {};
    for (let i=0; i < elements.length; i++) {
        const { id_element, li_element, id_niveau, li_niveau, id_niveau_parent, li_element_parent } = elements[i]

        if (li_niveau && li_element_parent && id_niveau_parent) {

            if (Object.keys(result).length) {
                const find_parent = recursiveToFindParent(result, { level: id_niveau_parent, title: li_element_parent })

                if (!find_parent) {
                    result = createAggregateElements(result, elements[i])

                } else {
                    eval(`result${find_parent}`)[li_element] = {
                        id: id_element,
                        title: li_element,
                        level: id_niveau
                    }
                }

            } else {
                result = createAggregateElements(result, elements[i])
            }

        }

    } // for

    return result

}

function createAggregateElements(result, elem) {

    result[elem.li_element_parent] = {
        id: elem.id_element_parent,
        title: elem.li_element_parent,
        level: elem.id_niveau_parent,
        [elem.li_element]: {
            id: elem.id_element,
            title: elem.li_element,
            level: elem.id_niveau
        }
    }

    return result

}

function recursiveToFindParent(result, parent, path = []) {

    if (typeof result === 'object') {
        if (result[parent.title] && result[parent.title].level === parent.level) {
            path.push(parent.title)
            return path.map(item => `["${item}"]`).join("")
        }

        for (let prop in result) {
            if (result.hasOwnProperty(prop)) {
                if (["id","title","level"].indexOf(prop) === -1) {
                    const _resp = recursiveToFindParent(result[prop], parent, path)

                    if (_resp) {
                        path.unshift(prop)
                        return path.map(item => `["${item}"]`).join("")
                    }

                }
            }
        } // for
    }
}

function getParentElementsForFilter(list, elements) {
    return elements.map(elem => {
        return list.find(item => item.id_element === elem.id_element_parent && item.id_niveau === elem.id_niveau_parent)
    })
}

function verifyFilterPerimeter(obj_filter) {
    const { idAgence } = obj_filter

    if (idAgence) { return _.uniq(idAgence.join(',').split(',')) }
    return AppUserMosaic.perimeter

}