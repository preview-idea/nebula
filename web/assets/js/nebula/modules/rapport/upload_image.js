function init_upload_image_rapport() {

}

function loadImageRapport(idRapport) {
    const image_rapport = $('input[name="uploadImageRapport"]');
    const btn_upload_img = $('#upload_img_rapport');

    btn_upload_img.click(() => image_rapport.click())

    image_rapport.unbind()
    image_rapport.change(function() {
        if (this.files[0]) { saveImageRapport(idRapport, this.files[0]) }
    })

}

async function saveImageRapport(idRapport, file) {
    const uploadImageRapport = new FileUploadNebula();
    const _data = await uploadImageRapport.validateBeforeUploadFile({
        file,
        url: '/rapport/images/upload',
        extensions: ['jpg', 'jpeg', 'png']
    });

    if (_data) {
        let img_uploaded = false;

        const prev_modal_image = $('#prevModalInfoImageRapport');
        const img_object = sprintf('<img src="%s" width="100%" />', `uploads/images/rapports/${_data}`);

        prev_modal_image.find('.modal-body')
            .html(img_object)
            .append('<div class="text-center">' +
                '<button class="btn btn-nebula-default" id="send_img_rapport">Enregistrer</button></div>'
            )

        const btn_send_img_rapport = $('#send_img_rapport');

        btn_send_img_rapport.unbind()
        btn_send_img_rapport.click(function() {
            updateRapportForLiCheminImage(idRapport, _data)
            img_uploaded = true
        })

        prev_modal_image.on('hidden.bs.modal', function() {
            if (!img_uploaded) { $.post('/rapport/images/remove', { filePath: _data }) }
        })
    }
}

async function updateRapportForLiCheminImage(idRapport, liChemin) {

    if (idRapport) {
        const _data = await $.post(sprintf('/rapport/images/update/%d', idRapport), { liChemin })

        if (typeof _data !== "number") {
            notifyNebula('Modification effectuée avec succès', null, 'success')
            setTimeout(function() { location.reload() }, 2000)
        }
    }



}