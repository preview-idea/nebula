function init_rapport() {

    getStructureRapport();
    prepareContentRapportByFilter()

}

function prepareContentRapportByFilter() {

    if (localStorage.getItem('filters_rapport')) {
        const filters_rapport = JSON.parse(localStorage.getItem('filters_rapport'))

        if (filters_rapport) {
            const { obj_rapport, obj_filter, recap } = filters_rapport

            changeTitleContentRapport(recap)
            openModalPreviewFilterRapport(recap)
            previewRapportImage(obj_rapport)

            createStructureRapport({ obj_rapport, obj_filter })

            $('div#content_filter_rapport a.collapse-link').click();
            localStorage.removeItem('filters_rapport')

            historyOfConsultation(obj_rapport.idRapport)

        }

    }

}

function createStructureRapport({ obj_rapport, obj_filter }) {
    const { idRapport, liRapport, sousRapports, jsPeriode } = obj_rapport
    const { dtDebutRapport, dtFinRapport, idAgence, searchFields } = obj_filter

    if (sousRapports) {
        const widgetModels = sousRapports.map(d => {
            return {
                idWidgetModel: d.idModele.idModele,
                liWidgetModel: d.idModele.liModele,
                jsWidgetModel: d.jsStructure,
                criteria: {
                    ...d.jsParams.filtres,
                    ...searchFields,
                    idPeriode: convertDateRapportByTypePeriode([dtDebutRapport, dtFinRapport], jsPeriode.type),
                    idAgence: idAgence,

                },
                viewRef: d.liVueRef,
                typeFormat: jsPeriode.type,
            }
        })

        createContentWidgetsRapport({  idRapport, liRapport, widgetModels })

    }
}

async function createContentWidgetsRapport({ idRapport, liRapport, widgetModels }) {

    delete window.INS_WIDGET_RAPPORT

    window.INS_WIDGET_RAPPORT = new WidgetsMosaic({
        id: 'rapport_'.concat(idRapport),
        name: liRapport
    })

    if (widgetModels) {

        // Define widgets
        await INS_WIDGET_RAPPORT.defineWidgets(widgetModels)

        // Start widgets
        INS_WIDGET_RAPPORT.init()

    }
}

function getStructureRapport() {
    const select_rapport = $('select[name="idRapport"]')

    select_rapport.unbind();
    select_rapport.change(function() {
        const idRapport =  $(this).val();

        if (idRapport) {
            const _jsPeriode = select_rapport.customDataSelect2('jsPeriode')

            resetCustomContentFilterAndContentRapport()
            createSettingsDatePickerFromPeriod(_jsPeriode)
            getStructureByRapport(idRapport);

        }

    });
}

async function getStructureByRapport(idRapport) {
    const _data = await $.getJSON(sprintf('/rapport/detail/%s', idRapport ));

    if (_data && typeof _data !== "number") {
        const { champsRecherche, alerts } = _data

        sendFilterRapport(_data)
        createCustomSearchFields(champsRecherche)
        createAlertDisplay(alerts)
    }
}

function clearContentRapport() {
    const _wPivot = 'rapport_widget_pivot';
    const _wChart = 'rapport_widget_chart';
    const _wStdGrid = $('div#rapport_widget_datatable table');

    // Widget webix
    if ($$(_wPivot)) { $$(_wPivot).destructor();  }
    if ($$(_wChart)) { $$(_wChart).destructor();  }

    // DataTable
    destroyInstanceDataTables(_wStdGrid)
    showAndHideContentRapport(false)
}

function showAndHideContentRapport(display = false) {
    const content_rapport = $('div.block_content_rapport');

    if (display) {
        if (content_rapport.hasClass('hide')) {
            content_rapport.removeClass('hide')
        }
    } else {
        if (!content_rapport.hasClass('hide')) {
            content_rapport.addClass('hide')
        }
    }
}

function changeTitleContentRapport({ idRapport, dtDebutRapport, dtFinRapport }) {
    const title_rapport = $('#nebula_content h2')

    title_rapport.text(sprintf('%s (%s - %s)', idRapport, dtDebutRapport, dtFinRapport))
    showAndHideContentRapport(true)
}



async function historyOfConsultation(idRapport) {
    await $.post('/rapport/consultation', {
        idRapport,
        idUtilisateur: AppUserMosaic.idUser,
        nbConsultation: 1,
        dtDerniereConsult: moment().format("DD/MM/YYYY H:mm:ss"),
        idPeriode: moment().format("YYYYMM") }
    );
}