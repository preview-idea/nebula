function init_filter_rapport() {

    resetFormFilterRapport()
    filterMotCleRapport()
    reinitialiserRapport()
    checkFilterRapport()

}

function filterMotCleRapport() {
    const search_motcle = $('#searchMotCleRapport'),
        li_mot_cle = $('#liMotCle');

    search_motcle.unbind()
    search_motcle.click(function() {
        if (li_mot_cle.val()) {
            listOfRapport({ filter: { liMotCle: li_mot_cle.val() }});
        }
    });
}

function reinitialiserRapport() {
    const send_form_rapport = $("#send_form_rapport_reinitialiser");

    send_form_rapport.unbind();
    send_form_rapport.click(function() {
        const form_filter_rapport = $('form[name="form_filter_rapport"]'),
            form = traitCommonElements(form_filter_rapport[0].elements);

        if (!_.isEmpty(form)) {
            resetFormFilterRapport();
            resetCustomContentFilterAndContentRapport()
        }
    });
}

function resetFormFilterRapport() {
    const form_filter_rapport = $('form[name="form_filter_rapport"]'),
        container_theme = $('#select2-idTheme-container'),
        container_agence = $('#select2-idAgence-container');

    // Reset form filter rapport
    form_filter_rapport[0].reset();
    form_filter_rapport.find('input, select').change();

    // Clear contents
    container_theme.html('')
    container_agence.html('')

}

function sendFilterRapport(obj_rapport) {
    const form_filter_rapport = $('form[name="form_filter_rapport"]'),
        custom_search_fields = $('form[name="form_search_fields"]'),
        send_form_filter = $("#send_form_rapport_filtrer")

    send_form_filter.unbind();
    send_form_filter.click(function() {

        if (form_filter_rapport[0].checkValidity()) {
            const obj_filter = traitCommonElements(form_filter_rapport[0].elements);
            const obj_custom_fileds = traitSearchFieldsElements(custom_search_fields[0].elements);

            if (obj_filter) {

                obj_filter.idAgence = verifyFilterPerimeter(obj_filter)
                obj_filter.searchFields = obj_custom_fileds;
                addToLocalStorageFilters(obj_rapport, obj_filter)

            }
        }
    })

}

function addToLocalStorageFilters(obj_rapport, obj_filter) {
    const form_filter_rapport = $('form[name="form_filter_rapport"]')[0].elements;
    const custom_search_fields = $('form[name="form_search_fields"]')[0].elements;

    const recap = traitElementsFilterForModalPreview(_.union(form_filter_rapport,custom_search_fields))

    if (obj_rapport && obj_filter && recap) {

        localStorage.setItem('filters_rapport', JSON.stringify({
            obj_rapport,
            obj_filter,
            recap
        }))

       location.reload()
    }

}

function checkFilterRapport() {
    const form_filter_rapport = $('form[name="form_filter_rapport"]'),
        send_form_filter = $("#send_form_rapport_filtrer");

    form_filter_rapport.find('input, select').on('input change', function() {
        send_form_filter.attr('disabled', !form_filter_rapport[0].checkValidity())
    })
}

function resetCustomContentFilterAndContentRapport() {
    clearContentRapport();
    resetBlockAlerts();
    resetSearchFields();
}

function traitElementsFilterForModalPreview(e) {
    let r = {};

    r.modal = {}

    for (let i = 0; i < e.length; i++) {
        if ($(e[i]).is(':visible') && $(e[i]).val()) {

            if (['checkbox', 'radio'].includes(e[i].type)) {
                r[e[i].name] = (e[i].checked) ? 'Oui' : 'Non';
                Object.assign(r.modal, {[getLabelElement(e[i])]: (e[i].checked) ? 'Oui' : 'Non'})

            } else if (e[i].name.match(/^id/)) {

                if (['select-one', 'select-multiple'].includes(e[i].type)) {
                    r[e[i].name] = getOptionsText($(e[i]))
                    Object.assign(r.modal, {[getLabelElement(e[i])]: getOptionsText($(e[i]))})

                } else {
                    r[e[i].name] = $(e[i]).val()
                    Object.assign(r.modal, {[getLabelElement(e[i])]: $(e[i]).val()})
                }

            } else {
                r[e[i].name] = $(e[i]).val().trim();
                Object.assign(r.modal, {[getLabelElement(e[i])]: $(e[i]).val().trim()})
            }
        }

    } // for
    return r;
}