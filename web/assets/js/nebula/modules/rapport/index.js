$(function() {

    //Script rapport
    if (URL_CURRENT.match(/^\/rapport$/)) {

        // filter.js
        init_filter_rapport();

        // params.js
        init_params_rapport();

        // rapport.js
        init_rapport();

    }

});