class ClientDwh {

    constructor({  name, baseUri = '' }) {
        this._name = name
        this._baseUri = baseUri
    }

    async request({ method = 'POST', url, criteria }) {
        const response = await $.ajax({ url, type: method, dataType: 'text', data: { criteria } })

        if (typeof response === 'string') {
            return JSON.parse(response)
        }
        return response
    }

    requestStream({ method = 'POST', url, data }) {
        return fetch(this._baseUri.concat(url), {
            method,
            body:  JSON.stringify({ criteria: data })
        })
            .then(response => response.body)
            .then(body => {
                const reader = body.getReader();

                return new ReadableStream({
                    async start(ctrl) {

                        while (true) {
                            const { done, value } = await reader.read();

                            // When no more data needs to be consumed, break the reading
                            if (done) { break; }

                            // Enqueue the next data chunk into our target stream
                            ctrl.enqueue(value)
                        }

                        // Close the stream
                        ctrl.close()
                        reader.releaseLock()

                    }
                })

            })
            .then(stream => new Response(stream))
            .then(response => response.json())
            .then(data => console.log(data))
            .catch(err => console.log(err))
    }

}

// Create client DWH
const ClientDWH = new ClientDwh({ name: 'client_dwh_rapport' })