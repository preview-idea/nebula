/**
 *
 * @param opt
 * @constructor
 */
function ModalForm(opt) {
    this.modal = opt.modal;
    this.contentList = opt.contentList;
    this.identity = opt.identity;
    this._dataIdentity = this.identity.convertCamelCaseFrom('-');
    this.url = opt.url;
    this.toNotEmpty = opt.toNotEmpty;
    this.data = opt.data;
    this.sortBy = [ opt.sortBy[0], opt.sortBy[1] ];

}

ModalForm.prototype = {

    fillModal: function(inputs, id) {
        let data = this.data.find(d => d[this.identity] === id),
            that = this;

        $.each(inputs, function() {
            let name = this.name;

            if (data.hasOwnProperty(name) && !that.toNotEmpty.includes(name)) {
                let value = data[name];

                if (typeof value !== "object") {
                    $(this).val(value);

                } else if (value !== null) {
                    $(this).val(value[name]);

                } else {
                    $(this).val('');

                }

                $(this).trigger('change');
            }
        });
        chargeFormLabel();
    },

    emptyModal: function(inputs) {
        let notEmpty = this.toNotEmpty;

        $.each(inputs, function() {
            if(!notEmpty.includes(this.name)) {
                $(this).val('');
                $(this).trigger('change');
            }
        });
        chargeFormLabel();
    },

    sendData: function(form) {
        let identityValue = form.find(`input[name="${this.identity}"]`).val();

        this.requestAjax(
            identityValue
                ? sprintf('%s/update/%s', this.url, identityValue)
                : sprintf('%s/new', this.url),
            'POST',
            this.getDatas(form)
        );

    },

    removeData: function(form) {
        let identityValue = form.find('input[name="'+this.identity+'"]').val(),
            data = {};

        data[this.identity] = parseInt(identityValue);

        this.requestAjax(
            sprintf('%s/remove/%s', this.url, identityValue),
            'DELETE',
            data
        );
    },

    getDatas: function(form) {
        let inputs = form.find('input, select'),
            obj = {};

        $.each(inputs, function() {

            if ($(this).val()) {
                !this.name.match(/^li/)
                    ? obj[this.name] = Number($(this).val())
                    : obj[this.name] = $(this).val();
            }

        });

        if (obj.hasOwnProperty(this.identity)) { delete obj[this.identity]; }
        return obj;

    },

    setDatas: function(data, method) {
        let identityValue = this.identity;

        if (data && method && data.hasOwnProperty(identityValue)) {

            if (method === 'DELETE') {
                this.data = this.data.filter(d => d[identityValue] !== data[identityValue]);

            } else {
                let indexOf = this.data.map(d => d[identityValue]).indexOf(data[identityValue]);

                indexOf === -1
                    ? this.data.splice(indexOf, 0, data)
                    : this.data.splice(indexOf, 1, data);

                if (Array.isArray(this.sortBy) && this.sortBy.length) {
                    this.data = this.data.sortArrayBy(this.sortBy[0], this.sortBy[1]);
                }

            }

            createListOfAnalytique(this.data);

        }
    },

    requestAjax: function(url, method, send) {
        //call a function declared in the page about (in case of success)
        let that = this;

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: send,
            success: function(data) {

                if (typeof data !== "number") {

                    if (data.hasOwnProperty('message') && method === 'DELETE') {
                        that.setDatas(send,method);
                        that.contentList.find(
                            sprintf('[data-%s=%s]', that._dataIdentity, send[that.identity])
                        ).remove();

                    } else {
                        that.setDatas(data,method);
                    }

                    notifyNebula('Le traitement s\'est bien effectué','Effectué','success');

                } else { console.error(data); }

            },
            error: function() {
                notifyNebula(
                    'Une erreur est apparue lors de l\'enregistrement',
                    'Erreur',
                    'danger'
                );
            }

        });
    }

};

