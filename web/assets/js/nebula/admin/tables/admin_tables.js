function createAdminTableFromDataTable(target) {

    // Destroy existing instance
    destroyInstanceDataTables(target.table);

    target.instance_dataTable = target.table.DataTable({
        ajax: {
            url: target.url.table,
            type: 'POST',
            serverSide: true,
            data: { liAdminTable: target.select.val() },
            dataSrc: data => dataTablesFormatDateFromAjax(data)
        },
        columns: formatColumnsDataTable(target.columns),
        columnDefs: [{
            targets: target.columns.map( (column, index) => {
                // Not ellipsis the ID'S and Column ACTION
                if ([0, target.columns.length].indexOf(index) === -1) { return index  }
            }),
            render: $.fn.dataTable.render.ellipsis( 20, true )
        }]
    });

    actionFormTables(target);

}

function formatColumnsDataTable(columns) {
    if (columns) {
        let $list = [];

        columns.forEach(column => {

            $list.push({
                data: column.liChampTable.underscoreToCamel(),
                title: column.liChampDescription
            })
        });

        // Add columns 'Statut' and 'Action' for button modify
        $list.push(
            { data: 'column_status', title: 'Statut', className: 'dt-body-center' },
            { data: 'column_action.btn_modify', title: 'Action', className: 'dt-body-center' }
        );

        return $list;

    }
}


function actionFormTables(target) {
    let btn_send = $('#send_admin_table'),
        btn_modify = $('#modify_admin_table');

    // Ajout function to show one row with detail
    target.table.unbind();
    target.table.find('tbody').on('click', 'tr', function() {
        openOneRowInDataTable(target.table);

    });

    // Button modify in column (Actions)
    target.btn_action_modify.unbind();
    target.table.on('click', '.btn-modifier', function() {
        let tbody_tr = $(this).parents('tr').hasClass('child') ? $(this).parents('tbody').find('tr.parent') : $(this).parents('tr'),
            modal_admin = $('#modalAdminTables'),
            data = target.instance_dataTable.row(tbody_tr).data();

        // Reload content in the form modal
        createFormAdminTableFromModal(target);
        setValueInTheFormModal(data, target.columns);
        modal_admin.modal('show');

        // Hide button send if it's visible
        if (!btn_send.hasClass('hide')) {
            btn_send.addClass('hide');
            btn_send.prop('disabled', true)
        }

        // Show button modify if it's hidden
        if (btn_modify.hasClass('hide')) {
            btn_modify.removeClass('hide');
            btn_modify.prop('disabled', false);
        }

        // Modify element
        btn_modify.unbind();
        btn_modify.click(function() {
            let idElement = localStorage.getItem('idElement'),
                form_tables_param = document.forms[BUILD_FORM_ELEMENTS.getName()].elements,
                obj_form = traitCommonElements(form_tables_param);

            if (idElement && obj_form) {

                $.post(
                    sprintf(target.url.modify, target.select.val(), idElement),
                    obj_form
                )
                    .done(data => {

                        if (typeof data !== "number") {

                            if (data.hasOwnProperty('bdd_msg') && data.bdd_msg) {
                                notifyNebula(data.bdd_msg, 'Server error 500', 'error')

                            } else {
                                createAdminTableFromDataTable(target);

                                if (!btn_modify.hasClass('hide')) {
                                    btn_modify.addClass('hide');
                                    btn_modify.prop('disabled', true);

                                }

                                localStorage.clear();
                                notifyNebula('Modifications effectuées avec succès!', null, 'success')

                            }

                        }

                        if (data === 400) {
                            console.log(JSON.stringify(obj_form));
                            notifyNebula(`Le formulaire envoyé n'est pas valide avec l'objet du server`, null, 'info');
                        }

                    })
                    .fail( err => console.error(err) )
            }

        })


    });


}

