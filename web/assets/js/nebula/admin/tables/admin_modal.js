function createFormAdminTableFromModal(target) {
    const MODAL_ADMIN_TABLES = $('#modalAdminTables');

    let _content_tables = MODAL_ADMIN_TABLES.find('.modal-body'),
        btn_send = $('#send_admin_table'),
        btn_modify = $('#modify_admin_table');

    if (target.columns) {

        // Set elements to from
        BUILD_FORM_ELEMENTS.setElements(target.columns);
        _content_tables.html(BUILD_FORM_ELEMENTS.createForm());

        // Call function to validate form
        checkValidationFormTables(target);

        // Add plugin icheck for all input checkbox
        inputFlat(_content_tables.find('input[type="checkbox"]'));
        reloadJS();

        // Show the modal with form to add a new data
        target.btn_add.unbind();
        target.btn_add.click(function() {

            if (!target.select.val()) { return  notifyNebula('Vous devez sélectionner une table dans la liste!', null, 'info') }

            if (btn_send.hasClass('hide')) { btn_send.removeClass('hide'); }

            // Disable button modify if it's active
            if (!btn_modify.hasClass('hide')) {
                btn_modify.addClass('hide');

                if (!btn_modify.attr('disabled')) { btn_modify.prop('disabled', 'disabled'); }

            }

            _content_tables.html(BUILD_FORM_ELEMENTS.createForm());

            // Remove disabled for all inputs
            _content_tables.find('input:disabled, select:disabled').each(function() {
                $(this).removeAttr('disabled')
            });

            MODAL_ADMIN_TABLES.modal('show');

            // Call function to validate form
            checkValidationFormTables(target);

            // Add plugin select2 for all selects
            _content_tables.find('select').each(function() {
                let $select = $(this);

                // Load list of select from table ref
                $select.attr('table-ref') && !$select.attr('disabled')
                    ? new ListOfParam({ target: $select, name: $select.attr('table-ref') })
                    : $select.select2({ minimumResultsForSearch: 12, language: 'fr' });

            });

            // Add plugin icheck for all input checkbox
            inputFlat(_content_tables.find('input[type="checkbox"]'));
            reloadJS();

        })

    }
}

// Validation Tables param
function checkValidationFormTables(target) {
    let form_tables = document.forms[BUILD_FORM_ELEMENTS.getName()],
        btn_send = $('#send_admin_table'),
        btn_modify = $('#modify_admin_table');

    // Enable button if the form is valid
    $(form_tables.elements).on('input change', function() {
        if (btn_send.is(':visible')) { btn_send.prop('disabled', !form_tables.checkValidity()) }
        if (btn_modify.is(':visible')) { btn_modify.prop('disabled', !form_tables.checkValidity()) }
    });

    // Save form
    btn_send.unbind();
    btn_send.click(function() {

        if (form_tables.checkValidity()) {
            let obj_form = traitCommonElements(form_tables.elements);

            if (obj_form && target.select.val()) {

                $.post(
                    sprintf(target.url.new, target.select.val()),
                    obj_form
                )
                    .done(data => {
                        if (typeof data !== "number") {

                            if (data.hasOwnProperty('bdd_msg') && data.bdd_msg) {
                                notifyNebula(data.bdd_msg, 'Server error 500', 'error')

                            } else {
                                createAdminTableFromDataTable(target);
                                notifyNebula('Enregistrement effectué avec succès!', null, 'success')
                            }
                        }

                        if (data === 400) {
                            console.log(JSON.stringify(obj_form));
                            notifyNebula(`Le formulaire envoyé n'est pas valide avec l'objet du serveur`, null, 'info');
                        }
                    })
                    .fail(err => console.error(err) )
            }

        }
    });

}

function setValueInTheFormModal(data, columns) {
    let idElement = columns.filter(column => column.liTypeElementUi === 'hidden')[0].liChampTable.underscoreToCamel(),
        elements = document.forms[BUILD_FORM_ELEMENTS.getName()].elements;

    // Set ID in local storage.
    localStorage.setItem('idElement', data[idElement]);

    for (let i=0; i < elements.length; i++) {

        for (let prop in data) {

            if (data.hasOwnProperty(prop)) {

                if (elements[i].name === prop) {

                    if (elements[i].type === 'checkbox') {

                        data[prop] === 1 || data[prop] === 'Oui'
                            ? $(elements[i]).iCheck('check')
                            : $(elements[i]).iCheck('uncheck');

                    } else if (elements[i].type === 'select-one') {
                        const select = JSON.parse(localStorage.getItem('selects'))
                            .filter(select => select.name === prop && select.text === data[prop])[0];

                        if (select) {
                            $(elements[i]).append(new Option(select.text, select.id, true, true)).trigger('change');

                            // Load list of select from table ref
                            $(elements[i]).attr('table-ref') && !$(elements[i]).attr('disabled')
                                ? new ListOfParam({ target:  $(elements[i]), name:  $(elements[i]).attr('table-ref') })
                                : $(elements[i]).select2({ minimumResultsForSearch: 12, language: 'fr' });
                        }

                    } else {
                        $(elements[i]).val(data[prop]).trigger('change');

                    }

                    if (elements[i].hasAttribute('disabled')) {

                        switch (elements[i].type) {
                            case 'select-one':
                                removeOptions($(elements[i]));
                                break;

                            case 'checkbox':
                                disableCheckbox($(elements[i]));
                                break;

                            default:
                                disableInput($(elements[i]));
                                break;
                        }
                    }

                }

            }

        } // for
    }

}
