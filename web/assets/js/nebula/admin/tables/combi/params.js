function getModelsCombiForSelect() {
    let select_combi = $('select[name="liTableCombi"]'),
        $list = [];

    $list.push(
        ObjCombiAnalytique,
        ObjCombiCategorieCoeff,
        ObjCombiCometeTempsTravail,
        ObjCombiNaturecontratTypeavenant,
        ObjCombiRelationTypeNature,
        ObjCombiSocieteEtablissementAgence,
        ObjCombiPerimetreElectoralCse
    );

    select_combi.select2({
        data: $list.map(d => {
            return {
                id: d.liTable.underscoreToCamel(true),
                text: d.liTableDescription,
                custom_data: { columns: d.columns }
            }
        }),
        minimumResultsForSearch: 12,
        language: 'fr'
    })

}
