function init_combi_tables_admin() {
    localStorage.clear();
    getElementsCombi();

    // On attends d'abord qui les modèles soient déclarés dans la variable global window.
    setTimeout(function() {
        // params.js
        getModelsCombiForSelect();
    })

}

//ADD ACTION WHEN A COMBI TABLE IS SELECTED
function getElementsCombi() {
    let select_combi = $('select[name="liTableCombi"]'),
        btn_add_combi = $('#add_table_combi');

    btn_add_combi.click(() => {
        if (!select_combi.val()) {
            notifyNebula('Vous devez sélectionner une table dans la liste!', null, 'info')
        }
    });

    select_combi.change(function() {
        if ($(this).val()) {
            const OBJECT_TABLE_COMBI = {
                select: select_combi,
                columns: select_combi.customDataSelect2('columns'),
                table: $('#datatable-tables-combi'),
                btn_add: btn_add_combi,
                btn_action_modify: $('.btn-modifier'),
                btn_modify_table: $('#modify_target_table'),
                url: {
                    table: '/admin/tables/combi',
                    new: '/admin/tables/combi/%d/new',
                    modify: '/admin/tables/combi/%s/update/%d'
                }
            };

            createFormAdminTableFromModal(OBJECT_TABLE_COMBI);
            createAdminTableFromDataTable(OBJECT_TABLE_COMBI);
        }

    })
}


