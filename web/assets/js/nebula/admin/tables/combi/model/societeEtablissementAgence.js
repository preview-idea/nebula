if (URL_CURRENT.match(/^\/admin\/tables$/)) {

    ObjCombiSocieteEtablissementAgence = {
        liTable: 'combi_societe_etablissement_agence',
        liTableDescription: 'Société Etablissement Agence',
        columns: [
            {
                liChampTable: 'id_ligne_combi_sea',
                liTypeElementUi: 'hidden',
                liChampTaille: '32',
                liChampDescription: 'ID',
                liChampTableRef: null,
                isChampObligatoire: 1,
                isAdministrable: 0
            },
            {
                liChampTable: 'id_societe',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Société',
                liChampTableRef: 'societe',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_agence',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Agence',
                liChampTableRef: 'agence',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_etablissement',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Etablissement',
                liChampTableRef: 'etablissement',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'li_departement_mobilite',
                liTypeElementUi: 'text',
                liChampTaille: '65',
                liChampDescription: 'Département de mobilité',
                liChampTableRef: null,
                isChampObligatoire: 0,
                isAdministrable: 1
            }
        ]
    };

}