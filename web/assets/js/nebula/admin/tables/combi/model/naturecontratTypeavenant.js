if (URL_CURRENT.match(/^\/admin\/tables$/)) {

    ObjCombiNaturecontratTypeavenant = {
        liTable: 'combi_naturecontrat_typeavenant',
        liTableDescription: 'Nature Contrat Type Avenant',
        columns: [
            {
                liChampTable: 'id_ligne_naturecontrat_typeavenant',
                liTypeElementUi: 'hidden',
                liChampTaille: '32',
                liChampDescription: 'ID',
                liChampTableRef: null,
                isChampObligatoire: 1,
                isAdministrable: 0
            },
            {
                liChampTable: 'id_naturecontrat',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Nature Contrat',
                liChampTableRef: 'naturecontrat',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_typeavenant',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Type Avenant',
                liChampTableRef: 'typeavenant',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_positionnementposte',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Positionnement Poste',
                liChampTableRef: 'positionnementposte',
                isChampObligatoire: 1,
                isAdministrable: 1
            }

        ]
    };

}