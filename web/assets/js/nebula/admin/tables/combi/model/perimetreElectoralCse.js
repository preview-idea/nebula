if (URL_CURRENT.match(/^\/admin\/tables$/)) {

    ObjCombiPerimetreElectoralCse = {
        liTable: 'combi_perimetre_electoral_cse',
        liTableDescription: 'Périmètre Electoral Cse',
        columns: [
            {
                liChampTable: 'id_ligne_perimetre_electoral_cse',
                liTypeElementUi: 'hidden',
                liChampTaille: '32',
                liChampDescription: 'ID',
                liChampTableRef: null,
                isChampObligatoire: 1,
                isAdministrable: 0
            },
            {
                liChampTable: 'id_perimetre_electoral',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Périmètre Electoral',
                liChampTableRef: 'perimetre_electoral',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_cse',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Cse',
                liChampTableRef: 'cse',
                isChampObligatoire: 1,
                isAdministrable: 1
            }
        ]
    };

}
