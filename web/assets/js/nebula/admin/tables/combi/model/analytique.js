if (URL_CURRENT.match(/^\/admin\/tables$/)) {

    ObjCombiAnalytique = {
        liTable: 'combi_analytique',
        liTableDescription: 'Analytique',
        columns: [
            {
                liChampTable: 'id_ligne_analytique',
                liTypeElementUi: 'hidden',
                liChampTaille: '32',
                liChampDescription: 'ID',
                liChampTableRef: null,
                isChampObligatoire: 1,
                isAdministrable: 0
            },
            {
                liChampTable: 'id_positionnementposte',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Positionnement Poste',
                liChampTableRef: 'positionnementposte',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_bu',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Bu',
                liChampTableRef: 'bu',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_region',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Region',
                liChampTableRef: 'region',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_agence',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Agence',
                liChampTableRef: 'agence',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_activite',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Activite',
                liChampTableRef: 'activite',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_metier',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Metier',
                liChampTableRef: 'metier',
                isChampObligatoire: 0,
                isAdministrable: 1
            }
        ]
    };
}