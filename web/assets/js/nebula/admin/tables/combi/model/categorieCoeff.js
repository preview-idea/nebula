if (URL_CURRENT.match(/^\/admin\/tables$/)) {

    ObjCombiCategorieCoeff = {
        liTable: 'combi_categorie_coeff',
        liTableDescription: 'Catégorie Coeff',
        columns: [
            {
                liChampTable: 'id_ligne_categorie_coeff',
                liTypeElementUi: 'hidden',
                liChampTaille: '32',
                liChampDescription: 'ID',
                liChampTableRef: null,
                isChampObligatoire: 1,
                isAdministrable: 0
            },
            {
                liChampTable: 'id_conventioncollective',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Convention Collective',
                liChampTableRef: 'conventioncollective',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_coeffcontrat',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Coeff Contrat',
                liChampTableRef: 'coeffcontrat',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_categorieemploye',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Catégorie Employé',
                liChampTableRef: 'categorieemploye',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'li_niveau',
                liTypeElementUi: 'text',
                liChampTaille: '4',
                liChampDescription: 'Niveau',
                liChampTableRef: null,
                isChampObligatoire: 0,
                isAdministrable: 1
            },
            {
                liChampTable: 'li_echelon',
                liTypeElementUi: 'text',
                liChampTaille: '2',
                liChampDescription: 'Echelon',
                liChampTableRef: null,
                isChampObligatoire: 0,
                isAdministrable: 1
            },
            {
                liChampTable: 'li_position',
                liTypeElementUi: 'text',
                liChampTaille: '5',
                liChampDescription: 'Activite',
                liChampTableRef: null,
                isChampObligatoire: 0,
                isAdministrable: 1
            },
            {
                liChampTable: 'nb_taux_horaire',
                liTypeElementUi: 'number',
                liChampTaille: '11,4',
                liChampDescription: 'Taux Horaire',
                liChampTableRef: null,
                isChampObligatoire: 0,
                isAdministrable: 1
            },
            {
                liChampTable: 'li_niveau_rhpi',
                liTypeElementUi: 'text',
                liChampTaille: '4',
                liChampDescription: 'Niveau RHPI',
                liChampTableRef: null,
                isChampObligatoire: 0,
                isAdministrable: 1
            },
            {
                liChampTable: 'nb_salaire_mensuel',
                liTypeElementUi: 'number',
                liChampTaille: '11,2',
                liChampDescription: 'Salaire Mensuel',
                liChampTableRef: null,
                isChampObligatoire: 0,
                isAdministrable: 1
            },
            {
                liChampTable: 'li_code_comete',
                liTypeElementUi: 'text',
                liChampTaille: '5',
                liChampDescription: 'Position RHPI',
                liChampTableRef: null,
                isChampObligatoire: 0,
                isAdministrable: 1
            }
        ]
    };
}
