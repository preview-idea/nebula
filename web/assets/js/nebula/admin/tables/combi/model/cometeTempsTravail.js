if (URL_CURRENT.match(/^\/admin\/tables$/)) {

    ObjCombiCometeTempsTravail = {
        liTable: 'combi_comete_temps_travail',
        liTableDescription: 'Comete Temps Travail',
        columns: [
            {
                liChampTable: 'id_ligne_comete_temps_travail',
                liTypeElementUi: 'hidden',
                liChampTaille: '32',
                liChampDescription: 'ID',
                liChampTableRef: null,
                isChampObligatoire: 1,
                isAdministrable: 0
            },
            {
                liChampTable: 'id_temps_travail',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Temps Travail',
                liChampTableRef: 'tempsTravail',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_decompte_temps_travail',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Decompte Temps de Travail',
                liChampTableRef: 'decompteTempsTravail',
                isChampObligatoire: 0,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_societe',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Société',
                liChampTableRef: 'societe',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_categorie_comete',
                liTypeElementUi: 'text',
                liChampTaille: '20',
                liChampDescription: 'Catégorie COMETE',
                liChampTableRef: null,
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_cycle_comete',
                liTypeElementUi: 'text',
                liChampTaille: '20',
                liChampDescription: 'Cycle COMETE',
                liChampTableRef: null,
                isChampObligatoire: 1,
                isAdministrable: 1
            }
        ]
    };

}