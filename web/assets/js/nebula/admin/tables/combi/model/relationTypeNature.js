if (URL_CURRENT.match(/^\/admin\/tables$/)) {

    ObjCombiRelationTypeNature = {
        liTable: 'combi_relation_type_nature',
        liTableDescription: 'Relation Type Nature',
        columns: [
            {
                liChampTable: 'id_ligne_relation_type_nature',
                liTypeElementUi: 'hidden',
                liChampTaille: '32',
                liChampDescription: 'ID',
                liChampTableRef: null,
                isChampObligatoire: 1,
                isAdministrable: 0
            },
            {
                liChampTable: 'id_naturecontrat',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Nature Contrat',
                liChampTableRef: 'naturecontrat',
                isChampObligatoire: 1,
                isAdministrable: 1
            },
            {
                liChampTable: 'id_typecontrat',
                liTypeElementUi: 'select',
                liChampTaille: '32',
                liChampDescription: 'Type Contrat',
                liChampTableRef: 'typecontrat',
                isChampObligatoire: 1,
                isAdministrable: 1
            }
        ]
    };

}