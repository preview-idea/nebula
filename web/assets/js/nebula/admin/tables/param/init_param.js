function init_param_tables_admin() {
    localStorage.clear();
    getElementsParam();

    // params.js
    getListTablesParam();

}

function getElementsParam() {
    let select_param = $('select[name="liTableParam"]'),
        btn_add_param = $('#add_table_param');

    btn_add_param.click(() => {
        if (!select_param.val()) {
            notifyNebula('Vous devez sélectionner une table dans la liste!', null, 'info')
        }
    });

    select_param.change(function() {

        if ($(this).val()) {
            const OBJECT_TABLE_PARAM = {
                select: select_param,
                columns: select_param.customDataSelect2('columns'),
                table: $('#datatable-tables-param'),
                btn_add: btn_add_param,
                btn_action_modify: $('.btn-modifier'),
                btn_modify_table: $('#modify_target_table'),
                url: {
                    table: '/admin/tables/params',
                    new: '/admin/tables/params/%d/new',
                    modify: '/admin/tables/params/%s/update/%d'
                }
            };

            createFormAdminTableFromModal(OBJECT_TABLE_PARAM);
            createAdminTableFromDataTable(OBJECT_TABLE_PARAM);

        }
    })
}

