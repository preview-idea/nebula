function getListTablesParam() {
    let select_tables = $('select[name="liTableParam"]');

    if (select_tables.is(':visible')) {
        $.getJSON('/admin/tables/params')
            .done(data => {

                if (typeof data !== "number") {

                    select_tables.select2({
                        data: data.params.map(d => {
                            return {
                                id: d.liTable.underscoreToCamel(true),
                                text: d.liTableDescription,
                                custom_data: { columns: d.columns }
                            }
                        }),
                        minimumResultsForSearch: 12,
                        language: 'fr'
                    })
                }
            })
            .fail(err => console.error(err))
    }

}