$(function() {

    // Script pour la page admin tables
    if (URL_CURRENT.match(/^\/admin\/tables$/)) {

        // Instance of class buildFormElementDOM
        BUILD_FORM_ELEMENTS = new BuildFormElementsDOM({
            name: 'administration_tables',
        });

        // Scripts tables param
        init_param_tables_admin();

        // Scripts tables combi
        init_combi_tables_admin();

        // Script tables gestion droit
        init_gestion_droit_tables_admin();

        // Script param applications
        init_params_users_admin();

    }

});