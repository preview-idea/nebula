function init_reset_page_tables_admin() {
    let menu_param = $('a[href="#table_param"]'),
        menu_combi = $('a[href="#table_combi"]');

    // RESET SELECT COMBI
    menu_param.click(() => {
        let select_combi = $('select[name="liTableCombi"]'),
            table_combi = $('#datatable-tables-combi');

        localStorage.clear();
        select_combi.val('').trigger('change');
        destroyInstanceDataTables(table_combi);
    });

    // RESET SELECT PARAM
    menu_combi.click(() => {
        let select_param = $('select[name="liTableParam"]'),
            table_param = $('#datatable-tables-param');

        localStorage.clear();
        select_param.val('').trigger('change');
        destroyInstanceDataTables(table_param);

    });
}