function createDataTableQualifContrat(target) {

    // Destroy existing instance
    destroyInstanceDataTables(target.table);

    target.instance_dataTable = target.table.DataTable({
        ajax: {
            url: target.url.table,
            type: 'POST',
            serverSide: true,
            data: { liAdminTable: target.select.val() },
            dataSrc: data => dataTablesFormatDateFromAjax(data)
        },
        columns: target.columns,
        columnDefs: [{
            targets: [1,2,3,4],
            render: $.fn.dataTable.render.ellipsis( 20, true )
        }]
    });

    updateTableQualifContrat(target);
    removeTableQualifContrat(target);
}

function removeRowQualifContrat(target) {
    let idQualifcontrat = localStorage.getItem('idQualifcontrat');

    if (idQualifcontrat) {

        $.post(sprintf(target.url.remove,idQualifcontrat))
            .done(data => {

                if (typeof data !== "number") {

                    createDataTableQualifContrat(target);
                    notifyNebula('La Qualif contrat a bien été supprimée', 'Qualif Contrat', 'success');
                    localStorage.removeItem('idQualifcontrat');
                }

            })
            .fail(err => console.error(err))

    }

}

function removeTableQualifContrat(target) {

    // BUTTON REMOVE ICON
    target.btn_action_remove.unbind();
    target.table.on('click', '.btn-supprimer', function() {
        let confirm_msg = '<h5 class="text-center">Êtes-vous vraiment sûr de vouloir supprimer cette qualif contrat ?</h5><p class="text-center">La ligne sera définitivement supprimée.</p>',
            tbody_tr = $(this).parents('tr').hasClass('child') ? $(this).parents('tbody').find('tr.parent') : $(this).parents('tr'),
            data = target.instance_dataTable.row(tbody_tr).data(),
            confirm_yes = $('#confirm_yes');

        // Save id in the local storage
        localStorage.setItem('idQualifcontrat', data.idQualifcontrat);

        MODAL_CONFIRM.find('div.modal-body').html(confirm_msg);
        MODAL_CONFIRM.modal('show');
        MODAL_CONFIRM.prop('action', 'remove-qualifcontrat');

        // CONFIRMATION
        confirm_yes.unbind();
        confirm_yes.click(function() {
            if (MODAL_CONFIRM.prop('action') === 'remove-qualifcontrat') {
                removeRowQualifContrat(target);
            }
        });

    });

}

function updateTableQualifContrat(target) {
    let btn_send = $('#send_admin_qualifcontrat'),
        btn_modify = $('#modify_admin_qualifcontrat');

    target.table.unbind();
    target.table.on('click', '.btn-modifier', function() {
        let tbody_tr = $(this).parents('tr').hasClass('child') ? $(this).parents('tbody').find('tr.parent') : $(this).parents('tr'),
            modal_admin = $('#modalAdminQualifContrat'),
            data = target.instance_dataTable.row(tbody_tr).data(),
            idQualifcontrat_value = data.idQualifcontrat,
            liQualifcontrat_value = data.liQualifcontrat,
            isQualifcontratStructure = $('#isQualifcontratStructure');

        // Hide button send if it's visible
        if (!btn_send.hasClass('hide')) {
            btn_send.addClass('hide');
            btn_send.prop('disabled', true)
        }

        // Show button modify if it's hidden
        if (btn_modify.hasClass('hide')) {
            btn_modify.removeClass('hide');
            btn_modify.prop('disabled', false);
        }

        resetFormQualifContrat();

        $.getJSON(target.url.detail, {
            idQualifcontrat: data.idQualifcontrat
        })
            .done(function(data) {

                if (typeof data !== "number") {
                    let idNiveauRef = $('#idNiveauRef'),
                        idQualifcontrat = $('#idQualifcontrat'),
                        liQualifcontrat = $('#liQualifcontrat'),
                        isDroitVoirStructure = $('#isDroitVoirStructure'),
                        form_tables = document.forms['qualifcontrat'],
                        content = $('#list_user_right'),
                        btn_remove_app_right = $('#remove_app_right');

                    new ListOfParam({
                        target: idNiveauRef,
                        targetName: 'idNiveau',
                        url: '/admin/tables/droits/niveauxent',
                        selectedValue: data.idNiveauRef.idNiveau
                    });

                    idQualifcontrat.find('option').remove();
                    idQualifcontrat.append(new Option(liQualifcontrat_value, idQualifcontrat_value, true, true));

                    data.isDroitVoirStructure === 1 ?
                        isDroitVoirStructure.iCheck('check') :
                        isDroitVoirStructure.iCheck('uncheck');

                    if (data.jsRole) {
                        let object = JSON.parse(data.jsRole),
                            html = '<ul class="to_do">\n';

                        Object.keys(object).forEach(function(key) {
                            html += createCheckboxRole(object[key]);
                        });

                        html += '</ul>\n';

                        content.html(html);
                        btn_remove_app_right.removeClass('hide');

                        let checkbox = content.find('input.flat');
                        inputFlat(checkbox);
                    }

                    addRole();
                    removeRole(btn_remove_app_right);
                    modal_admin.modal('show');

                    // Save form
                    btn_modify.unbind();
                    btn_modify.click(function() {

                        if (form_tables.checkValidity()) {

                            //autofill input hidden
                            liQualifcontrat.val(idQualifcontrat.find('option:selected').text());
                            isQualifcontratStructure.val(1);

                            let obj_form = traitCommonElements(form_tables.elements);
                            obj_form.jsRole = traitCheckRoleElements();

                            if (obj_form) {

                                console.log(JSON.stringify((obj_form)))

                                $.post(
                                    sprintf(target.url.update,idQualifcontrat_value),
                                    obj_form
                                )
                                    .done(data => {
                                        if (typeof data !== "number") {

                                            if (data.hasOwnProperty('bdd_msg') && data.bdd_msg) {
                                                notifyNebula(data.bdd_msg, 'Server error 500', 'error')

                                            } else {
                                                createDataTableQualifContrat(target);
                                                notifyNebula('Modification effectuée avec succès!', null, 'success')
                                            }

                                        }
                                    })
                                    .fail(err => console.error(err) )
                            }

                        }
                    });

                }

            })

            .fail(function(error) {
                console.error(error);
            })

    });
}

function createCheckboxRole(role) {
    let html = '';

    html += '<li class="user-app-right">\n';
    html += sprintf('<p><input type="checkbox" class="flat" name="idApplication" data-alias="%s">%s</p>\n',role,role);
    html += '</li>\n';

    return html;
}

function initModalAddQualifContrat(modal) {

    resetFormQualifContrat();

    let idQualifcontrat = $('#idQualifcontrat'),
        idNiveauRef = $('#idNiveauRef'),
        btn_remove_app_right = $('#remove_app_right');

    new ListOfParam({
        target: idNiveauRef,
        targetName: 'idNiveau',
        url: '/admin/tables/droits/niveauxent'
    });

    new ListOfParam({
        target: idQualifcontrat,
        targetName: 'idQualifcontrat',
        url: '/admin/tables/droits/qualifcontrat'

    });

    addRole();
    removeRole(btn_remove_app_right);

    modal.modal('show');

}

function addRole() {
    let form_app_right = $('#list_apps_right form[name="user_app_right"]'),
        inputs_app_right = form_app_right.find('input, select'),
        btn_add_apps_right = $('#add_app_right'),
        btn_remove_app_right = $('#remove_app_right');

    // SEND FORM APP RIGHT
    inputs_app_right.on('input change', function() {
        btn_add_apps_right.prop('disabled', !form_app_right[0].checkValidity());
    });

    // ADD APP RIGHT INTO LIST
    btn_add_apps_right.unbind();
    btn_add_apps_right.click(function() {

        if (form_app_right[0].checkValidity()) {
            let select_apps = form_app_right.find('select[name="idApplication"]'),
                obj_form = traitCommonElements(form_app_right[0].elements),
                html = '',
                htmlEnd = '',
                list_user_right = $('#list_user_right'),
                content,
                role = getUserRole(select_apps, obj_form.isEcriture);

            if (btn_remove_app_right.hasClass('hide')) {
                btn_remove_app_right.removeClass('hide');
            }

            if (list_user_right.find('ul').children().length === 0) {
                html = '<ul class="to_do">\n';
                htmlEnd = '</ul>\n';
                list_user_right.html(html);
            }

            content = list_user_right.find('ul');

            if (!content.html().includes(role)) {
                html = createCheckboxRole(role);
                html += htmlEnd;
                content.append(html);
                let checkbox = list_user_right.find('input.flat');
                inputFlat(checkbox);
            }

        }
    })
}

function createTableQualifContrat(target) {
    let form_tables = document.forms['qualifcontrat'],
        btn_send = $('#send_admin_qualifcontrat'),
        btn_modify = $('#modify_admin_qualifcontrat'),
        idQualifcontrat = $('#idQualifcontrat'),
        liQualifcontrat = $('#liQualifcontrat'),
        isDroitVoirStructure = $('#isDroitVoirStructure');

    // Enable button if the form is valid
    $(form_tables.elements).on('input change', function() {
        if (btn_send.is(':visible')) { btn_send.prop('disabled', !form_tables.checkValidity()) }
        if (btn_modify.is(':visible')) { btn_modify.prop('disabled', !form_tables.checkValidity()) }
    });

    createDataTableQualifContrat(target);

    // Save form
    btn_send.unbind();
    btn_send.click(function() {

        if (form_tables.checkValidity()) {

            //autofill input hidden
            liQualifcontrat.val(idQualifcontrat.find('option:selected').text());
            isDroitVoirStructure.val(1);

            let obj_form = traitCommonElements(form_tables.elements);
            obj_form.jsRole = traitCheckRoleElements();

            if (obj_form) {

                console.log(JSON.stringify(obj_form))

                $.post(
                    target.url.new,
                    obj_form
                )
                    .done(data => {
                        if (typeof data !== "number") {

                            if (data.hasOwnProperty('bdd_msg') && data.bdd_msg) {
                                notifyNebula(data.bdd_msg, 'Server error 500', 'error')

                            } else {
                                createDataTableQualifContrat(target);
                                notifyNebula('Enregistrement effectué avec succès!', null, 'success')
                            }

                        }
                    })
                    .fail(err => console.error(err) )
            }

        }
    });

}

function resetFormQualifContrat() {
    let form_tables = document.forms['qualifcontrat'],
        isDroitVoirStructure = $('#isDroitVoirStructure'),
        idQualifcontrat = $('#idQualifcontrat'),
        remove_app_right = $('#remove_app_right'),
        list_user_right = $('#list_user_right'),
        isEcriture = $('#isEcriture');

    localStorage.removeItem('roles');
    isDroitVoirStructure.iCheck('uncheck');
    isEcriture.prop('checked', false);
    idQualifcontrat.find('option').remove().end().append('<option value hidden></option>');
    $("#idApplication").val('').trigger('change');
    remove_app_right.addClass('hide');
    list_user_right.find('ul').remove();
    list_user_right.html('<p class="text-center">Aucun rôle attribué.</p>');
    form_tables.reset();

}

function traitCheckRoleElements() {
    let check = $('#list_user_right input:checkbox'),
        roles = {},
        i = 0;

    check.each(function() {
        let $checkbox = $(this);

        if (this.value) {

            roles[i] = $checkbox.data('alias');
            i++;

        }

    });

    return JSON.stringify(roles);
}

function removeRole(btn_remove_app_right) {

    btn_remove_app_right.unbind();
    btn_remove_app_right.click(function() {
        let check = $('#list_user_right input:checkbox'),
            remove_app_right = $('#remove_app_right'),
            list_user_right = $('#list_user_right');

        if (check.length > 0) {

            check.each(function() {
                let checked = $(this);
                if (checked.is(':checked')) {
                    checked.parents('li.user-app-right').remove();

                    if (list_user_right.find('ul').children().length === 0) {
                        list_user_right.find('ul').remove();
                        remove_app_right.addClass('hide');
                        list_user_right.html('<p class="text-center">Aucun rôle attribué.</p>');
                    }
                }

            });

        }
    });

}