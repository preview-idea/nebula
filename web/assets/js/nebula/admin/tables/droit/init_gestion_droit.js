function init_gestion_droit_tables_admin() {
    localStorage.clear();
    getElementsGestionDroit();

}

function getElementsGestionDroit() {
    let select_gestion_droit = $('select[name="liTableParamGestionDroit"]'),
        btn_add_gestion_droit = $('#add_table_param_gestion_droit'),
        btn_modify = $('.btn-modifier'),
        btn_remove = $('.btn-supprimer'),
        table = $('#datatable-tables-param-gestion-droit');

    // QUALIF CONTRAT
    const MODAL_ADMIN_TABLES_QUALIF_CONTRAT = $('#modalAdminQualifContrat');

    const OBJECT_TABLE_QUALIF_CONTRAT = {
        select: select_gestion_droit,
        columns: [
            { data: 'idQualifcontrat',                                                          title: 'ID'},
            { data: 'liQualifcontrat',                                                          title: 'Qualif Contrat'},
            { data: 'liNiveau',                                                                 title: 'Niveau agence'},
            { data: 'isDroitVoirStructure',                                                     title: 'Voir droit structure'},
            { data: 'jsRole',                                                                   title: 'Roles'},
            { data: data => data.column_action.btn_modify + data.column_action.btn_remove,      title: 'Action', className: 'dt-body-center' }
        ],
        btn_action_modify: btn_modify,
        btn_action_remove: btn_remove,
        table: table,
        url: {
            table: '/admin/tables/droits',
            new: '/admin/tables/droits/qualifcontrat/new',
            update: '/admin/tables/droits/qualifcontrat/update/%d',
            detail: '/admin/tables/droits/qualifcontrat/detail',
            remove: '/admin/tables/droits/qualifcontrat/remove/%d'
        }
    };

    //COMBI NIVEAUX ELEMENTS
    const OBJECT_TABLE_COMBI_NIVEAUX_ELEMENTS = {
        select: select_gestion_droit,
        columns: [
            { data: 'idLigneCombiNiveauxElements',                                          title: 'ID'},
            { data: 'liNiveau',                                                             title: 'Niveau'},
            { data: 'liElement',                                                            title: 'Element'},
            { data: 'liNiveauParent',                                                       title: 'Niveau Parent'},
            { data: 'liElementParent',                                                      title: 'Element Parent'},
            { data: data => data.column_action.btn_modify + data.column_action.btn_remove,  title: 'Action', className: 'dt-body-center' }
        ],
        btn_action_modify: btn_modify,
        btn_action_remove: btn_remove,
        table: table,
        url: {
            table: '/admin/tables/droits',
            new: '/admin/tables/droits/combiniveauxelements/new',
            update: '/admin/tables/droits/combiniveauxelements/update/%d',
            remove: '/admin/tables/droits/combiniveauxelements/remove/%d'
        }
    };

    btn_add_gestion_droit.click(() => {
        if (!select_gestion_droit.val()) {
            notifyNebula('Vous devez sélectionner une table dans la liste!', null, 'info')
        }
        else if (select_gestion_droit.val() === 'qualifcontrat') {
            initModalAddQualifContrat(MODAL_ADMIN_TABLES_QUALIF_CONTRAT);
        }
    });

    select_gestion_droit.change(function() {
        if ($(this).val() === 'qualifcontrat') {
            btn_add_gestion_droit.show();
            createTableQualifContrat(OBJECT_TABLE_QUALIF_CONTRAT)
        }
        else if ($(this).val() === 'combiniveauxelements') {
            btn_add_gestion_droit.hide();
            createDataTableCombiNiveauxElement(OBJECT_TABLE_COMBI_NIVEAUX_ELEMENTS);
        }

    })
}



