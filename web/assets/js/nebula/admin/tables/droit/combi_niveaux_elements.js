function createDataTableCombiNiveauxElement(target) {

    // Destroy existing instance
    destroyInstanceDataTables(target.table);

    target.instance_dataTable = target.table.DataTable({
        ajax: {
            url: target.url.table,
            type: 'POST',
            serverSide: true,
            data: { liAdminTable: target.select.val() },
            dataSrc: data => dataTablesFormatDateFromAjax(data, false)
        },
        columns: target.columns
    });

    updateTableCombiNiveauxElement(target);
    removeTableCombiNiveauxElement(target);
}

function removeRowCombiNiveauxElement(target) {
    let idCombiNiveauxElements = localStorage.getItem('idCombiNiveauxElements');

        if (idCombiNiveauxElements) {

            $.post(sprintf(target.url.remove, idCombiNiveauxElements))
                .done(data => {

                    if (typeof data !== "number") {

                        createDataTableCombiNiveauxElement(target);
                        notifyNebula('La Combi Niveau Element a bien été supprimée', 'Combi Niveaux Element', 'success');
                    }

                })
                .fail(err => console.error(err))

        }

}

function removeTableCombiNiveauxElement(target) {

    // BUTTON REMOVE ICON
    target.btn_action_remove.unbind();
    target.table.on('click', '.btn-supprimer', function() {
        let confirm_msg = '<h5 class="text-center">Êtes-vous vraiment sûr de vouloir supprimer cette Combi niveaux elements ?</h5><p class="text-center">La ligne sera définitivement supprimée.</p>',
            tbody_tr = $(this).parents('tr').hasClass('child') ? $(this).parents('tbody').find('tr.parent') : $(this).parents('tr'),
            data = target.instance_dataTable.row(tbody_tr).data(),
            confirm_yes = $('#confirm_yes');

        if(data.idLigneCombiNiveauxElements) {

            // Save id in the local storage
            localStorage.setItem('idCombiNiveauxElements', data.idLigneCombiNiveauxElements);

            MODAL_CONFIRM.find('div.modal-body').html(confirm_msg);
            MODAL_CONFIRM.modal('show');
            MODAL_CONFIRM.prop('action', 'remove-combiniveauxelements');

            // CONFIRMATION
            confirm_yes.unbind();
            confirm_yes.click(function () {
                if (MODAL_CONFIRM.prop('action') === 'remove-combiniveauxelements') {
                    removeRowCombiNiveauxElement(target);
                }
            });
        }

        else {
            notifyNebula('Impossible de supprimer cette ligne car la combi n\'existe pas', null, 'warning');
        }

    });

}

function updateTableCombiNiveauxElement(target){

    target.table.unbind();
    target.table.on('click', '.btn-modifier', function() {
        let tbody_tr = $(this).parents('tr').hasClass('child') ? $(this).parents('tbody').find('tr.parent') : $(this).parents('tr'),
            data = target.instance_dataTable.row(tbody_tr).data(),
            idNiveau = $('#idNiveau'),
            idElement = $('#idElement'),
            idElementParent = $('#idElementParent');

        idElement.empty();
        idNiveau.empty();
        idElementParent.empty();
        idNiveau.select2({
            data: [{
                id: data.idNiveau,
                text: data.liNiveau,
                selected: true

            }],
            placeholder: '',
            minimumResultsForSearch: 3
        });
        idElement.select2({
            data: [{
                id: data.idElement,
                text: data.liElement,
                selected: true
            }],
            placeholder: '',
            minimumResultsForSearch: 3
        });

        chargeFormLabel();

        if (!data.idLigneCombiNiveauxElements) idElementParent.append('<option value="" hidden></option>');

        new ListOfParam({
            target: idElementParent,
            targetName: 'idElementParent',
            url: '/admin/tables/droits/niveauxligparent',
            filter: {
                idNiveau: data.idNiveau,
                idElement: data.idElement
            },
            selectedValue: (data.idLigneCombiNiveauxElements) ? data.idElementParent : null
        });

        //MODIF D'UNE LIGNE
        if (data.idLigneCombiNiveauxElements) {
            updateCombiNiveauxElement(target, data.idLigneCombiNiveauxElements);
        }
        //AJOUT D'UNE NEW LIGNE
        else {
            newCombiNiveauxElement(target);
        }

    });

}

function updateCombiNiveauxElement(target, idCombi) {
    let modal_admin = $('#modalAdminCombiNiveauxElements'),
        form_tables = document.forms['combiniveauxelements'],
        btn_modify = $('#modify_admin_combiniveauxelements');

    modal_admin.modal('show');

    // Save form
    btn_modify.unbind();
    btn_modify.click(function() {

        if (form_tables.checkValidity()) {

            let obj_form = traitCommonElements(form_tables.elements);

            if (obj_form) {

                $.post(
                    sprintf(target.url.update,idCombi),
                    obj_form
                )
                    .done(data => {
                        if (typeof data !== "number") {

                            if (data.hasOwnProperty('bdd_msg') && data.bdd_msg) {
                                notifyNebula(data.bdd_msg, 'Server error 500', 'error')

                            } else {
                                createDataTableCombiNiveauxElement(target);
                                notifyNebula('Modification effectuée avec succès!', null, 'success')
                            }
                        }
                    })
                    .fail(err => console.error(err) )
            }

        }
    });

}

function newCombiNiveauxElement(target) {
    let modal_admin = $('#modalAdminCombiNiveauxElements'),
        form_tables = document.forms['combiniveauxelements'],
        btn_send = $('#modify_admin_combiniveauxelements');

    modal_admin.modal('show');

    // Save form
    btn_send.unbind();
    btn_send.click(function() {

        if (form_tables.checkValidity()) {

            let obj_form = traitCommonElements(form_tables.elements);

            if (obj_form) {

                $.post(
                    target.url.new,
                    obj_form
                )
                    .done(data => {
                        if (typeof data !== "number") {

                            if (data.hasOwnProperty('bdd_msg') && data.bdd_msg) {
                                notifyNebula(data.bdd_msg, 'Server error 500', 'error')

                            } else {
                                createDataTableCombiNiveauxElement(target);
                                notifyNebula('Enregistrement effectué avec succès!', null, 'success')
                            }
                        }
                    })
                    .fail(err => console.error(err) )
            }

        }
    });
}