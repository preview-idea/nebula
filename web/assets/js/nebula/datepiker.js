$(function() {
    datePikerNebula();
});

function datePikerNebula () {

    $('form :text').each(function() {
        var elem = $(this);

        if (elem.prop('name').match(/^dt/)) {

            elem.on("focus", function() {
                const _timePicker = elem.data("timepicker");

                $(this).daterangepicker({
                    timePicker: _timePicker,
                    timePicker24Hour: _timePicker,
                    startDate: moment(),
                    minDate: MIN_DATE_RANGE,
                    maxDate: MAX_DATE_RANGE,
                    timeZone: 'Europe/Paris',
                    ranges: {},
                    expanded: true,
                    single: true,
                    locale: {
                        format: _timePicker ? "DD/MM/YYYY HH:mm:ss" : "DD/MM/YYYY",
                        separator: " - ",
                        applyLabel: "Valider",
                        applyButtonTitle: "Valider",
                        cancelLabel: "Annuler",
                        cancelButtonTitle: "Annuler",
                        fromLabel: "De",
                        toLabel: "à",
                        customRangeLabel: "Custom",
                        daysOfWeek: [
                            "Dim","Lun","Mar","Mer","Jeu","Ven","Sam"
                        ],
                        monthNames: [
                            "Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"
                        ],
                        firstDay: 1
                    },
                    drops: 'auto',
                    autoUpdateInput: false,
                    singleDatePicker: true,
                    showDropdowns: true,
                    singleClasses: "picker_4"

                }, function(start) {
                    elem.val(start.format(_timePicker ? "DD/MM/YYYY HH:mm:ss" : "DD/MM/YYYY")).trigger('change');
                    chargeFormLabel();

                });
            });

            // Force format 'DD/MM/YYYY'
            elem.on('input', function() {
                let vdt = $(this).val();
                const _timePicker = elem.data("timepicker");

                if (vdt.match(/^\d{8}$/)) {
                    $(this).val(vdt.replace(
                        _timePicker ? /(\d{2})(\d{2})(\d{4}) (\d{2})(\d{2})(\d{2})/ : /(\d{2})(\d{2})(\d{4})/,
                        _timePicker ? '$1/$2/$3 $4:$5:$6' : '$1/$2/$3'))
                }

            });
        }

    })

}
