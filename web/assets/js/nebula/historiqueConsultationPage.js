$(function() {

    $.post('/consultation', {
        idUtilisateur: AppUserMosaic.idUser,
        dtConsultation: moment().format("DD/MM/YYYY H:mm:ss"),
        liUrl: location.pathname
    })

});