
/**
 *
 * @param msg
 * @param title
 * @param type
 */
function notifyNebula(msg, title, type) {
    let status = type || 'success',
        title_msg = title || $('.block-nebula-title').text();

    return new PNotify({
        title: title_msg,
        text: msg,
        type: status,
        styling: 'bootstrap3'
    });

}

/**
 *
 * @param msg
 * @returns {*}
 */
function confirmNebula(msg) {

    return (new PNotify({
        title: 'Confirmation',
        text: msg,
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    })).get().on('pnotify.confirm', function() {
        return true;
    }).on('pnotify.cancel', function() {
        return false;
    });

}