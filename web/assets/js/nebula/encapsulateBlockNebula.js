class NebulaEncapsulateBlock {

    constructor() {
        this.forms = document.querySelectorAll('div.x_panel');
        this.url = window.location.pathname;

        this.hideAllBlocks();

    }

    hideAllBlocks() {

        if (this.url.match(/new$/)) {

            this.forms.forEach((bloc, index) => {
                if (index !== 0) {
                    bloc.classList.add('hide');
                }

            });

            this.verifyValidateBlock();

        }

    }

    verifyValidateBlock() {

        this.forms.forEach((bloc, index) => {

            if (bloc.offsetWidth > 0 && bloc.offsetHeight > 0) {
                let $form = bloc.querySelector('form'),
                    $elements = $($form).find('input:visible, select:visible');

                $elements.on('input change', () => {
                    if ($form.checkValidity()) {
                        let next_bloc = $(bloc).next(':hidden');

                        $(bloc).addClass('animated lightSpeedOut');
                        next_bloc.addClass('animated lightSpeedIn')

                    }

                }).bind(this)


            }
        })

    }

}

