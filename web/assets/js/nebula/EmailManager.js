class EmailManager {

    constructor(model) {

        if (typeof model !== 'string') { throw new Error('You must passe a string in the model option'); }

        this._form_mail = document.querySelector('form[name="send_email"]');
        this._btn_prevent = document.querySelector('input[name="isPreventEmailUser"]');
        this._select_user = document.querySelector('select[name="idDestinataire"]');

        this._state = {
            model: model
        }

        this.verifyPreventEmailUser();
    }

    getState() {
        return this._state
    }

    verifyPreventEmailUser() {
        let $self = this;

        $(this._btn_prevent).unbind();
        $(this._btn_prevent).on('ifChanged', function() {
            let content_email = $('div#form_send_email');

            if ($(this).is(':checked')) {
                $self.getEmailListUtilisateur();
                content_email.removeClass('hide');

            } else {
                $(this._select_user).val('');
                content_email.addClass('hide');
            }

        })
    }

    async getEmailListUtilisateur() {
        if (this._select_user.options.length > 1) { return; }

        const _data = await $.post('/admin/utilisateurs');

        if (typeof _data !== "number") {

            // Select utilisateurs
            $(this._select_user).select2({
                data: _data.map(d => {
                    return {
                        id: sprintf('%s@seris-security.com', d.liLoginLdap),
                        text: d.liLoginLdap
                    }
                }),
                minimumResultsForSearch: 12,
                language: 'fr'

            }).change(function() {
                chargeFormLabel();
            });
        }
    }

    setContext({ objModel, template, message, toCopy = [], annexe }) {
        this._state = Object.assign(this.getState(), {
            objModel: JSON.stringify(objModel),
            template: template,
            message: message,
            recipient: $(this._select_user).val(),
            toCopy: toCopy,
            annexe: annexe
        })
    }

    sendEmail() {
        if (this._state.objModel && this._state.model && this._state.template && this._state.message && this._state.recipient) {
            return $.post('/sendEmail', this.getState());

        } else {
            console.log(this.getState());
        }
    }

}
