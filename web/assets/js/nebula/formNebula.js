/**
 * @class FormNebula
 */
class FormNebula {

    constructor(opt) {
        this.url = opt.url || URL_CURRENT;
        this.except = opt.except || [];
        this.objs = opt.objLinked || [];
        this.send =  $('#send_form');
        this.reset =  $('#reset_form');
        this.valid = $('#valid_form');
        this.prev = $('#prev_form');
        this.update = $('#update_form');
        this.isOk = !!(URL_CURRENT.match(/(validate|detail)/));

        this.addNew();
        this.resetForm();
        this.checkForm();

        // this.onValidation();

    }

    addNew() {

        this.send.click(function() {
            if (this.isOk === true) { this.actionForm(true); }
        }.bind(this));

    }

    onValidation() {

        this.valid.click(function() {
            if (this.isOk === true) { this.actionForm(false); }
        }.bind(this));

    }

    actionForm(check) {
        let isValid = $('input[name="isValid"]');

        // Set TRUE if contrats/avenants is validated
        isValid.prop('checked', check);

        if (this.url.match(/(contrats|avenants)\/validate/)) {
            let id = parseInt($('input[name="idAvenant"]').val() || $('input[name="codeContrat"]').val()),
                redirect = '/salaries';

            return this.updateForm(id, redirect);
        }

        this.sendForm();

    }

    createForm() {
        this.form = $('form');
        this.form_obj = {};

        if (this.objs.length > 0) {
            let regexNon = new RegExp(`^(${this.objs.join('|')})`, 'g');

            // Treats all elements that correspond to the main form
            for(let x=0; x < this.form.length; x++) {

                if (!this.form[x].name.match(regexNon) && this.except.indexOf(this.form[x].name) === -1)  {
                    this.mergeObj(this.traitElement(this.form[x].elements));
                }

            }// for

            // Treats all elements corresponding to the objLinked
            for (let i=0; i < this.objs.length; i++) {
                let regex = new RegExp(`^${this.objs[i]}`);

                if (Array.isArray(this.getObjLinked(this.form, regex))) {
                    this.form_obj[this.objs[i]] = this.getObjLinked(this.form, regex);
                }

            }// for

        } else {

            for(let j=0; j < this.form.length; j++) {
                if (this.except.indexOf(this.form[j].name) === -1) {
                    this.mergeObj(this.traitElement(this.form[j].elements));
                }

            } // for
        }

        console.log(JSON.stringify(this.form_obj));
        return this.form_obj;
    }

    mergeObj(o) {
        for (let prop in o) {
            if (o.hasOwnProperty(prop)) {
                this.form_obj[prop] = o[prop];
            }
        }
    }

    traitElement(e) {
        let r = {};

        for (let i=0; i < e.length; i++) {

            if (e[i].value || (e[i].type === 'select-one' && e[i].options[e[i].selectedIndex].value)) {
                let masks = ['idNirDef', 'liIban'];

                if (masks.indexOf(e[i].name) !== -1 || e[i].type === 'tel') {
                    r[e[i].name] = e[i].value.replace(/\s|_/g, '');

                } else if(e[i].type === 'checkbox') {
                    r[e[i].name] = (e[i].checked) ? 1 : 0;

                } else if(e[i].type === 'radio') {
                    if (e[i].checked) { r[e[i].name] = parseInt(e[i].value) }

                } else if (e[i].type === 'number' && e[i].hasAttribute('step')) {
                    r[e[i].name] = parseFloat(e[i].value);

                } else if (e[i].name.match(/^id/)) {
                    r[e[i].name] = e[i].type === 'select-one'
                        ? e[i].options[e[i].selectedIndex].value
                        : e[i].value;

                } else {
                    e[i].type === 'text'
                        ? r[e[i].name] = e[i].value.trim()
                        : r[e[i].name] = e[i].value;

                }
            }

        } // for
        return r;

    }

    checkForm() {

        $('input, select').on('change focus', function() {

            $('input:visible, select:visible').on('input change focus', function() {
                let form = $('form:visible').not('[name="block_info"]'),
                    count = 0;

                for (let i=0; i < form.length; i++) {
                    if (form[i].checkValidity()) { count++; }
                }

                let isEqalCount = count === form.length;

                this.send.prop('disabled', !isEqalCount);
                this.update.prop('disabled', !isEqalCount);

                // Button preview contrat
                this.prev.prop('disabled', !isEqalCount);
                this.prev.attr('data-target', '#prevFormContrat');

                this.isOk = isEqalCount;

            }.bind(this))

        }.bind(this));

    }

    async sendForm() {
        const _resp = await $.post(this.url, this.createForm());

        if (_resp === 500) {
            let _msg = 'Désolé, nous avons un souci. Contactez le support';

            if (this.url.match(/avenants\/new$/)) { _msg = 'Il est interdit de créer 2 avenants le même jour pour le même contrat' }
            if (this.url.match(/contrats\/new$/)) { _msg = 'Il est interdit de créer 2 contrats le même jour pour le même salarié' }

            return notifyNebula(_msg, 'Server error 500', 'error');
        }

        if (typeof _resp !== "number") {

            localStorage.removeItem('enfatns');
            notifyNebula('Enregistrement effectué avec succès', null, 'success');

            // REDIRECTION AFTER CREATION
            this.redirection(_resp);

        } else {
            notifyNebula('Désolé, nous avons un souci. Contactez le support', `Server error ${_resp}`, 'error');
        }

    }

    async updateForm(id, redirect) {

        if (id) {
            const _resp = await $.post(this.url.replace(/(detail|validate)/, 'update/') + id, this.createForm());

            if (typeof _resp !== "number") {

                localStorage.removeItem('enfatns');
                notifyNebula('Modification effectuée avec succès', null, 'success');

                // REDIRECTION AFTER UPDATE
                this.redirection(_resp);

                setTimeout(() => { redirect ? location.assign(redirect) : location.reload() }, 2000);

            } else {
                notifyNebula('Désolé, nous avons un souci. Contactez le support', 'Server error 500', 'error');
            }

        }
    }

    resetForm() {

        this.reset.click(function() {
            localStorage.removeItem('enfatns');
            location.reload();
        })

    }

    // Trait elements only for objLinkeds
    getObjLinked(form, reg) {
        this.arrObj = [];

        for (let x=0; x < this.form.length; x++) {
            let el = form[x].elements;

            if (form[x].name.match(reg)) {
                if (!isEmpty(this.traitElement(el))) {
                    this.arrObj.push(this.traitElement(el));
                }
            }
        }
        return this.arrObj;

    }

    redirection(_resp) { }

}
