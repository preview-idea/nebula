class FileUploadNebula {

    constructor() {
        this.form_file = new FormData();
    }

    validateBeforeUploadFile({ url, file, extensions = [] }) {
        const $self = this;
        const regExtension = new RegExp(`${extensions.join('|')}$`);

        if (file) {

            if (file.name.toLowerCase().match(regExtension)) {
                return $self.ajaxFileUpload({ url, file })

            } else {
                $(this).val('')
                notifyNebula(
                    sprintf('Seulement les formats : %s sont accéptés!', extensions.join(', ')),
                    'FileUploader',
                    'info'
                );
            }

        }

    }

    ajaxFileUpload({ url, file }) {

        if (file && url) {
            this.form_file.append('uploadFileImage', file, file.name);

            return $.ajax
            ({
                url,
                type: 'POST',
                data: this.form_file,
                cache: false,
                contentType: false,
                processData: false,
            })
        }

    }

}
