//=== REMOVE ACCENT AND SPACE ====
function removeSpecialCharacters(str, bool) {
    let rem = str.normalize('NFD').replace(/[\u0300-\u036f]|[^\w\s\-]/g, ""),
        boolean = bool || false;

    if (boolean) { return rem.replace(/(\s|s\s)/g,'_').toLowerCase(); }

    return rem.toLowerCase();

}

function removeAccent(string) {

    const pattern = {
        accents: 'ÀÁÂÃÄÅĄàáâãäåąßÒÓÔÕÕÖØÓòóôõöøóÈÉÊËĘèéêëęðÇĆçćÐÌÍÎÏìíîïÙÚÛÜùúûüÑŃñńŠŚšśŸÿýŽŻŹžżź',
        out: 'AAAAAAAaaaaaaaBOOOOOOOOoooooooEEEEEeeeeeeCCccDIIIIiiiiUUUUuuuuNNnnSSssYyyZZZzzz'
    };

    return string.split('').map( letter =>  {
        let i = pattern.accents.indexOf(letter);
        return i !== -1 ? pattern.out[i] : letter

    }).join('')
}

