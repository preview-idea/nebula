loaderFromAjax();

function loaderFromAjax() {
    let url_execptions = new RegExp('(batches\/detail|nuxeo\/documents)');
    
    OJBNebulaPreloaded = {
        _container: $('body'),
        ajaxSend: 0,
        ajaxComplete : 0,
        reloaded: false,
        customLoad: function() {},
        init: function() {
            if (!this._container.find('div.preload').length) {
                this._container.append('' +
                    '<div class="preload">' +
                    '   <div class="loader-message">' +
                    '       <h2 class="animated fadeInUp">En cours de traitement ' +
                    '           <span></span>' +
                    '           <span></span>' +
                    '           <span></span>' +
                    '       </h2>' +
                    '   </div>' +
                    '   <div class="loader"></div>' +
                    '</div>'
                );
            }
        },
        destroy: function() {
            this._container.find('div.preload').remove()
        }
    }

    // COUNT AJAX STARTED
    $(document).ajaxSend(function(event, req, opt) {

        if (!opt.url.match(url_execptions)) {
            OJBNebulaPreloaded.ajaxSend++;
            OJBNebulaPreloaded.init();
        }

    // COUNT AJAX COMPLETED
    }).ajaxComplete(function(event, req, opt) {

        if (!opt.url.match(url_execptions)) {
            OJBNebulaPreloaded.ajaxComplete++;

            if (OJBNebulaPreloaded.ajaxComplete === OJBNebulaPreloaded.ajaxSend) {
                OJBNebulaPreloaded.destroy();

                if (!OJBNebulaPreloaded.reloaded) {
                    checkNewContratFromDuplicate();
                    disableInputsAcompte();
                    OJBNebulaPreloaded.customLoad();
                    OJBNebulaPreloaded.reloaded = true;
                }

            }
        }

    });

}



/**
 * Reload functions to calc (période d'essai, salaire de base) in the page duplicate contrat.
 */
function checkNewContratFromDuplicate() {

    if (URL_CURRENT.match(/contrats\/new$/)) {
        const CDD = 1, CDI = 2, APPRENT = 4;

        let dtDebutcontrat = $('input[name="dtDebutcontrat"]'),
            dtFinContrat = $('input[name="dtFincontratPrevue"]'),
            typeContrat = $('select[name="idTypecontrat"]'),
            coeffContrat = $('select[name="idCoeffcontrat"]'),
            typeCategorie = $('select[name="idCategorieemploye"]');

        if (dtDebutcontrat.val().isFormatDateView()) {
            let dt_current = moment().year(),
                dt_contrat = moment(dtDebutcontrat.val().convertToDate()).year();

            if (typeContrat.val() && dt_current !== dt_contrat) {

                if (parseInt(typeContrat.val()) === CDI) {
                    typeCategorie.trigger('change');

                } else if (
                    [CDD, APPRENT].indexOf(parseInt(typeContrat.val())) !== -1 &&
                    dtFinContrat.val() && dtFinContrat.val().isFormatDateView() &&
                    coeffContrat.val()
                ) {
                    dtFinContrat.trigger('change');
                    coeffContrat.trigger('change');

                }

            }

        }

    }

}

function disableInputsAcompte() {
    const TYPE_RECURRENT = 1;

    if (URL_CURRENT.match(/acomptes\/detail$/)) {
        let form_elements = $('form[name="acompte"] input, select'),
            typeAcompte = parseInt($('select[name="idTypeAcompte"]').val()),
            calendrierPaieFin = $('select[name="idCalendrierPaieFin"]');

        if (calendrierPaieFin.val()) {

            typeAcompte !== TYPE_RECURRENT
                ? removeOptions(calendrierPaieFin)
                : form_elements = form_elements.not('[name="idCalendrierPaieFin"]');

        } else {
            form_elements = form_elements.not('[name="idCalendrierPaieFin"]');
        }

        form_elements.each(function() {

            switch($(this).prop('type')) {
                case 'select-one':
                    return removeOptions($(this));

                case 'checkbox':
                    return  disableCheckbox($(this));

                default:
                    return  disableInput($(this));

            }

        });

    }
}