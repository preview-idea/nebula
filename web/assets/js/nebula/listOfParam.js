$(function() {
    console.log('list_of_params');

});

/**
 *
 * @param {Object} opt
 * @constructor
 */
function ListOfParam(opt) {
    this.target = opt.target;
    this.name = opt.name;
    this.targetName = opt.targetName || opt.target.prop('name');
    this.libelleName = opt.libelleName || this.targetName.replace('id', 'li');
    this.filter = opt.filter || null;
    this.results = opt.results || 12;
    this.extra = opt.extra || null;
    this.customData = opt.customData || null;
    this.onlyExtra = opt.onlyExtra || false;
    this.showData = opt.showData || false;
    this.sortByLib = opt.sortByLib || false;
    this.url = opt.url || '/params';
    this.selectedValue = opt.selectedValue || null;
    this.ClientDWH = opt.ClientDWH || false;

    this.getListToSelect2();

}

ListOfParam.prototype = {

     getListToSelect2: async function() {

        if (this.target) {
            const _data = !this.ClientDWH
                ? await $.getJSON(this.url, { filter: this.filter, param: this.name })
                : await this.ClientDWH.request({ url: this.url, criteria: { filter: this.filter, param: this.name }})

            if (typeof _data !== "number") {
                const $list = $.map(_data, function(d) {
                    if (d[this.libelleName]) {
                        return {
                            id: d[this.targetName],
                            text: d[this.libelleName],
                            selected: _data.length === 1,
                            custom_data: this.getCustomData(d)

                        }
                    }

                }.bind(this));

                if (!this.onlyExtra) {
                    this.target.select2({
                        data: $list,
                        placeholder: '',
                        allowClear: !this.target.prop('required') && this.target.prop('type') === 'select-one',
                        minimumResultsForSearch: this.results,
                        language: 'fr'
                    });
                }

                if (this.extra) { this.createExtraList($list); }

                if (this.sortByLib) {
                    this.target.select2({
                        sorter: data => data.sort((a,b) => (a.text.toLowerCase() > b.text.toLowerCase())
                            ? 1 : ((b.text.toLowerCase() > a.text.toLowerCase()) ? -1 : 0))

                    })
                }

                if (this.selectedValue) {
                    this.target.val(this.selectedValue);
                    this.target.trigger('change');
                }

                if (_data.length === 1) { this.target.trigger('change') }
                if (this.showData) { console.log(_data) }

                chargeFormLabel();
                this.customSettings();
            }

        }

    },

    getCustomData: function(d) {

        if (this.customData !== null) {
            let test = {};

            for (let prop in d) {
                if (d.hasOwnProperty(prop)) {
                    if (this.customData.indexOf(prop) !== -1) {
                        test[prop] = d[prop];
                    }
                }
            }
            return test;
        }

    },

    customSettings: function() {},

    createExtraList: function(list) {

        this.extra.forEach(function(ex) {
            ex.select2({
                data: list,
                placeholder: '',
                allowClear: !ex.prop('required') && ex.prop('type') === 'select-one',
                minimumResultsForSearch: this.results,
                language: 'fr'
            })

        }.bind(this))

    }


};