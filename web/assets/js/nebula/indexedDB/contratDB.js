// Declare global variable STORE_CONTRAT
const STORE_CONTRAT = new InstanceMosaicDB('Contrat');

// Open cursor to connection on idb
STORE_CONTRAT.init().then(function() {

    // Get all items for contrat
    init_store_contrat();
    STORE_CONTRAT.getAllItems();

});

function init_store_contrat() {

    console.log('init_store_contrat');

    // GET ALL ITEMS
    STORE_CONTRAT.getAllItems = function() {
        let
            // Get everything in the store;
            keyRange = this.getKeyRange().lowerBound(0),
            cursorRequest = this.getStore().openCursor(keyRange),
            btn_prev = document.querySelector('#prev_form'),
            idContrat = document.querySelector('[name="codeContrat"]'),
            content = document.querySelector('#list_contrat_saved');

        cursorRequest.onsuccess = function(e) {
            let result = e.target.result;

            if (!!result === false)  {
                if ($(content).find('span').length === 0) {
                    $(content).append('<li class="not-source"><a href="javascript:void(0)"> Aucun enregistrement .</a></li>');
                }
                return ;
            }

            STORE_CONTRAT.renderItem(result.value);
            result.continue();
        };

        cursorRequest.onerror = function(e) {
            console.error(cursorRequest.error)
        };

        // REMOVE DATA SAVED IF BUTTON PREVIEW IS CLICKED
        if (btn_prev) {
            btn_prev.addEventListener('click', function() {
                if (idContrat.value.match(/^[A-Z]/)) {
                    STORE_CONTRAT.deleteItem(idContrat.value);
                }
            })
        }

    };

    // ADD ITEM
    STORE_CONTRAT.addItem = function(item, name) {

        if (item.hasOwnProperty(name)) {
            let request = this.getStore().add(item);

            request.onsuccess = function() {
                notifyNebula('Sauvegarde effectuée avec succès!', null, 'success');
            };

            request.onerror = function(e) {
                notifyNebula('Erreur lors de l\'enregistrement!', null, 'error');
                console.error(request.error)
            };
        }

    };

    // GET ONE IEM
    STORE_CONTRAT.getOneItem = function(id) {

        if (id) {
            let request = this.getStore().get(id);

            request.onsuccess = function(e) {
                let data_saved = e.target.result;

                if (data_saved) {
                    STORE_CONTRAT.postRedirect({
                        data: data_saved,
                        url: '/salaries/contrats/new'
                    });
                }

            };

            request.onerror = function(e) {
                console.error("Error to remove this item:", e)
            };
        }
    };

    // DELETE ITEM
    STORE_CONTRAT.deleteItem = function(id) {
        let content = document.querySelector('#list_contrat_saved');

        if (id) {
            let request = this.getStore().delete(id);

            request.onsuccess = function() {
                $(content).children().remove();
                STORE_CONTRAT.getAllItems();
                console.log('Item has been removed successfully!');
            };

            request.onerror = function(e) {
                console.error("Error to remove this item:", e)
            };
        }
    };

    // RENDER ITEM
    STORE_CONTRAT.renderItem = function(item) {
        let content = document.querySelector('#list_contrat_saved'),
            codeContrat = $('input[name="codeContrat"]').val();

        if (codeContrat !== item.idContrat) {
            const li = document.createElement('li'),
                span = document.createElement('span'),
                a = document.createElement('a'),
                span_text = document.createTextNode(sprintf(' Contrat %s saved - %d', item.idContrat, item.idMatricule));

            li.setAttribute('data-saved', item.idContrat);
            a.setAttribute('href', 'javascript:void(0)');
            span.setAttribute('class', 'fa fa-file');

            span.appendChild(span_text);
            a.appendChild(span);
            li.appendChild(a);

            if (content) { content.appendChild(li); }

            a.addEventListener("click", function () {
                STORE_CONTRAT.getOneItem(item.idContrat);
            }, false);

        } else {
            if ($(content).find('span').length === 1 ) {
                $(content).append('<li class="not-source"><a href="javascript:void(0)"> Aucun enregistrement .</a></li>');
            }
        }
    };

    /**
     * POST REDIRECT
     * @param opt object
     */
    STORE_CONTRAT.postRedirect = function(opt) {
        let $value = JSON.stringify(opt.data),
            postFrom = '<form method="POST" action=" '+ opt.url + '" name="data_saved">\n'
                + '<input type="hidden" name="dataSaved">\n'
                + '<input type="hidden" name="idMatricule" value="' + opt.data.idMatricule + '">\n'
                + '</form>';

        $('body').append(postFrom);

        $('form[name="data_saved"] input[name="dataSaved"]').val($value);
        $('form[name="data_saved"]').submit();

    };
}