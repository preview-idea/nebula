idb_mosaic_connect = { state: false };

class MosaicDB {

    constructor() {

        // Verify if browser support indexedDB.
        if (!('indexedDB' in window)) {
            throw new Error("This browser doesn't support IndexedDB");
        }

        this.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB ||  window.msIndexedDB;
        this.manager = null;

    }

    openConnection() {
        const DB_NAME = 'Mosaic', DB_VERSION = 1;

        // Delete idb nebula if it exists
        this.indexedDB.deleteDatabase('nebula');

        // Create objects store if not exists
        let request = this.indexedDB.open(DB_NAME, DB_VERSION),
            $this = this;

        request.onupgradeneeded = function(e) {
            console.log('Creating the stores...');

            $this.manager = e.target.result;

            // Create object store for contrat
            if (!this.manager.objectStoreNames.contains('Contrat')) {
                let store = this.manager.createObjectStore('Contrat', { keyPath: 'idContrat' });
                store.createIndex('idContrat', 'idContrat', { unique: true });

            }

            // Create object store for batch
            if (!this.manager.objectStoreNames.contains('Batch')) {
                let store = this.manager.createObjectStore('Batch', { keyPath: 'idTraitement' });
                store.createIndex('idTraitement', 'idTraitement', { unique: true });

            }

        }.bind(this);

        return new Promise((res, rej) => {

            request.onerror = function() {
                rej(request.error)
            };

            request.onsuccess = function(e) {
                $this.manager = e.target.result;

                if (!idb_mosaic_connect.state) {
                    console.log('You are connected in MosaicDB from indexedDB');
                    idb_mosaic_connect.state = true;
                }

                res($this);

            }.bind(this);

        });

    }

    getManagerDB() { return this.manager }

}

class InstanceMosaicDB  extends MosaicDB {

    constructor(storeName) {
        super();
        this.storeName = storeName;
        this.keyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
    }

    async init() {
        await this.openConnection();
    }

    getStore() {

        if (!this.getManagerDB().objectStoreNames.contains(this.storeName)) {
            throw new Error('Store not exists in MosaicDB');

        } else {
            let trans = this.getManagerDB().transaction(this.storeName, "readwrite");
            return trans.objectStore(this.storeName);
        }

    }

    getKeyRange() {
        return this.keyRange;
    }

}
