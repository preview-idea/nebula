// Declare global variable STORE_BATCH
const STORE_BATCH = new InstanceMosaicDB('Batch');

// Open cursor to connection on idb
STORE_BATCH.init().then(function() {

    // Get all items for batch
    init_store_batch();
    STORE_BATCH.getAllItems();

});

function init_store_batch() {

    console.log('init_store_batch');

    // GET ALL ITEMS
    STORE_BATCH.getAllItems =  function() {
        let // Get everything in the store;
            keyRange = this.getKeyRange().lowerBound(0),
            cursorRequest = this.getStore().openCursor(keyRange);

        cursorRequest.onsuccess = function(e) {
            let result = e.target.result;

            if (!!result === false) { return; }

            STORE_BATCH.getOneItem(result.value);
            result.continue();
        };

        cursorRequest.onerror = function(e) {
            console.error(cursorRequest.error)
        };

    };

    // ADD ITEM
    STORE_BATCH.addItem = function(item, name) {

        if (item.hasOwnProperty(name)) {
            let request = this.getStore().add(item);
            request.onsuccess = function() {
                notifyNebula('Votre demande est bien prise en compte, vous serez averti dès la fin du traitement.', null, 'success');
            };

            request.onerror = function(e) {
                notifyNebula('Erreur lors de l\'enregistrement de votre demande!', null, 'error');
                console.error(request.error)
            };
        }

    };

    // GET ONE ITEM
    STORE_BATCH.getOneItem = function(item) {

        if (item) {
            let date_demande = moment(item.dtDemande).add(item.tempsEstime, 'seconds'),
                diff_temps_estime = date_demande.diff(moment(), 'milliseconds'),
                result_batch = JSON.parse(localStorage.getItem('result_batch')) || [];

            // In case of Génération contrat (idTypeTraitement=1): Added in the local storage to check idContrat from the batch
            if (item.idTypeTraitement === 1) {
                if (result_batch.indexOf(item.idContrat) === -1) result_batch.push(item.idContrat);
                localStorage.setItem('result_batch', JSON.stringify(result_batch));
            }

            if (diff_temps_estime < 0) {
                verifyIfTraitementIsDone(item);

            } else {
                setTimeout(function() {
                    verifyIfTraitementIsDone(item);
                }, diff_temps_estime);
            }
        }

    };

    // DELETE ITEM
    STORE_BATCH.deleteItem = function(id) {

        if (id) {
            let request = this.getStore().delete(id);

            request.onsuccess = function() {
                STORE_BATCH.getAllItems();
                console.log('Item has been removed successfully!');
            };

            request.onerror = function(e) {
                console.error("Error to remove this item:", e)
            };
        }
    };

    // RENDER ITEM
    STORE_BATCH.renderItem = function(item) {
        const TERMINER_SANS_ERREUR = 3;
        const DEMANDE_ANNULE = 5;

        let time_remove_message = moment().diff(moment(item.dtDemande), 'minutes'),
            time_past = item.dtFinTraitement
                ? moment(item.dtFinTraitement).fromNow()
                : moment(item.dtDemande).fromNow();

        // If time_remove_message > 360 (6 hours) it will be deleted
        if (time_remove_message > 360) {

            // In case of Génération contrat (idTypeTraitement=1): Remove contrat from localStorage
            if (item.idTypeTraitement === 1) {
                let result_batch = JSON.parse(localStorage.getItem('result_batch'));
                localStorage.setItem('result_batch', JSON.stringify(result_batch.filter(id => id !== item.idContrat)));
            }

            return STORE_BATCH.deleteItem(item.idTraitement);

        } else {

            if (item.dtFinTraitement) {
                let msg_ok = 'Votre demande est terminée, vous pouvez la consulter dès maintenant.',
                    msg_ko = 'Nous n\'avons pas pu traiter votre demande, vous pouvez donc contacter le support!',
                    msg_annuler = sprintf('Votre demande a été annulée/refusée: %s', item.liMessage);

                sendMessageEnvelope(item, time_past,
                    (item.idEtatTraitement === TERMINER_SANS_ERREUR
                        ? msg_ok
                        : (item.idEtatTraitement === DEMANDE_ANNULE
                            ? msg_annuler
                            : msg_ko
                        )
                    )
                );

            } else {
                setTimeout(function() {
                    verifyIfTraitementIsDone(item);
                }, 60000);
            }

        }

    };

    /**
     * Create a element dom to show the message to user
     * @param content
     * @param id
     * @param url
     * @param time
     * @param type
     * @param message
     */
    function createElementDOM(content, id, url, time, type, message) {
        const li_message = document.createElement('li'),
            icon_message = document.createElement('i'),
            span_title = document.createElement('span'),
            span_time = document.createElement('span'),
            span_message = document.createElement('span'),
            link_message = document.createElement('a'),
            title_text = document.createTextNode(sprintf(' %s', type)),
            message_text = document.createTextNode(message),
            time_text = document.createTextNode(time);

        // SETTERS ATTRIBUTES
        li_message.setAttribute('id', sprintf('item_%d',id));
        link_message.setAttribute('href', url);
        icon_message.setAttribute('class', 'fa fa-modx');
        span_time.setAttribute('class', 'time');
        span_message.setAttribute('class', 'message');

        span_title.appendChild(title_text);
        span_message.appendChild(message_text);
        span_time.appendChild(time_text);

        // APPEND DOM
        li_message.appendChild(link_message);
        link_message.appendChild(icon_message);
        link_message.appendChild(span_title);
        link_message.appendChild(span_time);
        link_message.appendChild(span_message);

        // MAKE DOM IN CONTENT
        content.appendChild(li_message);

        // EVENT TO REMOVE ITEM IN INDEXDED DB
        link_message.addEventListener('click', function() {
            STORE_BATCH.deleteItem(id)
        }, false);

    }

    /**
     * Function to verify if the batch traitement is done
     * @param item
     */
    function verifyIfTraitementIsDone(item) {

        if (item.idTraitement) {
            $.getJSON('/batches/detail',{ idTraitement: item.idTraitement })
                .done(function(data) {
                    if (typeof data !== "number") {

                        if (item.idContratOrigine) { data.idContratOrigine = item.idContratOrigine }

                        STORE_BATCH.renderItem(data);
                    }
                })
                .fail(function(error) {
                    STORE_BATCH.deleteItem(item.idTraitement);
                    console.error(error)
                })
        }
    }

    function sendMessageEnvelope(item, time_past, message) {
        let content = document.querySelector('#moisac-notify-user'),
            count_message = document.querySelector('#count_message'),
            count = Number(count_message.innerHTML) + 1;

        count_message.innerHTML = count.toString();
        createElementDOM(
            content,
            item.idTraitement,
            traitURLToRedirect(item),
            time_past,
            item.liTypeTraitement,
            message
        );

        // Enable action if traitement is ok (3: terminer sans erreurs)
        if (item.idEtatTraitement === 3) {

            // In case of Génération contrat (idTypeTraitement=1): Remove contrat from localStorage
            if (item.idTypeTraitement === 1) {
                let result_batch = JSON.parse(localStorage.getItem('result_batch'));
                localStorage.setItem('result_batch', JSON.stringify(result_batch.filter(id => id !== item.idContrat)));
            }

            // Enable download button if we are on contrat/avenant detail page
            if (URL_CURRENT.match(/(contrats|avenants)\/(detail|validate)/)) {
                let idDocument = $('input[name="idAvenant"]').val() || $('input[name="codeContrat"]').val();

                if (item.idContrat === idDocument) { enableDownloadButton(idDocument) }

            }
        }

    }

    function traitURLToRedirect(item) {
        let idContratOrigine = item.idContrat > 9999999 ? item.idContratOrigine : null;

        if (item.idContrat) {
            switch (item.idTypeTraitement) {
                case 1:
                    return idContratOrigine
                        ? sprintf('/salaries/contrats/avenants/detail?idMatricule=%d&idContrat=%d&idAvenant=%d', item.idMatricule, idContratOrigine, item.idContrat)
                        : sprintf('/salaries/contrats/detail?idMatricule=%d&idContrat=%d', item.idMatricule, item.idContrat);

                case 3:
                    return sprintf('/salaries/contrats/detail?idMatricule=%d&idContrat=%d', item.idMatricule, item.idContrat);

                default:
                    return sprintf('/salaries/detail?idMatricule=%d', item.idMatricule);

            }
        }

        return sprintf('/salaries/detail?idMatricule=%d', item.idMatricule);

    }


}

