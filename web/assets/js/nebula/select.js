// LANGUAGE
(function(){if(jQuery&&jQuery.fn&&jQuery.fn.select2&&jQuery.fn.select2.amd)var e=jQuery.fn.select2.amd;return e.define("select2/i18n/fr",[],function(){return{errorLoading:function(){return"Les résultats ne peuvent pas être chargés."},inputTooLong:function(e){var t=e.input.length-e.maximum;var c=(t>1)?"s ":"";return"Supprimez "+t+" caractère"+c},inputTooShort:function(e){var t=e.minimum-e.input.length; var c=(t>1)?"s ":"";return"Saisissez au moins "+t+" caractère"+c},loadingMore:function(){return"Chargement de résultats supplémentaires…"},maximumSelected:function(e){var c=(e.maximum>1)?"s":"";return"Vous pouvez seulement sélectionner "+e.maximum+" élément"+c},noResults:function(){return"Aucun résultat trouvé"},searching:function(){return"Recherche en cours…"}}}),{define:e.define,require:e.require}})();

// DEFAULT SETTINGS SELECT2
$.fn.select2.defaults.set('width', '92%');
$.fn.select2.defaults.set('language', 'fr');

$('select').select2({
    minimumResultsForSearch: 12,
    language: 'fr'

}).change(() => chargeFormLabel() );

let _span_copy_txt = $('span.select-copy-text');

_span_copy_txt.click(e => {
    let _target = e.target,
        _parent = $(_target).parent();

    copyTextOfOption(_parent.find('select'));
    notifyNebula('La copie a bien été effectuée', 'Select copy', 'success')

});