class SwitchRemoteUser {

    constructor() {
        this._modal = document.querySelector('#switchRemoteUser');
        this._target = document.querySelector('form[name="switch_remote_user"] select[name="idUtilisateur"]');
        this._btn_switch = document.querySelector('.btn-remote-user');
        this._switch_remote = document.querySelector('#switch_remote_user');
    }

    getTarget() {
        return this._target;
    }

    getModal() {
        return this._modal;
    }

    switchRemoteUser() {
        let $this = this;

        if ($this._btn_switch && isVisible($this._btn_switch)) {

            // Open modal with list of users
            $this._btn_switch.addEventListener('click', () => {
                $($this.getModal()).modal('show');
                $this.getListUsers();
            });

            // Verify target to enable switch mode
            $($this.getTarget()).change(() => {
                $this._switch_remote.disabled = !$this.getTarget().options[$this.getTarget().selectedIndex].value;
                $this._remote_user = getOptionsText($this.getTarget());
            });

            // Send chosen user to switch
            $this._switch_remote.addEventListener('click', () => {

                if ($this._remote_user) {

                    $.post('/remote/users/switch', {
                        LoginLdap: $this._remote_user
                    })
                        .done((data) => {

                            if (data !== 'number') {
                                notifyNebula(data.message, 'Remote User', 'success');
                                location.reload();
                            }
                        })
                        .fail(err => console.error(err))
                }
            })
        }
    }

    getListUsers() {
        let _target = this._target;

        if (_target) {

            $.getJSON('/remote/users')
                .done(function(users) {

                    if (Array.isArray(users) && users.length > 0) {
                        let $list = users.map(d => {
                            return {
                                id: d.idUtilisateur,
                                text: d.liLoginLdap
                            }
                        });

                        $(_target).select2({
                            data: $list,
                            minimumResultsForSearch: 12,
                            language: 'fr'

                        });
                    }
                })

                .fail(err => console.error(err))

        }

    }
}

const REMOTE_USER = new SwitchRemoteUser();

$(function() {
    REMOTE_USER.switchRemoteUser();

});
