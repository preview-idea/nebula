class BuildFormElementsDOM {

    constructor({ name, class_div }) {
        this._name = name;
        this._elements = []
        BuildFormElementsDOM._class_div = class_div;
    }

    getName() {
        return this._name;
    }

    getElements() {
        return this._elements.slice();
    }

    setElements(elems) {
        if (typeof elems !== 'object' && !Array.isArray(elems)) { throw new Error('Type not accepted to set _elements') }

        this._elements = elems[0].idOrderElement
            ? elems.sortArrayBy('idOrderElement').map(elem => new ElementsDOM(elem))
            : elems.map(elem => new ElementsDOM(elem))
    }

    createElements() {
        let elems = this._elements,
            obj_elem = [];

        if (Array.isArray(elems)) {
            elems.forEach(elem => {
                obj_elem.push(BuildFormElementsDOM.getElementsDOM(elem));
            })
        }

        return obj_elem;
    }

    createForm() {
        let _form = document.createElement('form'),
            elems = this._elements;

        /** Form set attributes */
        _form.setAttribute('action', 'javascript:void(0)');
        _form.setAttribute('name', this._name);

        if (Array.isArray(elems)) {
            for (let i=0; i < elems.length; i++) {
                if (elems[i].type !== 'hidden') { _form.appendChild(BuildFormElementsDOM.getElementsDOM(elems[i])); }
                else { _form.insertAdjacentElement('afterend', BuildFormElementsDOM.getElementsDOM(elems[i])); }
            }
        }

        return _form;

    }

    static getElementsDOM(elem) {
        let _label = document.createElement('label'),
            _required = document.createAttribute('required'),
            _i = document.createElement('i'),
            _span = document.createElement('span'),
            _div = document.createElement('div'),
            _label_text = document.createTextNode(elem.label),
            _disabled = document.createAttribute('disabled'),
            _elem = this.getTypeDOM(elem.type);

        // Create unique name for properties label and id
        const unique_name = elem.name.concat('_', uniqidLocal());

        /** Div set attributes */
        elem.type === 'checkbox'
            ? _div.setAttribute('class', 'col-md-4 col-sm-4 col-xs-6')
            : _div.setAttribute('class', sprintf('form-group %s', this._class_div
                ? this._class_div
                : 'col-md-4 col-sm-6 col-xs-12')
            );

        /** Label set attributes */
        _label.setAttribute('for', unique_name);
        _label.appendChild(_label_text);

        /** Element set attributes */
        elem.type === 'checkbox'
            ? _elem.setAttribute('class', 'flat')
            : _elem.setAttribute('class', 'form-control');

        _elem.setAttribute('id', unique_name);
        _elem.setAttribute('name',  elem.name);

        /** Set attribute maxlength and step */
        if (elem.length) {

            elem.length.length > 1 && elem.length.match(/[\.\,]/)
                ? this.traitNumber(elem, _elem)
                : _elem.setAttribute('maxlength', elem.length);

        }

        /** Append _elements in the div */
        _div.appendChild(_elem);

        if (elem.type !== 'hidden') {
            elem.type !== 'checkbox'
                ? _div.insertBefore(_label, _div.childNodes[0])
                : _div.appendChild(_label);
        }

        /** Set attribute table ref if it is referenced */
        if (['select-one','select-multiple'].indexOf(_elem.type) !== -1  && elem.table_ref) {
            _elem.setAttribute('table-ref', elem.table_ref);
        }

        /** Set attribute table filter */
        if (elem.filter) {
            _elem.setAttribute('table-filter', JSON.stringify(elem.filter));
        }

        /** Disable element if is not administrable */
        if (!elem.disabled) { _elem.setAttributeNode(_disabled); }

        /** Add icon to element type date */
        if (elem.name.match(/^dt/)) {
            _i.setAttribute('class', 'fa fa-calendar');
            _div.appendChild(_i);
        }

        /** Span set attributes */
        if (elem.type !== 'checkbox' && elem.type !== 'hidden') {

            if (this.getPattern(elem)) { _elem.setAttribute('pattern', this.getPattern(elem)); }

            if (elem.required) {
                _elem.setAttributeNode(_required);
                _span.setAttribute('class', 'valid-nebula');
                _div.appendChild(_span);
            }

        }

        /** remove basic style for select-multiple */
        if (_elem.type === 'select-multiple') {
            _div.classList.remove('form-group')
            _elem.classList.remove('form-control')

        }

        return _div;

    }

    static getTypeDOM(type) {
        const INPUTS_TYPE = [
            'button', 'checkbox','color','date','datetime-local','email','file',
            'hidden','month','number','password', 'radio', 'ranger', 'reset',
            'search', 'submit', 'tel', 'text', 'time', 'url', 'week'
        ];

        if (typeof type !== 'string') { throw Error('Type not accepted to create a element'); }

        switch (type) {
            case 'select':
                return BuildFormElementsDOM.getSelectElement();

            case 'select_multiple':
                return BuildFormElementsDOM.getSelectMultipleElement();

            default:
                if (INPUTS_TYPE.indexOf(type) !== -1) {
                    return BuildFormElementsDOM.getInputElement(type);
                }

        }

    }

    static getPattern(elem) {
        const OFFSET = 1, LENGTH = 30;

        if (elem.type) {
            elem.length = elem.length || LENGTH;

            switch (elem.type) {

                case 'text':
                    return elem.name.match(/^dt/)
                        ? sprintf('\\d{2}\\/\\d{2}\\/\\d{4}', OFFSET, elem.length)
                        : sprintf('.{%d,%d}', OFFSET, elem.length);

                case 'tel':
                    return sprintf('[0-9\\s]{%d,%d}', OFFSET, elem.length);

                case 'date':
                    return sprintf('\\d{2}\\/\\d{2}\\/\\d{4}', OFFSET, elem.length);

                default:
                    return;

            }
        }

    }

    static getInputElement(type) {
        let elem = document.createElement('input');

        elem.setAttribute('type', type);

        return elem;
    }

    static getSelectElement() {
        let elem = document.createElement('select'),
            _hidden = document.createAttribute('hidden'),
            _option =  document.createElement('option');

        _option.setAttributeNode(_hidden);

        elem.appendChild(_option);

        return elem;
    }

    static getSelectMultipleElement() {
        let elem = document.createElement('select'),
            _multiple = document.createAttribute('multiple'),
            _hidden = document.createAttribute('hidden'),
            _option =  document.createElement('option');

        _option.setAttributeNode(_hidden);

        elem.setAttributeNode(_multiple)
        elem.appendChild(_option);

        return elem;
    }

    static traitNumber(elem, _elem) {
        let _length = elem.length.split(','),
            _precision= Number(_length[0]) - Number(_length[1]),
            _escale = Number(_length[1]),
            _mask_step = _escale - 1;

        _elem.setAttribute('max', `${BuildFormElementsDOM.factoryMask(_precision, '9')}.${BuildFormElementsDOM.factoryMask(_escale, '9')}`);
        _elem.setAttribute('step', _mask_step ? `0.${BuildFormElementsDOM.factoryMask(_mask_step, '0')}1` : '0.1'); // Set attribute step to number
    }

    static factoryMask(number, mask) {
        return number > 1 ? mask += BuildFormElementsDOM.factoryMask(number - 1, mask) : mask;
    }

}

class ElementsDOM {

    constructor(elem) {

        if (typeof elem !== 'object' && Array.isArray(elem)) { throw new Error('Element cant not be create!'); }
        this.createElements(elem);
    }

    createElements(elem) {
        const MODEL_ELEMENT = ElementsDOM.getModelElements();

        /** Set element */
        for (let prop in MODEL_ELEMENT) {
            let $_key = MODEL_ELEMENT[prop],
                $_prop = $_key.convertCamelCaseFrom();

            // If the elements are passed to camelCase
            if (elem.hasOwnProperty($_key)) {
                prop === 'name'
                    ? this[prop] = elem[$_key].underscoreToCamel()
                    : this[prop] = elem[$_key];

            // If the elements are passed to underscore
            } else if (elem.hasOwnProperty($_prop)) {
                prop === 'name'
                    ? this[prop] = elem[$_prop].underscoreToCamel()
                    : this[prop] = elem[$_prop];

            }
        }
    }

    static getModelElements() {
        return {
            name:       'liChampTable',
            label:      'liChampDescription',
            type:       'liTypeElementUi',
            length:     'liChampTaille',
            required:   'isChampObligatoire',
            disabled:   'isAdministrable',
            table_ref:  'liChampTableRef',
            filter:     'jsFiltres'
        }
    }

}
