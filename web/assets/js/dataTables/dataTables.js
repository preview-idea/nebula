init_DataTables();

/* DATA TABLES */
function init_DataTables() {

    if ( typeof ($.fn.dataTable) === 'undefined') { return; }
    console.log('init_DataTables');

    // CUSTOM CONFIG LANGUAGE TO DEFATUL SETTING
    $.extend(true, $.fn.dataTable.defaults, {
        scrollX: '50vh',
        dom: "Bfrtip",
        lengthMenu: [
            [10,25,50, -1],
            ['10 lignes', '25 lignes', '50 lignes', 'Tout afficher']
        ],
        language: {
            search: "Rechercher :",
            emptyTable: "Aucune donnée disponible",
            info: "Aperçu de _START_ à _END_ sur _TOTAL_ enregistrements",
            infoEmpty: "Aperçu de 0 à 0 sur 0 enregistrements",
            zeroRecords: "Aucun enregistrements trouvés",
            infoFiltered: " ( filtré à partir d'un total de _MAX_ enregistrements )",
            paginate: {
                previous: "Précédent",
                next: "Suivant"
            },
            buttons: {
                pageLength: {
                    _: "Afficher %d lignes",
                    '-1': "Tout afficher"
                },
                colvis: "Affichage des colonnes"
            },
            searchBuilder: {
                add: 'Ajouter filtre',
                condition: 'Comparer',
                clearAll: 'Réinitialiser',
                deleteTitle: 'Supprimer',
                data: 'Colonne',
                leftTitle: 'Left',
                logicAnd: 'Et',
                logicOr: 'Ou',
                rightTitle: 'Right',
                title: {
                    0: 'Recherche customisée',
                    _: 'Recherche customisée(%d)'
                },
                value: 'Valeur',
            }
        },
        buttons: [
            {
                extend: "pageLength",
                className: "btn-sm"
            },
            {
                extend: "csv",
                className: "btn-sm",
                exportOptions: {
                    orthogonal: 'export',
                    columns: ':visible'
                }
            },
            {
                extend: "excel",
                className: "btn-sm",
                exportOptions: {
                    orthogonal: 'export',
                    columns: ':visible'
                }
            },
            {
                extend: "pdfHtml5",
                className: "btn-sm",
                exportOptions: {
                    orthogonal: 'export',
                    columns: ':visible'
                }
            },
            {
                extend: "print",
                className: "btn-sm",
                exportOptions: {
                    orthogonal: 'export',
                    columns: ':visible'
                }
            },
            {
                extend: "colvis",
                className: "btn-sm"
            }
        ]
    });

    // DATATABLES TO SORTING THE DATE.
    $.fn.dataTable.moment( 'DD/MM/YYYY' );

    // FUNCTION ELLIPSIS
    $.fn.dataTable.render.ellipsis = function ( cutoff, wordbreak, escapeHtml ) {
        return function ( d, type, row ) {

            // Order, search and type get the original data
            if (type !== 'display' || (typeof d !== 'number' && typeof d !== 'string')) { return d; }

            // cast numbers
            d = d.toString();

            if (d.length <= cutoff) { return d;}

            let shortened = d.substr(0, cutoff-1);

            // Find the last white space character in the string
            if ( wordbreak ) { shortened = shortened.replace(/\s([^\s]*)$/, ''); }

            return `<sapn class="ellipsis" title="${d}">${shortened}&#8230;</sapn>`;

        };

    };

}

// BLOCK ATTACK XSS
var __entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
};

String.prototype.escapeHTML = function() {
    return String(this).replace(/[&<>"'\/]/g, function (s) {
        return __entityMap[s];
    });
};

// DECODE
var __entityDescap = {
    "&amp;": "&",
    "&lt;": "<",
    "&gt;": ">",
    '&quot;': '"',
    '&#39;': "'",
    '&#x2F;': "/"
};

String.prototype.descapeHTML = function() {
    return String(this).replace(/(&amp;|&lt;|&gt;|&quot;|&#39;|&#x2F;)/g, function (s) {
        return __entityDescap[s];
    });
};