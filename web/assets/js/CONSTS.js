// Calcule horaire mensuel
const TEMPS_TRAVAIL_COMPLET = 35,
    NOMBRE_SEMAINE_PAR_AN = 52,
    NOMBRE_MOIS_PAR_AN = 12,
    NB_JOUR_FORFAIT = 212,
    MIN_DATE_RANGE = moment().subtract(80, 'year'),
    MAX_DATE_RANGE = moment().add(30, 'year'),
    MODAL_CONFIRM = $('div#modalConfirmation'),
    URL_CURRENT = window.location.pathname,

    /***** GLOBAL CUSTOM FUNCTIONS *******/
    sprintf = function () {
        let a = arguments[0];

        for (let c in arguments) {
            if (arguments.hasOwnProperty(c) && c > 0) {
                a = a.replace(/%[sd]/, arguments[c])
            }
        }
        return a;
    },

    uniqidLocal = function () {
        return String.fromCharCode(Math.floor(Math.random() * (90 - 64) + 65)) + Math.floor(Math.random() * 1000000);
    },

    isEmpty = function (obj) {
        for (let prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }
        return JSON.stringify(obj) === JSON.stringify({});
    },

    getOptionsText = function (select) {

        if (select instanceof jQuery) {

            if (select[0].type === 'select-multiple') {
                const options = [];
                select.find('option:selected').each(function() { options.push($(this).text().trim()) });
                return options.join(', ')
            }

            return select.find('option:selected').text()

        } else {
            return select.options[select.selectedIndex].text;
        }

    },

    getLabelElement = function(element) {
        let parent = (['select-one','select-multiple'].indexOf(element.type) !== -1)
            ? $('select[name="'+ element.name +'"]').parent()
            : $('input[name="'+ element.name +'"]').parent();

        return (element.className === 'flat')
            ? parent.parent().find('label').first().text()
            : parent.find('label').first().text();

    },

    copyTextOfOption = function (select) {
        let textOption = getOptionsText(select),
            input_copy = document.createElement('input'),
            readonly = document.createAttribute('readonly');

        input_copy.setAttribute('value', textOption);
        input_copy.setAttributeNode(readonly);

        select.parent().append(input_copy);

        input_copy.focus();
        input_copy.select();

        try {
            document.execCommand('copy');
        } catch (err) {
            console.error(err)
        }

        input_copy.remove();

    },

    getCheckboxText = function (name) {

        if (name instanceof jQuery) {
            name = name.find('h4').text();
        }
        if (typeof name === 'string') {
            name = $('form[name="' + name + '"]').find('h4').text();
        }

        return name.replace(/(\s+)?:/, '');

    },

    traitStatusDtaTables = etat => {
        // Add span to view status
        let label_status = 'label label-';

        switch (etat) {
            case 'Inactif':
            case 'Inactive':
                label_status += 'danger';
                break;

            case 'Actif':
            case 'Active':
                label_status += 'success';
                break;

            case 'Inexistant':
            case 'Inexistante':
                label_status += 'warning';
                break;

            case undefined:
                etat = 'Actif';
                label_status += 'primary';
                break;
        }

        return `<span style="font-weight: normal;" class="${label_status}">${etat}</span>`
    },

    addActionAfterPreload = callback => {
        OJBNebulaPreloaded.customLoad = function () {
            callback();
        }
    },

    // BLOCK INPUT
    disableInput = function (target) {

        if (target instanceof jQuery) {
            const current = target.val();

            target.prop('readonly', true);
            target.on('input change focus', function () {
                // REMOVE DATE RANGE PICKER
                if (target.prop('name').match(/^dt/)) {
                    $('div.daterangepicker').remove();
                }
                $(this).val(current);
            });
        }

    },

    addRequirementIfValue = function (target) {

        if (target instanceof jQuery) {

            target.prop('required', target.val());

            if (target.val()) {
                if (!target.parent().has('span.valid-nebula').length) {
                    target.parent().append('<span class="valid-nebula"></span>');
                }
            } else {
                if (target.parent().has('span.valid-nebula').length) {
                    target.parent().find('span.valid-nebula').remove();
                }
            }
        }

    },

    // BLOCK CHECKBOX
    disableCheckbox = function (target) {

        if (target instanceof jQuery) {
            let check = target.prop('checked');

            target.prop('disabled', true);
            target.on('ifChanged', function () {
                $(this).prop('disabled', true);

                $(this).prop('checked', check);
                setTimeout(function () {
                    check ?
                        target.iCheck('check') :
                        target.iCheck('uncheck');
                }, 5)
            });
        }
    },

    // REMOVE OPTIONS NOT SELECTED
    removeOptions = function (target) {

        if (target instanceof jQuery) {
            let options = target.find('option:not(:selected)');

            // Remove options not selected
            options.remove();

            // Reset Select
            target.select2({
                minimumResultsForSearch: 12,
                language: 'fr'
            })
        }

    },

    destroyInstanceDataTables = instance => {
        if (instance instanceof jQuery) {

            if ($.fn.DataTable.isDataTable(instance[0])) {
                instance.DataTable().destroy();
                instance.empty();
            }

        } else {
            throw new Error(sprintf('this %s is not an element jquery valid', instance))
        }

    },

    isVisible = function (e) {
        return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length);
    },

    traitCommonElements = function (e) {
        let r = {};

        for (let i = 0; i < e.length; i++) {
            if ($(e[i]).val()) {

                if (['checkbox', 'radio'].includes(e[i].type)) {
                    r[e[i].name] = (e[i].checked) ? 1 : 0;

                } else if (e[i].name.match(/^id/)) {
                    r[e[i].name] = $(e[i]).val();

                } else {
                    r[e[i].name] = e[i].value.trim();

                }
            }

        } // for
        return r;
    },

    batchUpdate = async ({
         idTraitement,
         idMatriculeValideur,
         idEtatTraitement,
         dtFinTraitement,
         paramFunction,
         redirectFunction,
         liMessage = null
    }) => {

        if (!idMatriculeValideur || !idTraitement) {
            throw new Error('We need an ID type of matriculeValideur, traitement')
        }

        const obj_batch = {
            idTraitement,
            idMatriculeValideur,
            liMessage
        };

        // In case of refusal, we update idEtatTraitemetn and dtFinTraitement
        if (idEtatTraitement) { obj_batch.idEtatTraitement = idEtatTraitement }
        if (dtFinTraitement) { obj_batch.dtFinTraitement = dtFinTraitement }

        const _data = await $.post('/batches/update', obj_batch);

        if (typeof _data !== "number") {

            notifyNebula('Le valideur a été ajouté, la demande va être traitée.', null, 'success');
            if (typeof redirectFunction === 'function')
                setTimeout(function () {
                    redirectFunction(paramFunction);
                }, 2000);
        } else {
            console.log(obj_batch)
            notifyNebula('Erreur lors de l\'ajout du valideur.', null, 'error');
        }

    },

    traitElementsToTwig = function (e) {
        let r = {};

        for (let i = 0; i < e.length; i++) {
            if (e[i].value || (e[i].type === 'select-one' && e[i].options[e[i].selectedIndex].value)) {

                if (e[i].name.match(/^dt/)) {
                    r[e[i].name] = moment(e[i].value.convertToDate()).toISOString();

                } else if (e[i].name.match(/^id/)) {
                    e[i].type === 'select-one'
                        ? r[e[i].name] = {
                            [e[i].name]: parseInt(e[i].options[e[i].selectedIndex].value),
                            [e[i].name.replace(/^id/, 'li')]: getOptionsText(e[i])
                        }
                        : e[i].name.match(/(Prime|Clauses)/)
                        ? r[e[i].name] = {
                            [e[i].name]: parseInt(e[i].value),
                            [e[i].name.replace(/^id/, 'li')]: getCheckboxText(e[i].form.name),
                            isParametrable: getParametrage(e[i].form.name)
                        }
                        : r[e[i].name] = parseInt(e[i].value);

                } else if (e[i].type === 'checkbox') {
                    r[e[i].name] = (e[i].checked) ? 1 : 0;

                } else if (e[i].type === 'number') {
                    r[e[i].name] = Number(e[i].value);

                } else {
                    e[i].type === 'text'
                        ? r[e[i].name] = e[i].value.trim()
                        : r[e[i].name] = e[i].value;

                }
            }

        } // for
        return r;

    },

    getDateCalandrierPaie = async dt => {
        if (dt) {
            const _data = await $.getJSON('/params', {filter: dt, param: 'calendrier_paie'});
            return _data.nbJourEnvoiAcomptesRhpi
        }

    },

    openOneRowInDataTable = function (target) {

        target.find('tbody tr:not([class=child])').on('click', function () {
            let tr_current = $(this).index(),
                tbody_tr = $(this).parents('tbody').find('tr'),
                tr_opened = [];

            tbody_tr.each(function () {
                if ($(this).index() !== tr_current) {

                    if ($(this).hasClass('parent')) {
                        tr_opened.push($(this));
                    }
                }
            });

            if (tr_opened.length) {

                $(tr_opened).each(function () {

                    if ($(this).index() !== tr_current) {
                        let rowCollap = target.DataTable().row($(this));

                        rowCollap.child.hide();
                        $(this).removeClass('parent');
                    }
                })

            }
            reloadJS();
        });

    },

    disableHideColumn = function (columnIndex) {
        let buttonArray = $.fn.dataTable.defaults.buttons,
            objIndex = buttonArray.findIndex((obj => obj.extend === 'colvis'));

        buttonArray[objIndex].columns = sprintf(':gt(%s)', columnIndex);

        return buttonArray;
    },

    disabledButtonMenu = function (tabButton) {

        //disable button in right menu
        tabButton.forEach(element => {
            element.parent().addClass('not-allowed');
            element.addClass('disabled');
        });

    },

    sortObjectByKeyRecursive = function (object) {

        // Not to sort the array
        if (typeof object !== "object" || object instanceof Array || !object) {
            return object;
        }

        let keys = Object.keys(object),
            newObject = {};

        keys.sort();

        for (let i = 0; i < keys.length; i++) {
            newObject[keys[i]] = sortObjectByKeyRecursive(object[keys[i]])
        }

        return newObject;
    };

